<?php
/*
Template Name: Ritas Store Locator Search Page Template
Description:
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

				    <div id="main" class="first clearfix" role="main">
					    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                <h1 class="page-title" itemprop="headline"><?php ritas_the_title(); ?></h1>
                <?php the_content(); ?>
                <?php echo do_shortcode('[SLPLUS]'); ?>

					    <?php endwhile; endif; ?>

    				</div> <!-- end #main -->

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>
