<?php
/*
File Description: Press Release Content For Top Right Block on the Press Center Page
*/


?>

<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix press-content'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

  <header class="article-header">
    <div class="press-release-header">
      <!-- Display the Title as a link to the Post's permalink. -->
      <h3 class="press-headline"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
      <!-- Display the date (November 16th, 2012 format) and a link to other posts by this posts author. -->
          </div><!-- end press-release-header -->

  </header>

  <footer class="article-footer">
    <!-- Add information about pagination -->
  </footer>  <!-- end article footer -->
</article> <!-- end article -->


