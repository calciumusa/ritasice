<?php

/*
*  File: store_locator_map_functions.php
*  description: This file contains the database query function for
*        classifying which states have locations on the hover map
*  author: Anson W. Han, The Star Group
*/


/*
*  Function: classifyActiveStates()
*  description:  obtains distinct list of states from wp_store_locator table and uses
*    that to assign an "avail" or "notavail" class to each state (DD element) on
*    clickable/hover map
*/
function classifyActiveStates() {
  global $wpdb;
  $lookupSQL = 'SELECT DISTINCT filteredResults1.sl_state FROM (SELECT * FROM wp_store_locator WHERE sl_tags NOT LIKE "Coming Soon") AS filteredResults1 WHERE filteredResults1.sl_state <> ""';
  $statesList = $wpdb->get_results($lookupSQL);
  /*debug*/ /* echo '<script>console.log("' . count($statesList) . ' states with stores found in the database by the classifyActiveStates function.");</script>'; */

  if(count($statesList) > 0 ) : ?>
    <script>
    var statesArray = <?php echo json_encode($statesList); ?>;
    //console.log(statesArray);
    jQuery(document).ready(function($) {
      $('dd').each(function() {
        $(this).addClass('notavail').removeClass('avail');
      });
      var stateCount = statesArray.length;
      for (var i=0; i<stateCount; i++) {
        var stateID = "#img_";
        stateID = stateID + statesArray[i]['sl_state'].toLowerCase();
        $(stateID).addClass('avail').removeClass('notavail');
      }
    });
    </script>
  <?php
  endif;
} /* end classifyActiveStates */



function filterStates($state) {
/* debug */  /*echo '<script>alert("filterStates called with a state parameter of ' . $state . '");</script>' */;
  global $wpdb;
  $lookupSQL = 'SELECT * FROM wp_store_locator WHERE sl_tags NOT LIKE "Coming Soon" AND sl_state ="' . $state . '" ORDER BY sl_city,sl_city ASC';
  $storesList = $wpdb->get_results($lookupSQL);

  $myZoomFactor = 7;
  $myLat = $myLng = "";
  switch ($state) {
    case 'AL':
      $myLat = "32.841667";
      $myLng = "-86.633333";
      $myZoomFactor = 7;
      break;
    case 'AK':
      $myLat = "64.73166667";
      $myLng = "-152.47";
      $myZoomFactor = 5;
      break;
    case 'AZ':
      $myLat = "34.30833333";
      $myLng = "-111.7933333";
      $myZoomFactor = 7;
      break;
    case 'AR':
      $myLat = "34.815";
      $myLng = "-92.30166667";
      $myZoomFactor = 7;
      break;
    case 'CA':
      $myLat = "36.965";
      $myLng = "-120.0816667";
      $myZoomFactor = 6;
      break;
    case 'CO':
      $myLat = "38.99833333";
      $myLng = "-105.6416667";
      $myZoomFactor = 7;
      break;
    case 'CT':
      $myLat = "41.595";
      $myLng = "-72.70666667";
      $myZoomFactor = 9;
      break;
    case 'DE':
      $myLat = "38.98";
      $myLng = "-75.51166667";
      $myZoomFactor = 8;
      break;
    case 'DC':
      $myLat = "39.16666667";
      $myLng = "-76.85";
      $myZoomFactor = 9;
      break;
    case 'FL':
      $myLat = "28.13333333";
      $myLng = "-81.63166667";
      $myZoomFactor = 6;
      break;
    case 'GA':
      $myLat = "32.71333333";
      $myLng = "-83.495";
      $myZoomFactor = 7;
      break;
    case 'HI':
      $myLat = "20.95166667";
      $myLng = "-157.2766667";
      $myZoomFactor = 7;
      break;
    case 'ID':
      $myLat = "44.25666667";
      $myLng = "-114.9566667";
      $myZoomFactor = 6;
      break;
    case 'IL':
      $myLat = "40.01333333";
      $myLng = "-89.30666667";
      $myZoomFactor = 6;
      break;
    case 'IN':
      $myLat = "39.895";
      $myLng = "-86.26666667";
      $myZoomFactor = 7;
      break;
    case 'IA':
      $myLat = "41.385";
      $myLng = "-93.385";
      $myZoomFactor = 7;
      break;
    case 'KS':
      $myLat = "38.49833333";
      $myLng = "-98.69833333";
      $myZoomFactor = 7;
      break;
    case 'KT':
      $myLat = "37.35833333";
      $myLng = "-85.50666667";
      $myZoomFactor = 7;
      break;
    case 'LA':
      $myLat = "30.96833333";
      $myLng = "-92.53666667";
      $myZoomFactor = 7;
      break;
    case 'ME':
      $myLat = "45.25333333";
      $myLng = "-69.23333333";
      $myZoomFactor = 7;
      break;
    case 'MD':
      $myLat = "39.44166667";
      $myLng = "-77.37166667";
      $myZoomFactor = 8;
      break;
    case 'MA':
      $myLat = "42.34";
      $myLng = "-72.03166667";
      $myZoomFactor = 9;
      break;
    case 'MI':
      $myLat = "45.06166667";
      $myLng = "-84.93833333";
      $myZoomFactor = 6;
      break;
    case 'MN':
      $myLat = "46.025";
      $myLng = "-95.32666667";
      $myZoomFactor = 6;
      break;
    case 'MS':
      $myLat = "32.815";
      $myLng = "-89.71666667";
      $myZoomFactor = 7;
      break;
    case 'MO':
      $myLat = "38.495";
      $myLng = "-92.63166667";
      $myZoomFactor = 7;
      break;
    case 'MT':
      $myLat = "47.03166667";
      $myLng = "-109.6383333";
      $myZoomFactor = 6;
      break;
    case 'NE':
      $myLat = "41.525";
      $myLng = "-99.86166667";
      $myZoomFactor = 7;
      break;
    case 'NV':
      $myLat = "39.505";
      $myLng = "-116.9316667";
      $myZoomFactor = 6;
      break;
    case 'NH':
      $myLat = "43.64166667";
      $myLng = "-71.57166667";
      $myZoomFactor = 7;
      break;
    case 'NJ':
      $myLat = "40.07";
      $myLng = "-74.55833333";
      $myZoomFactor = 7;
      break;
    case 'NM':
      $myLat = "34.50166667";
      $myLng = "-106.1116667";
      $myZoomFactor = 6;
      break;
    case 'NY':
      $myLat = "42.965";
      $myLng = "-76.01666667";
      $myZoomFactor = 7;
      break;
    case 'NC':
      $myLat = "35.60333333";
      $myLng = "-79.455";
      $myZoomFactor = 7;
      break;
    case 'ND':
      $myLat = "47.41166667";
      $myLng = "-100.5683333";
      $myZoomFactor = 7;
      break;
    case 'OH':
      $myLat = "40.36166667";
      $myLng = "-82.74166667";
      $myZoomFactor = 7;
      break;
    case 'OK':
      $myLat = "35.53666667";
      $myLng = "-97.66";
      $myZoomFactor = 7;
      break;
    case 'OR':
      $myLat = "43.86833333";
      $myLng = "-120.9783333";
      $myZoomFactor = 7;
      break;
    case 'PA':
      $myLat = "40.89666667";
      $myLng = "-77.54666667";
      $myZoomFactor = 7;
      break;
    case 'RI':
      $myLat = "41.67166667";
      $myLng = "-71.57666667";
      $myZoomFactor = 9;
      break;
    case 'SC':
      $myLat = "33.83";
      $myLng = "-80.87333333";
      $myZoomFactor = 7;
      break;
    case 'SD':
      $myLat = "44.40166667";
      $myLng = "-100.4783333";
      $myZoomFactor = 7;
      break;
    case 'TN':
      $myLat = "35.795";
      $myLng = "-86.62166667";
      $myZoomFactor = 7;
      break;
    case 'TX':
      $myLat = "31.24333333";
      $myLng = "-99.45833333";
      $myZoomFactor = 6;
      break;
    case 'UT':
      $myLat = "39.38666667";
      $myLng = "-111.685";
      $myZoomFactor = 6;
      break;
    case 'VT':
      $myLat = "43.92666667";
      $myLng = "-72.67166667";
      $myZoomFactor = 7;
      break;
    case 'VA':
      $myLat = "37.48833333";
      $myLng = "-78.56333333";
      $myZoomFactor = 7;
      break;
    case 'WA':
      $myLat = "47.33333333";
      $myLng = "-120.2683333";
      $myZoomFactor = 7;
      break;
    case 'WV':
      $myLat = "38.59833333";
      $myLng = "-80.70333333";
      $myZoomFactor = 7;
      break;
    case 'WI':
      $myLat = "44.43333333";
      $myLng = "-89.76333333";
      $myZoomFactor = 6;
      break;
    case 'WY':
      $myLat = "42.97166667";
      $myLng = "-107.6716667";
      $myZoomFactor = 6;
      break;
    default:
      break;
  }

  $customGoogleMapsAPIstring = '<script type="text/javascript">' . "\n";

  $i = 0;
  $infoWindowContentArray = array();
  foreach($storesList as $store) {
    $streetAddress = $store->sl_address;
    if($store->sl_address2 != "") { $streetAddress .= ', ' . $store->sl_address2; }
    $streetAddress .= ', ' . $store->sl_city . ', ' . $store->sl_state . ', ' . $store->sl_zip;

    $infoWindowContentArray[$i] = "
    <div id='sl_info_bubble'>
    <!--tr><td--><strong>" . $store->sl_store . "</strong><br>" . $streetAddress . "
    <br> <a href='http://maps.google.com/maps?saddr=" . $store->sl_latitude . "%2C" . $store->sl_longitude . "&amp;daddr=" . urlencode($streetAddress) . "' target='_blank' class='storelocatorlink'>Directions</a> | <a href='" . $store->sl_url . "' target='_blank' id='slp_marker_website' class='storelocatorlink'><nobr>Website </nobr></a>
    <br>" . $store->sl_description . "
    <br><span class='location_detail_label hours'>Hours: </span>" . $store->sl_hours . "
    <br><span class='location_detail_label contact'>Phone:</span><span class='contact_link'><a href='tel:" . $store->sl_phone . "' target='_blank'>" . $store->sl_phone . "</a></span>
    <br><span class='location_detail_label contact'>Facebook:</span><span class='contact_link'><a href='" . $store->sl_email . "' target='_blank'>" . $store->sl_email . "</a></span>
    <br><span class='location_detail_label contact'>Twitter:</span><span class='contact_link'><a href='" . $store->sl_fax . "' target='_blank'>" . $store->sl_fax . "</a></span>
    <br><div class='bubble'><span class='slp_info_bubble_tags'>" . $store->sl_tags . "</span></div>
    <br><!--/td></tr--></div>
    ";

    $i++;
  }
  $customGoogleMapsAPIstring .= 'var geocoded_locationsHTML = [];' . "\n";
  for($i = 0; $i < count($infoWindowContentArray); $i++) {
    $customGoogleMapsAPIstring .= 'geocoded_locationsHTML[' . $i . '] = "' . urlencode ( $infoWindowContentArray[$i] ) . '";' . "\n";
  }
  /* complete definition of geocoded_locationsHtml js array for google maps API call */


  $customGoogleMapsAPIstring .= 'var geocoded_locations = [' . "\n";
  foreach($storesList as $store) {
    $customGoogleMapsAPIstring .= '["'. $store->sl_store  .'", ' . $store->sl_latitude . ', ' . $store->sl_longitude . '],';
  }
  $customGoogleMapsAPIstring .= '];' . "\n"; /* complete definition of geocoded_locations js array for google maps API call */

  $customGoogleMapsAPIstring .= '
  for (i = 0; i < geocoded_locations.length; i++) {
    if (i < 3) { console.log(geocoded_locationsHTML[i]); }
    geocoded_locations[i][0] = geocoded_locationsHTML[i];
  }
  ';

    $customGoogleMapsAPIstring .= '

    var map = new google.maps.Map(document.getElementById("resultsMapCanvas"), {
      zoom: '. $myZoomFactor . ',
      center: new google.maps.LatLng(' . $myLat . ',' . $myLng . '),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;
    var image = "/wp-content/themes/ritas/library/images/ritas/ritas-map-locator.gif";
    for (i = 0; i < geocoded_locations.length; i++) {
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(geocoded_locations[i][1], geocoded_locations[i][2]),
        map: map,
        icon: image
      });

      google.maps.event.addListener(marker, "click", (function(marker, i) {
        return function() {
          infowindow.setContent( decodeURIComponent(geocoded_locationsHTML[i].replace(/\+/g, " ")) );
/*          infowindow.setContent(geocoded_locations[i][0]); */
          infowindow.open(map, marker);
        }
      })(marker, i));
    }

    </script>';


  $resultsString = '<div id="sl_div">';
  $resultsString .= '<table id="results_table"><tbody><tr id="cm_mapTR" class="slp_map_search_results"><td width="" valign="top" id="map_sidebar_td" style="display: block;"><div id="map_sidebar" style="width:100%;">';
  $resultsString .= '<ol class="locations-listing">';
/* debug */ /*   echo '<script>alert("There are ' . count($storesList) . ' stores in the query results");</script>'; */

  foreach($storesList as $store) {
           /** Create the results table
          *
          *              {10} slplus.map_domain,
          *              {11} encodeURIComponent(this.address),
          *              {12} encodeURIComponent(address),
          */
			$tags = $store->sl_tags;
			$coolAlertsVisibility = ' style="display:none;visibility:hidden;"';
      if(strpos($tags,"Cool Alerts") > -1)
			{
			  $coolAlertsVisibility = ' style="display:block;visibility:visible;"';
			}
			$tags = str_replace("Cool Alerts,","",$tags);
			$tags = str_replace("Cool Alerts","",$tags);
      $destinationAddressEncoded = $store->sl_address . ', ' . $store->sl_city . ', ' .  $store->sl_state . ' ' . $store->sl_zip;
      $destinationAddressEncoded = urlencode($destinationAddressEncoded);
    $resultsString .= '<li class="results_entry location-listing" id="slp_results_entry_' . $store->sl_id . '">' .
    '<table width="97%" cellpadding="4px" cellspacing="0" class="searchResultsTable" id="slp_results_table_' . $store->sl_id . '">'  .
      '<tr class="slp_results_row" id="slp_location_' . $store->sl_id . '">'  .
          '<td class="results_row_left_column" id="slp_left_cell_' . $store->sl_id . '">'.
              '<span class="location-name-ID">' .
                '<span class="location_name">' . $store->sl_store . '</span><br/>'.
                '<span class="slp_result_contact slp_result_directions"><a href="http://maps.google.com/maps?saddr='  .
                '&daddr=' . $destinationAddressEncoded .
                '" target="_blank" class="storelocatorlink">Directions</a></span><br/>'.
              '</span><!--/.location-name-ID-->' .
                '<div class="location-specific-social-links">' .
                  '<div class="cool-alerts" ' . $coolAlertsVisibility . ' >' .
                    '<a href="http://www.ritasfranchises.com/stores/store.cfm?store=' . $store->ritas_id . '}&p=FOD" target="_blank" class="coolAlerts"></a>' .
                  '</div><!--/.cool-alerts-->' .
                '<div class="social-icons">' .
                  '<div class="social-icon"><a href="' . $store->sl_email . '" target="_blank" class="facebook"></a></div>' .
                  '<div class="social-icon"><a href="' . $store->sl_fax . '" target="_blank" class="twitter"></a></div>' .
                '</div> <!--/.social-icons-->' .
              '</div><!--/.location-specific-social-links-->' .
          '</td>'  .
          '<td class="results_row_center_column" id="slp_center_cell_' . $store->sl_id . '">' .
              '<address><span class="slp_result_address slp_result_street">' . $store->sl_address . '</span><br/>';
              if($store->sl_address2 <> "") {
                $resultsString .= '<span class="slp_result_address slp_result_street2">' . $store->sl_address2 . '</span><br/>';
              }
              $resultsString .= '<span class="slp_result_address slp_result_citystatezip">' . $store->sl_city . ', ' . $store->sl_state . '  ' . $store->sl_zip . '</span></address>' .
              '<span class="location-phone slp_result_address slp_result_phone">Phone: ' . $store->sl_phone . '</span><br/>' .
              '<span class="location-url slp_result_website">website: <a href="' . $store->sl_url . '" target="_blank">' . $store->sl_url . '</a></span><br/>' .
              '<span class="slp_result_contact slp_result_tags">Additional Services/Products: ' . $tags . '</span>'.
          '</td>'   .
          '<td class="results_row_right_column" id="slp_right_cell_' . $store->sl_id . '">' .
              '<span class="location-hours">' . $store->sl_hours . '</span>' .
          '</td>'  .
      '</tr>'  .
    '</table></li>';
  }
  $resultsString .= '</ol><!--/.locations-listing-->';
  $resultsString .= '</div><!--/#map_sidebar-->';
  $resultsString .= '</td></tr></tbody></table><!--/#results_table--></div><!--/#sl_div-->';

  echo $customGoogleMapsAPIstring . "\n";
  echo $resultsString;
}
?>