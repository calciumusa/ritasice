<?php
/*
*   file:  product_menu_sync.php
*   description:  Rita's product/flavor/size feed parser and WP sync functionality
*   function:  imports product/flavor/size data from JSON feed and syncs to individual product_menu custom posts in WordPress
*
*/
?>

<?php

$italianIcePID = $sugarFreeItalianIcePID = $creamIcePID = $custardPID = $gelatiPID = $mistoPID = $blendiniPID = $milkshakePID = $frozenLemonadePID = $toppingPID = $sundaePID = null;
$italianIceFlavs = $sugarFreeItalianIceFlavs = $creamIceFlavs = $custardFlavs = $gelatiFlavs = $mistoFlavs = $blendiniFlavs = $milkshakeFlavs = $frozenLemonadeFlavs = $toppingFlavs = $sundaeFlavs = $hybridFlavs = null;
$italianIceSizes = $sugarFreeItalianIceSizes = $creamIceSizes = $custardSizes = $gelatiSizes = $mistoSizes = $blendiniSizes = $milkshakeSizes = $frozenLemonadeSizes = $toppingSizes = $sundaeSizes = null;
$italianIceNutri = $sugarFreeItalianIceNutri = $creamIceNutri = $custardNutri = $gelatiNutri = $mistoNutri = $blendiniNutri = $milkshakeNutri = $frozenLemonadeNutri = $toppingNutri = $sundaeNutri = null;


/* page/post ID:     desktopMenu postID      mobileMenu postID  :  product index*/
/*
214: 4328: 4339 "Italian Ice";
818: 4329: 4341 "Sugar-Free";
820: 4330: 4342 "Cream Ice";
238: 4332: 4343 "Custard";
246: 4333: 4344 "Misto";
244: 4334: 4345 "Gelati";
541: 4336: 4346 "Blendini";
554: 4337: 4347 "Toppings";
556: 4338: 4348 "MilkShakes"; // no nutrition info
558: "Frozen Lemonade";
4191: "Sundaes";
4214: "Frozen Custard Cakes";
4218: "Custard Cookie Sandwiches";
*/



/*
* Function: alphaSortFlavors($flavorArray)
* description: resorts an array of flavor names and IDs alphabetically
* input argument: array of flavors as parsed from JSON feed
* return argument: alphabetically sorted array
*
*/
function alphaSortFlavors($flavorArray) {
  // Obtain a list of columns
  foreach ($flavorArray as $key => $row) {
    $flavID[$key]  = $row['FLAVORID'];
    $flavName[$key] = $row['FLAVORNAME'];
  }

  // Sort the data by flavor name, then ID
  array_multisort($flavName, SORT_ASC, $flavID, SORT_ASC, $flavorArray);

  return $flavorArray;
};  //end function alphaSortFlavors()



/*
* Function: loadNutritionJSON()
* description: loads product/size/flavor JSON file and parses individual arrays for product-specific
*     flavors, product-specific sizes, and the product IDs from the JSON feed
*
*/
function loadNutritionJSON() {
  /* establish variable to point to json product data file */
  //$filepath = 'http://' . $_SERVER['SERVER_NAME'] . '/data/star_nutrition.txt';
  $filepath = 'http://ritasfranchises.com/Vendors_Gateway/data/star_nutrition.txt';
  $nutriJsonString = file_get_contents($filepath);
  $nutriJsonString = utf8_decode($nutriJsonString);
  $nutriJsonArray = json_decode($nutriJsonString, true);

  switch (json_last_error()) {
      case JSON_ERROR_NONE:
//        echo ' - No errors';
          break;
      case JSON_ERROR_DEPTH:
          echo ' - Maximum stack depth exceeded';
          break;
      case JSON_ERROR_STATE_MISMATCH:
          echo ' - Underflow or the modes mismatch';
          break;
      case JSON_ERROR_CTRL_CHAR:
          echo ' - Unexpected control character found';
          break;
      case JSON_ERROR_SYNTAX:
          echo ' - Syntax error, malformed JSON';
          break;
      case JSON_ERROR_UTF8:
          echo ' - Malformed UTF-8 characters, possibly incorrectly encoded';
          break;
      default:
          echo ' - Unknown error';
          break;
  }

  /*debug*/ //var_dump($jsonArray['PRODUCTS']);

  /* preformatted array has additional wrappers, assign nutrition array without extra wrapper */
  $nutriJsonGlobalArray = $nutriJsonArray['products'];

  global $italianIceNutri, $sugarFreeItalianIceNutri, $creamIceNutri, $custardNutri, $gelatiNutri, $mistoNutri, $blendiniNutri, $milkshakeNutri, $frozenLemonadeNutri, $toppingNutri, $sundaeNutri;

  foreach ($nutriJsonGlobalArray as $key => $val) {
    if(is_array($val)) {
        switch($nutriJsonGlobalArray[$key]['name']) {
          case 'Italian Ice':
            $italianIceNutri = $nutriJsonGlobalArray[$key]['size'];
            break;
          case 'Sugar-Free':
            $sugarFreeItalianIceNutri = $nutriJsonGlobalArray[$key]['size'];
            break;
          case 'Cream Ice':
            $creamIceNutri = $nutriJsonGlobalArray[$key]['size'];
            break;
          case 'Custard':
            $custardNutri = $nutriJsonGlobalArray[$key]['size'];
            break;
          case 'Gelati':
            $gelatiNutri = $nutriJsonGlobalArray[$key]['size'];
            break;
          case 'Misto':
            $mistoNutri = $nutriJsonGlobalArray[$key]['size'];
            break;
          case 'Blendini':
            $blendiniNutri = $nutriJsonGlobalArray[$key]['size'];
            break;
          case 'Milkshakes':
            $milkshakeNutri = $nutriJsonGlobalArray[$key]['size'];
            break;
          case 'Frozen Lemonade':
            $frozenLemonadeNutri = $nutriJsonGlobalArray[$key]['size'];
            break;
          case 'Topping':
            $toppingNutri = $nutriJsonGlobalArray[$key]['size'];
            break;
          case 'Sundaes':
            $sundaeNutri = $nutriJsonGlobalArray[$key]['size'];
            break;
          default:
            break;
        } //end switch statement to set keys
    }
  } /* end foreach, key lookup & detection */

}; //end function loadNutritionJSON()



/*
* Function: compareFlavorNutrition($flavorArray, $nutritionArray)
* description: compares flavors from Product JSON file to flavors in Nutrition JSON
*    file and erases flavors that don't have nutrition stats
* returns updated $flavorArray
*/
function compareFlavorNutrition($flavorArray, $nutritionArray, $consoleLogging) {
  if($consoleLogging) {
    print_r($flavorArray);
    echo '<h3>Number of flavors detected in flavor array: ' . count($flavorArray) . '</h3>';
    print_r($nutritionArray);
    echo '<h3>Number of flavors detected in nutrition array: ' . count($nutritionArray) . '</h3>';
  }

  $keysToRemove = array();

  for($j = 0; $j < count($flavorArray); $j++) {
      $flavorNutritionAvail = -1;
      $flavorID = $flavorArray[$j]['FLAVORID'];
      $flavorName = $flavorArray[$j]['FLAVORNAME'];
      //if($consoleLogging) { echo '<ul><li>j value: ' . $j . '; flavor to search for: ' . $flavorID . '</li><ol>'; }

      for($k = 0; $k < count($nutritionArray); $k++) {
        $currentValue = $nutritionArray[$k]["attribute"][0];
        //if($consoleLogging) { echo '<li>nutrition row flavor value: ' . $currentValue . '</li>'; }
        if($currentValue == $flavorID) {
          $flavorNutritionAvail = $k; /* match found */
        }
      }

      if($flavorNutritionAvail == -1) {
/*debug*/ if($consoleLogging) { echo "<li>The flavor, " . $flavorName . " [" . $flavorID . "] was not found in the nutritional feed and is being queued for removal.</li>"; }
        array_push($keysToRemove, $j);
      }
      //if($consoleLogging) { echo '</ol><li>ending j value: ' . $j . '</li></ul>'; }
  } // end for loop

  if($consoleLogging) { echo '<h3>A total of ' . count($keysToRemove) . ' flavors were not found, and will be removed from display.</h3>'; }

  for($w = 0; $w < count($keysToRemove); $w++) {
    $keyIndex = $keysToRemove[$w];
    unset($flavorArray[$keyIndex]);
  }
  sort($flavorArray);
  $flavorArray = alphaSortFlavors($flavorArray);

  return $flavorArray;
};



/*
* Function: loadJSON()
* description: loads product/size/flavor JSON file and parses individual arrays for product-specific
*     flavors, product-specific sizes, and the product IDs from the JSON feed
*
*/
function loadJSON() {
  /* establish variable to point to json product data file */
  //$filepath = 'http://' . $_SERVER['SERVER_NAME'] . '/data/star_product_size_flavor.txt';
  $filepath = 'http://www.ritasfranchises.com/Vendors_Gateway/data/star_product_size_flavor.txt';
  $jsonString = file_get_contents($filepath);
  $jsonString = utf8_decode($jsonString);
  $jsonArray = json_decode($jsonString, true);

  switch (json_last_error()) {
      case JSON_ERROR_NONE:
//        echo ' - No errors';
          break;
      case JSON_ERROR_DEPTH:
          echo ' - Maximum stack depth exceeded';
          break;
      case JSON_ERROR_STATE_MISMATCH:
          echo ' - Underflow or the modes mismatch';
          break;
      case JSON_ERROR_CTRL_CHAR:
          echo ' - Unexpected control character found';
          break;
      case JSON_ERROR_SYNTAX:
          echo ' - Syntax error, malformed JSON';
          break;
      case JSON_ERROR_UTF8:
          echo ' - Malformed UTF-8 characters, possibly incorrectly encoded';
          break;
      default:
          echo ' - Unknown error';
          break;
  }

  /*debug*/ //var_dump($jsonArray['PRODUCTS']);

  /* preformatted array has additional wrappers, assign product array without extra wrapper */
  $jsonGlobalProductArray = $jsonArray['PRODUCTS'];

  /*debug*/ // $jsonProductTypeQuantity = count($jsonGlobalProductArray);
  /*debug*/ // echo "There are " . $jsonProductTypeQuantity . " products in the array.<hr/>";

  global $italianIcePID, $sugarFreeItalianIcePID, $creamIcePID, $custardPID, $gelatiPID, $mistoPID, $blendiniPID, $milkshakePID, $frozenLemonadePID, $toppingPID, $sundaePID;
  global $italianIceFlavs, $sugarFreeItalianIceFlavs, $creamIceFlavs, $custardFlavs, $gelatiFlavs, $mistoFlavs, $blendiniFlavs, $milkshakeFlavs, $frozenLemonadeFlavs, $toppingFlavs, $sundaeFlavs, $hybridFlavs;
  global $italianIceSizes, $sugarFreeItalianIceSizes, $creamIceSizes, $custardSizes, $gelatiSizes, $mistoSizes, $blendiniSizes, $milkshakeSizes, $frozenLemonadeSizes, $toppingSizes, $sundaeSizes;
  global $italianIceNutri, $sugarFreeItalianIceNutri, $creamIceNutri, $custardNutri, $gelatiNutri, $mistoNutri, $blendiniNutri, $milkshakeNutri, $frozenLemonadeNutri, $toppingNutri, $sundaeNutri;

  $consoleLogging = false;
  foreach ($jsonGlobalProductArray as $key => $val) {
    if(is_array($val)) {
        switch($jsonGlobalProductArray[$key]['PRODUCTTYPENAME']) {
          case 'Italian Ice':
            if($consoleLogging) { echo '<h2>Italian Ice Flavors</h2>'; }
            $italianIceFlavs = $jsonGlobalProductArray[$key]['FLAVORS'];
            $italianIceFlavs = alphaSortFlavors($italianIceFlavs);
            $nutritionArray = $italianIceNutri[0]["nutr"];
            $italianIceFlavs = compareFlavorNutrition($italianIceFlavs, $nutritionArray, $consoleLogging);
            if($consoleLogging) { print_r($italianIceFlavs); }
            $italianIceSizes = $jsonGlobalProductArray[$key]['SIZES'];
            $italianIcePID = $jsonGlobalProductArray[$key]['PRODUCTTYPEID'];
            break;
          case 'Sugar-Free':
            if($consoleLogging) { echo '<h2>Sugar-Free Italian Ice Flavors</h2>'; }
            $sugarFreeItalianIceFlavs = $jsonGlobalProductArray[$key]['FLAVORS'];
            $sugarFreeItalianIceFlavs = alphaSortFlavors($sugarFreeItalianIceFlavs);
            $nutritionArray = $sugarFreeItalianIceNutri[0]["nutr"];
            $sugarFreeItalianIceFlavs = compareFlavorNutrition($sugarFreeItalianIceFlavs, $nutritionArray, $consoleLogging);
            if($consoleLogging) { echo print_r($sugarFreeItalianIceFlavs); }
            $sugarFreeItalianIceSizes = $jsonGlobalProductArray[$key]['SIZES'];
            $sugarFreeItalianIcePID = $jsonGlobalProductArray[$key]['PRODUCTTYPEID'];
            break;
          case 'Cream Ice':
            if($consoleLogging) { echo '<h2>Cream Ice Flavors</h2>'; }
            $creamIceFlavs = $jsonGlobalProductArray[$key]['FLAVORS'];
            $creamIceFlavs = alphaSortFlavors($creamIceFlavs);
            $nutritionArray = $creamIceNutri[0]["nutr"];
            $creamIceFlavs = compareFlavorNutrition($creamIceFlavs, $nutritionArray, $consoleLogging);
            if($consoleLogging) { print_r($creamIceFlavs); }
            $creamIceSizes = $jsonGlobalProductArray[$key]['SIZES'];
            $creamIcePID = $jsonGlobalProductArray[$key]['PRODUCTTYPEID'];
            break;
          case 'Custard':
            $custardFlavs = $jsonGlobalProductArray[$key]['FLAVORS'];
            $custardFlavs = alphaSortFlavors($custardFlavs);
            $nutritionArray = $custardNutri[0]["nutr"];
            $custardFlavs = compareFlavorNutrition($custardFlavs, $nutritionArray, $consoleLogging);
            $custardSizes = $jsonGlobalProductArray[$key]['SIZES'];
            /* remove unreported sizes from size array used to populate selectors (referral by json array index) */
            /* includes Extra Custard Upcharge, Kids Sundae, Regular Sundae, Large Sundae, Waffle Cone, Waffle Bowl */
            $sizeIDRemovalList = array(9,8,7,3,2,1,0);
            foreach($sizeIDRemovalList as $arrayElementToRemove) {
              unset($custardSizes[$arrayElementToRemove]);
            }
            //reset keys to array since items were deleted
            sort($custardSizes);
            $custardPID = $jsonGlobalProductArray[$key]['PRODUCTTYPEID'];
            break;
          case 'Gelati':
            $gelatiSizes = $jsonGlobalProductArray[$key]['SIZES'];
            $gelatiPID = $jsonGlobalProductArray[$key]['PRODUCTTYPEID'];
            break;
          case 'Misto':
            $mistoSizes = $jsonGlobalProductArray[$key]['SIZES'];
            $mistoPID = $jsonGlobalProductArray[$key]['PRODUCTTYPEID'];
            break;
          case 'Blendini':
            $blendiniSizes = $jsonGlobalProductArray[$key]['SIZES'];
            $blendiniPID = $jsonGlobalProductArray[$key]['PRODUCTTYPEID'];
            break;
          case 'Milkshakes':
            if($consoleLogging) { echo '<h2>Milkshake Flavors</h2>'; }
            $milkshakeFlavs = $jsonGlobalProductArray[$key]['FLAVORS'];
            $milkshakeFlavs = alphaSortFlavors($milkshakeFlavs);
            $nutritionArray = $milkshakeNutri[0]["nutr"];
            $milkshakeFlavs = compareFlavorNutrition($milkshakeFlavs, $nutritionArray, $consoleLogging);
            if($consoleLogging) { print_r($milkshakeFlavs); }
            $milkshakeSizes = $jsonGlobalProductArray[$key]['SIZES'];
            $milkshakePID = $jsonGlobalProductArray[$key]['PRODUCTTYPEID'];
            break;
          case 'Frozen Lemonade':
            $frozenLemonadeFlavs = $jsonGlobalProductArray[$key]['FLAVORS'];
            $frozenLemonadeFlavs = alphaSortFlavors($frozenLemonadeFlavs);
            $frozenLemonadeSizes = $jsonGlobalProductArray[$key]['SIZES'];
            $frozenLemonadePID = $jsonGlobalProductArray[$key]['PRODUCTTYPEID'];
            break;
          case 'Topping':
            if($consoleLogging) { echo '<h2>Toppings</h2>'; }
            $toppingFlavs = $jsonGlobalProductArray[$key]['FLAVORS'];
            $toppingFlavs = alphaSortFlavors($toppingFlavs);
            $nutritionArray = $toppingNutri[0]["nutr"];
            $toppingFlavs = compareFlavorNutrition($toppingFlavs, $nutritionArray, $consoleLogging);
            if($consoleLogging) { print_r($toppingFlavs); }
            $toppingSizes = $jsonGlobalProductArray[$key]['SIZES'];
            $toppingPID = $jsonGlobalProductArray[$key]['PRODUCTTYPEID'];
            break;
          case 'Sundaes':
            $sundaeFlavs = $jsonGlobalProductArray[$key]['FLAVORS'];
            $sundaeFlavs = alphaSortFlavors($sundaeFlavs);
            $sundaeSizes = $jsonGlobalProductArray[$key]['SIZES'];
            $sundaePID = $jsonGlobalProductArray[$key]['PRODUCTTYPEID'];
            break;
          default:
            break;
        } //end switch statement to set keys
    }
  } /* end foreach, key lookup & detection */
  $hybridFlavs = alphaSortFlavors( array_merge($italianIceFlavs, $sugarFreeItalianIceFlavs, $creamIceFlavs) );
  $gelatiFlavs = $hybridFlavs;
  $mistoFlavs = $hybridFlavs;

  $noIce = array();
  $noIce["FLAVORID"] = 'NULL';
  $noIce["FLAVORNAME"] = "zAll Custard Blendini";
  $hybridFlavs[] = $noIce;
  $blendiniFlavs = alphaSortFlavors( $hybridFlavs );
}; //end function loadJSON()



/*
* Function: generateSecondaryMenu($type,$menuFormat)
* description: resorts an array of flavor names and ids alphabetically
* input arguments:  $type- a string representation of the product group, either 'custard' or 'toppings'
*                   $menuFormat- a string depicting 'standard' or 'mobile'
* returns:  html formatted content for secondary menu
*
*/
function generateSecondaryMenu($type,$menuFormat) {
  $results = null;
  global $custardFlavs, $toppingFlavs;
  if($menuFormat == 'standard') {
    if($type == 'custard') {
        $custardFlavorCount = count($custardFlavs);
        $custardFlavorSelectorHTML = '';
        $custardFlavorSelectorHTML .= '<div class="custardFlavorSelectors">Select your flavor of custard.';

        $flavorColumnCounter = 1;
        for ($flavorCounter = 0; $flavorCounter < $custardFlavorCount; $flavorCounter++) {

          $flavorID = $custardFlavs[$flavorCounter]['FLAVORID'];
          $flavorName =  htmlentities ($custardFlavs[$flavorCounter]['FLAVORNAME'], ENT_QUOTES, 'UTF-8');
          // for loop through and populate each flavor left to right, one row at a time to a max of 7 columns per row
          $flavorName = str_replace( "?", "&#0174;", $flavorName ); //encode registered trademark symbols
          $flavorName = strtolower($flavorName);
          $flavorName = ucwords($flavorName);

          $cleanFlavorName = preg_replace('/\s+/', '', $flavorName);
          $cleanFlavorName = preg_replace("/[^a-zA-Z0-9]+/", "", html_entity_decode($flavorName, ENT_QUOTES));
          $cleanFlavorName = str_replace("&#0174;", '', $cleanFlavorName );

          $cleanProductType = 'Custard';

          $thumbnailPath = "/wp-content/gallery/products/";
          $thumbnailURL = $thumbnailPath . $cleanProductType . "/" .  $cleanProductType . "_" . $cleanFlavorName . ".png";

          if($flavorCounter % 2 == 0) {
              $custardFlavorSelectorHTML .= '<div class="custardFlavorRow">';
          }

          $indiv_flavor = '<div class="custardFlavorSelector custardFlavor flavor-' . $flavorID . ' flav-Custard-' . $flavorCounter. '" style="position:relative;">';
          $indiv_flavor .= "<a href='javascript:void(0);' class='toggleSecondFlavorSelection' >";
          $indiv_flavor .= "<img src=" . $thumbnailURL . " alt='" . $flavorName . "' />";
          $indiv_flavor .= "<span>" . $flavorName . "</span>";
          $indiv_flavor .= "</a>";
          $indiv_flavor .= "</div>"; //<!--/.custardFlavorSelector.custardFlavor-->

          $custardFlavorSelectorHTML .= $indiv_flavor;

          // if there are more than the max number of flavors to display on a row, start a new row
          if($flavorCounter % 2 == 1) {
            $custardFlavorSelectorHTML .= '</div>'; //<!--/.custardFlavorRow-->
            $flavorColumnCounter = 1;
          } else {
            $flavorColumnCounter++;
          }

        } //next iteration

        // for loop ends; close out the custardFlavorSelectors div
        $custardFlavorSelectorHTML .= "</div>"; //<!--/.custardFlavorSelectors-->

        $results = $custardFlavorSelectorHTML;
    }  //end if 'custard'

    else if($type == 'toppings') {
        $toppingCount = count($toppingFlavs);
        $toppingSelectorHTML = '';

        $toppingSelectorHTML .= '<div class="toppingSelectors">';
        $toppingSelectorHTML .= '<select class="mixInSelector">';
        $toppingSelectorHTML .= '<option value="tbd" selected="selected">Select your mix-in</option>';

        for ($toppingCounter = 0; $toppingCounter < $toppingCount; $toppingCounter++) {
          $flavorID = $toppingFlavs[$toppingCounter]['FLAVORID'];
          $flavorName =  htmlentities ($toppingFlavs[$toppingCounter]['FLAVORNAME'], ENT_QUOTES, 'UTF-8');
          // for loop through and populate each flavor left to right, one row at a time to a max of 7 columns per row
          $flavorName = str_replace( "?", "&#0174;", $flavorName ); //encode registered trademark symbols
          $flavorName = strtolower($flavorName);
          $flavorName = ucwords($flavorName);

          $cleanFlavorName = preg_replace('/\s+/', '', $flavorName);
          $cleanFlavorName = preg_replace("/[^a-zA-Z0-9]+/", "", html_entity_decode($flavorName, ENT_QUOTES));
          $cleanFlavorName = str_replace("&#0174;", '', $cleanFlavorName );

          $cleanProductType = 'Topping';


          $indiv_flavor = '<option class="toppingSelector toppingFlavor flav-Topping-' . $toppingCounter . '" value="topping-' . $flavorID . '">';
          $indiv_flavor .= $flavorName;
          $indiv_flavor .= "</option>"; //<!--/.toppingSelector.toppingFlavor-->

          $toppingSelectorHTML .= $indiv_flavor;


        } //next iteration

        // for loops end; close out the toppingSelectors div
        $toppingSelectorHTML .= "</select></div>"; //<!--/.toppingSelectors-->
        $results = $toppingSelectorHTML;
    } //end if 'toppings'
  }
  // generate mobile formatted selects
  else {
    global $custardPID, $toppingPID;
    if($type == 'custard') {
        $custardFlavorSelectorHTML = '<div id="custardSelector' . $custardPID . '" class="selectorWrapper custardSelector">';
        $custardFlavorSelectorHTML .= "<label for='custards'>Choose a Custard Flavor</label>";
        $custardFlavorSelectorHTML .= "<select name='custards' class='custardSelect'>";
        $custardFlavorSelectorHTML .= '<option value="tbd" selected="selected">Choose a flavor</option>';

        for ($flavorCounter = 0; $flavorCounter < count($custardFlavs); $flavorCounter++) {
          $flavorID = $custardFlavs[$flavorCounter]['FLAVORID'];
          $flavorName =  htmlentities ($custardFlavs[$flavorCounter]['FLAVORNAME'], ENT_QUOTES, 'UTF-8');
          // for loop through and populate each flavor left to right, one row at a time to a max of 7 columns per row
          $flavorName = str_replace( "?", "&#0174;", $flavorName ); //encode registered trademark symbols
          $flavorName = strtolower($flavorName);
          $flavorName = ucwords($flavorName);

          $cleanFlavorName = preg_replace('/\s+/', '', $flavorName);
          $cleanFlavorName = preg_replace("/[^a-zA-Z0-9]+/", "", html_entity_decode($flavorName, ENT_QUOTES));
          $cleanFlavorName = str_replace("&#0174;", '', $cleanFlavorName );

          $cleanProductType = 'Custard';

          $thumbnailPath = "/wp-content/gallery/products/";
          $thumbnailURL = $thumbnailPath . $cleanProductType . "/" .  $cleanProductType . "_" . $cleanFlavorName . ".png";

          $custardFlavorSelectorHTML .= '<option value="' . $thumbnailURL . '" class="' . $flavorID . '">' . $flavorName . '</option>';

        } //next iteration

        // for loop ends; close out the custardFlavorSelectors div
        $custardFlavorSelectorHTML .= "</select>"; //<!--/.custardSelect-->
        $custardFlavorSelectorHTML .= '</div>'; //<!--/#custardSelector .selectorWrapper-->

        $results = $custardFlavorSelectorHTML;
    }  //end if 'custard'

    else if($type == 'toppings') {
        $toppingCount = count($toppingFlavs);

        $toppingSelectorHTML = '<div id="toppingSelector' . $toppingPID . '" class="selectorWrapper toppingSelector">';
        $toppingSelectorHTML .= "<label for='toppings'>Choose a mix-in</label>";
        $toppingSelectorHTML .= "<select name='toppings' class='toppingSelect'>";

        $toppingSelectorHTML .= '<option value="tbd" selected="selected">Choose a mix-in</option>';
        $toppingSelectorHTML .= '<option value="topping-none">None</option>';

        for ($toppingCounter = 0; $toppingCounter < $toppingCount; $toppingCounter++) {
          $flavorID = $toppingFlavs[$toppingCounter]['FLAVORID'];
          $flavorName =  htmlentities ($toppingFlavs[$toppingCounter]['FLAVORNAME'], ENT_QUOTES, 'UTF-8');
          // for loop through and populate each flavor left to right, one row at a time to a max of 7 columns per row
          $flavorName = str_replace( "?", "&#0174;", $flavorName ); //encode registered trademark symbols
          $flavorName = strtolower($flavorName);
          $flavorName = ucwords($flavorName);

          $cleanFlavorName = preg_replace('/\s+/', '', $flavorName);
          $cleanFlavorName = preg_replace("/[^a-zA-Z0-9]+/", "", html_entity_decode($flavorName, ENT_QUOTES));
          $cleanFlavorName = str_replace("&#0174;", '', $cleanFlavorName );

          $cleanProductType = 'Topping';

          $thumbnailPath = "/wp-content/gallery/products/";
          $thumbnailURL = $thumbnailPath . $cleanProductType . "/" .  $cleanProductType . "_" . $cleanFlavorName . ".png";

          $toppingSelectorHTML .= '<option value="' . $thumbnailURL . '" class="' . $flavorID . '">' . $flavorName . '</option>';

        } //next iteration

        // for loops end; close out the toppingSelectors div
        $toppingSelectorHTML .= "</select></div>"; //<!--/.toppingSelectors-->
        $results = $toppingSelectorHTML;
    } //end if 'toppings'
  }
  return $results;
}; // end generateSecondaryMenu()



/*
* Function: createMenuHTML($productClassification,$productType,$productID,$flavorsArray,$sizeArray)
* description: generates HTML for each product's variety of flavors/sizes
* input arguments:  $productClassification
*                   $productType
*                   $productID
*                   $flavorsArray
*                   $sizeArray
* returns:  html formatted content for primary flavor menu
*
*/
function createMenuHTML($productClassification,$productType,$productID,$flavorsArray,$sizeArray) {

  $matchedProductFlavorQuantity = count($flavorsArray);
  $matchedProductSizeQuantity = count($sizeArray);
/*debug*/ //echo "<script>console.log('There are " . $matchedProductSizeQuantity . " sizes/classifications and " . $matchedProductFlavorQuantity . " flavors for the " . $productType . " product type.')</script>";
  $flavorTotal = $matchedProductFlavorQuantity; // set to count of flavor records
  $flavorCounterB = 1;

  $htmlResult = null;
  $htmlResult = '<div id="slideshow' . $productID . '-slide1" class="content-slide">';
  $htmlResult .= "<div class='flavorRow topFlavorRow'>";

  for ($flavorCounter = 1; $flavorCounter <= $flavorTotal; $flavorCounter++) {
//  echo '<script>console.log("' . $flavorCounter . '");</script>';
    $flavorID = $flavorsArray[$flavorCounter-1]['FLAVORID'];
    $flavorName =  htmlentities ($flavorsArray[$flavorCounter-1]['FLAVORNAME'], ENT_QUOTES, 'UTF-8');
    // for loop through and populate each flavor left to right, one row at a time to a max of 7 columns per row
    $flavorName = str_replace( "?", "&#0174;", $flavorName ); //encode registered trademark symbols
    $flavorName = strtolower($flavorName);
    $flavorName = ucwords($flavorName);

    $friendlyFlavorName = preg_replace('/\s+/', '', $flavorName);
    $friendlyFlavorName = preg_replace("/[^a-zA-Z0-9]+/", "", html_entity_decode($flavorName, ENT_QUOTES));
    $friendlyFlavorName = str_replace("&#0174;", '', $friendlyFlavorName );

    if( $productID == '6') {
      $productLoopType = 'ItalianIce'; /* show water ice photos in water ice selector */
    } else if( $productID == '4' ) {
      $productLoopType = 'Misto'; /* show misto photos in water ice selector */
    } else if( $productID == '3' ) {
      $productLoopType = 'Gelati'; /* show gelati photos in water ice selector */
    } else {
      $productLoopType = $productType;
    }
    $friendlyProductType = preg_replace('/\s+/', '', $productLoopType);

    $thumbnailPlaceholder = "/wp-content/themes/ritas/library/images/spinner.gif";
    $thumbnailPath = "/wp-content/gallery/products/";
    $thumbnailURL = $thumbnailPath . $friendlyProductType . "/" .  $friendlyProductType . "_" . $friendlyFlavorName . ".png";
    $indiv_flavor = "<div class='flavor flavor-" . $flavorID;
    if($flavorCounterB >=5 ) {
      $indiv_flavor .= " rightAlignNutrition";
    } else {
      $indiv_flavor .= " leftAlignNutrition";
    }
    $indiv_flavor .= "' id='flav-" . $productType . '-' . $flavorCounter. "' style='position:relative;'>";
    $indiv_flavor .= "<a href='javascript:void(0);' class='toggleNutritionInfo' >";
    $indiv_flavor .= "<img src=" . $thumbnailURL . " alt='" . $flavorName . "' />";
    $indiv_flavor .= "<p>" . $flavorName . "</p>";
    $indiv_flavor .= "</a>";
    $indiv_flavor .= "<div class='nutritionInfoOverlay'>";
    $indiv_flavor .= "<div class='selectorBlock'>";
    $indiv_flavor .= "<div class='servingSizeSelectors'>";
    $flavorDivID = '';
    for ($sizeCounter = 0; $sizeCounter < $matchedProductSizeQuantity; $sizeCounter++) {
      $sizeID = $sizeArray[$sizeCounter]['PRODUCTSIZEID'];
      $isDefaultSizeClass = "";
      $sizeLabel =  htmlentities ($sizeArray[$sizeCounter]['PRODUCTSIZENAME'], ENT_QUOTES, 'UTF-8');
      if( strpos($sizeLabel, 'Regular') !== FALSE ) {
        $isDefaultSizeClass = " defaultSize";
      }
      $flavorDivID = $productID . "-" . $flavorID . "-" . $sizeID;
      $indiv_flavor .= "<div class='servingSizeSelector" . $isDefaultSizeClass ."' id='" . $flavorDivID . "'>" . $sizeLabel . "</div>"; //closeout .servingSizeSelector
    } // end for loop to cycle through size classifications
    $indiv_flavor .= "</div>"; //closeout .servingSizeSelectors

    if($productClassification == 'hybrid') {
        $indiv_flavor .= "<div class='secondaryFlavorSelectors' style='display:none;visibility:hidden;'>";
        $indiv_flavor .= "<div class='secondaryFlavorSelector selectedSecondFlavor'></div>";
        $indiv_flavor .= "<div class='secondaryFlavorSelector resetSecondFlavor'><a class='chooseAnotherFlavor' onClick='chooseAnotherFlavor(";
        $indiv_flavor .= '"' . $flavorDivID . '"';
        $indiv_flavor .= ");'>Choose Another<br/>Custard Flavor</a></div>";
        $indiv_flavor .= "</div>"; //closeout .secondaryFlavorSelectors
        $indiv_flavor .= "<div class='tertiaryFlavorSelectors' style='display:none;visibility:hidden;'>";
        $indiv_flavor .= "<div class='tertiaryFlavorSelector selectedTopping'></div>";
        $indiv_flavor .= "<div class='tertiaryFlavorSelector resetTopping'><a class='chooseAnotherTopping' onClick='chooseAnotherTopping(";
        $indiv_flavor .= '"' . $flavorDivID . '"';
        $indiv_flavor .= ");'>Choose Another<br/>Topping</a></div>";
        $indiv_flavor .= "</div>"; //closeout .tertiaryFlavorSelectors
    }

    $indiv_flavor .= "</div>"; //closeout .selectorBlock
    $indiv_flavor .= "<div class='nutritionInfoThumb'>";
    $indiv_flavor .= "<img src=" . $thumbnailURL . "  alt='" . $flavorName . "' />";
    $indiv_flavor .= "</div>"; //closeout .nutritionInfoThumb
    $indiv_flavor .= "<div class='right-block'>";

    if($productClassification == 'hybrid') {
        //append secondary menu selection list
        $indiv_flavor .= generateSecondaryMenu('custard','standard');
        if($productID == '6') {
          //append tertiary menu selection list
          $indiv_flavor .= generateSecondaryMenu('toppings','standard');
        }
    }

    $indiv_flavor .= "<div class='nutritionInfo'></div>"; //closeout .nutritionInfo
    $indiv_flavor .= "</div>"; //closeout .right-block
    $indiv_flavor .= "</div>"; //closeout .nutritionInfoOverlay
    $indiv_flavor .= "</div>";
    $htmlResult .= $indiv_flavor;

    // if there are more than the max number of flavors to display on a row, start a new row
    if( ($flavorCounter % 7 == 0) && ($flavorCounter < $flavorTotal) && ($flavorCounter % 14 > 0) ) {
      $htmlResult .= "</div>"; // close out .flavorRow div
      $htmlResult .= "<div class='flavorRow bottomFlavorRow'>";
      $flavorCounterB = 1;
    }
    // if there are more than the max number of flavors to display in the div, start a new div
    else if ( ($flavorCounter % 14 == 0) && ($flavorCounter < $flavorTotal) ) {
      $htmlResult .= "</div>"; // close out .flavorRow div
      $htmlResult .= "</div>"; // close out .content-slide div
      $htmlResult .= "<div id='slideshow" . $productID . "-slide" . ($flavorCounter / 14 + 1) . "' class='content-slide'>";
      $htmlResult .= "<div class='flavorRow topFlavorRow'>";
      $flavorCounterB = 1;
    }
    else {
      $flavorCounterB ++;
    }
  } //next iteration of for loop


  // close out the row of flavors and the last slide
  $htmlResult .= "</div>"; // close out .flavorRow div

  $htmlResult .= '</div>'; // close out .content-slide div

  return $htmlResult;
}; // end createMenuHTML()



/*
* Function: createMobileMenuHTML($productClassification,$productType,$productID,$flavorsArray,$sizeArray)
* description: generates HTML for each product's variety of flavors/sizes
* input arguments:  $productClassification
*                   $productType
*                   $productID
*                   $flavorsArray
*                   $sizeArray
* returns:  html formatted content for mobile version of primary flavor menu
*
*/
function createMobileMenuHTML($productClassification,$productType,$productID,$flavorsArray,$sizeArray) {
  $htmlResult = '<div id="product-' . $productID . '-menu" class="mobileNutritionWidget">';
  $matchedProductFlavorQuantity = count($flavorsArray);
  $matchedProductSizeQuantity = count($sizeArray);
  $flavorTotal = $matchedProductFlavorQuantity; // set to count of flavor records

  $htmlResult .= '<div id="flavorSelector' . $productID . '" class="selectorWrapper flavorSelector">';
  $text = "flavor";
  if($productType=="toppings") {
    $text = "topping";
  }
  $htmlResult .= "<label for='flavors'>Choose a " . $text . "</label>";

  $htmlResult .= "<select name='flavors' class='flavorSelect'>";
  $htmlResult .= '<option value="tbd" selected="selected">Choose a ' . $text . '</option>';

  for ($flavorCounter = 1; $flavorCounter <= $flavorTotal; $flavorCounter++) {
    $flavorID = $flavorsArray[$flavorCounter-1]['FLAVORID'];
    $flavorName =  htmlentities ($flavorsArray[$flavorCounter-1]['FLAVORNAME'], ENT_QUOTES, 'UTF-8');
    $flavorName = str_replace( "?", "&#0174;", $flavorName ); //encode registered trademark symbols
    $flavorName = strtolower($flavorName);
    $flavorName = ucwords($flavorName);

    $friendlyFlavorName = preg_replace('/\s+/', '', $flavorName);
    $friendlyFlavorName = preg_replace("/[^a-zA-Z0-9]+/", "", html_entity_decode($flavorName, ENT_QUOTES));
    $friendlyFlavorName = str_replace("&#0174;", '', $friendlyFlavorName );

    if( $productID == '6') {
      $productLoopType = 'ItalianIce'; /* show water ice photos in water ice selector */
    } else if( $productID == '4' ) {
      $productLoopType = 'Misto'; /* show misto photos in water ice selector */
    } else if( $productID == '3' ) {
      $productLoopType = 'Gelati'; /* show gelati photos in water ice selector */
    } else {
      $productLoopType = $productType;
    }
    $friendlyProductType = preg_replace('/\s+/', '', $productLoopType);

    $thumbnailPlaceholder = "/wp-content/themes/ritas/library/images/spinner.gif";
    $thumbnailPath = "/wp-content/gallery/products/";
    $thumbnailURL = $thumbnailPath . $friendlyProductType . "/" .  $friendlyProductType . "_" . $friendlyFlavorName . ".png";

    $htmlResult .= '<option value="' . $thumbnailURL . '" class="' . $flavorID . '">' . $flavorName . '</option>';

  } //next iteration of for loop


  // close out the row of flavors and the last slide
  $htmlResult .= "</select>"; // closeout .flavorSelect
  $htmlResult .= '</div>'; // closeout #flavorSelector .selectorWrapper


  if($productClassification == 'hybrid') {
      // show custards
      $htmlResult .= generateSecondaryMenu('custard','mobile');
      if($productType == 'Blendini') {
        $htmlResult .= generateSecondaryMenu('toppings','mobile');
      }
  }

  $htmlResult .= '<div id="sizeSelector' . $productID . '" class="selectorWrapper sizeSelector">';
    $htmlResult .= "<label for='sizes'>Choose a size</label>";
    $htmlResult .= "<select name='sizes' class='sizeSelect'>";
      for ($sizeCounter = 0; $sizeCounter < $matchedProductSizeQuantity; $sizeCounter++) {
        $sizeID = $sizeArray[$sizeCounter]['PRODUCTSIZEID'];
        $isDefaultSizeClass = "";
        $sizeLabel =  htmlentities ($sizeArray[$sizeCounter]['PRODUCTSIZENAME'], ENT_QUOTES, 'UTF-8');
        $isDefaultSizeClass = '';
        if( strpos($sizeLabel, 'Regular') !== FALSE ) {
          $isDefaultSizeClass = 'selected="selected"';
        }
        $htmlResult .= "<option " . $isDefaultSizeClass . " value='" . $sizeID . "'>" . $sizeLabel . "</option>";
      } // end for loop to cycle through size classifications
    $htmlResult .= "</select>"; // closeout .sizeSelect
  $htmlResult .= '</div>'; // closeout #sizeSelector .selectorWrapper

  $htmlResult .= '<a class="mobileProductMenuLink" href="/">Back to Cool Treats</a>';
  return $htmlResult;
}; // end createMobileMenuHTML()



/*
* Function: syncMenu($productID,$contentHTML,$menuType)
* description: syncs/writes HTML content to "product-menu" post type corresponding to each product
* input arguments:  $productID
*                   $contentHTML
*                   $menuType- a string depicting 'standard' or 'mobile'
*
*/
function syncMenu($productID,$contentHTML,$menuType) {
  switch($productID) {
    case 1:
    /* italianIce */
      $wp_post_ID = '4328';
      $wp_post_mobileID = '4339';
      break;
    case 8:
    /* sugarFreeItalianIce */
      $wp_post_ID = '4329';
      $wp_post_mobileID = '4341';
      break;
    case 2:
    /* creamIce */
      $wp_post_ID = '4330';
      $wp_post_mobileID = '4342';
      break;
    case 5:
    /* custard */
      $wp_post_ID = '4332';
      $wp_post_mobileID = '4343';
      break;
    case 4:
    /* misto */
      $wp_post_ID = '4333';
      $wp_post_mobileID = '4344';
      break;
    case 3:
    /* gelati */
      $wp_post_ID = '4334';
      $wp_post_mobileID = '4345';
      break;
    case 6:
    /* blendini */
      $wp_post_ID = '4336';
      $wp_post_mobileID = '4346';
      break;
    case 45:
    /* milkshakes */
      $wp_post_ID = '4338';
      $wp_post_mobileID = '4348';
      break;
    case 55:
    /* toppings */
      $wp_post_ID = '4337';
      $wp_post_mobileID = '4347';
      break;
    default:
      break;
  }  // end switch

  require_once(ABSPATH . '/wp-load.php');
  global $wpdb;

  $table = 'wp_posts';
  $formatInsert = array ("%s", "%d");

  $data = array(
    "post_content" => $contentHTML,
    "post_modified" => date('Y-m-d H:i:s')
  );

  $lookupSQL = "SELECT * FROM " . $table . " WHERE ID = " . $wp_post_ID;
  $whereCondition = array( "ID" => $wp_post_ID );

  if($menuType == "mobile") {
      $lookupSQL = "SELECT * FROM " . $table . " WHERE ID = " . $wp_post_mobileID;
      $whereCondition = array( "ID" => $wp_post_mobileID );
  }

  $wpdb->get_row($lookupSQL);

  if( $wpdb->num_rows == 1 ) {
    $wpdb->update( $table, $data, $whereCondition, $format = null, $where_format = null );
  }

//  echo '<script>console.log("Post #' . $wp_post_ID . ' updated in wp database.");</script>';

}; //end syncMenu()



loadNutritionJSON();

loadJSON();

//echo "<hr/><h2>Italian Ice Primary Listing</h2>";
$result = createMenuHTML("indiv", "ItalianIce", $italianIcePID, $italianIceFlavs, $italianIceSizes);
syncMenu( $italianIcePID, $result, "standard");
//echo "<h2>Italian Ice Mobile Listing</h2>";
$result = createMobileMenuHTML("indiv", "ItalianIce", $italianIcePID, $italianIceFlavs, $italianIceSizes);
syncMenu( $italianIcePID, $result, "mobile");

//echo "<hr/><h2>Sugar-Free Italian Ice Primary Listing</h2>";
$result = createMenuHTML("indiv", "SugarFreeItalianIce", $sugarFreeItalianIcePID, $sugarFreeItalianIceFlavs, $sugarFreeItalianIceSizes);
syncMenu( $sugarFreeItalianIcePID, $result, "standard");
//echo "<h2>Sugar-Free Italian Ice Mobile Listing</h2>";
$result = createMobileMenuHTML("indiv", "SugarFreeItalianIce", $sugarFreeItalianIcePID, $sugarFreeItalianIceFlavs, $sugarFreeItalianIceSizes);
syncMenu( $sugarFreeItalianIcePID, $result, "mobile");

//echo "<hr/><h2>Cream Ice Primary Listing</h2>";
$result = createMenuHTML("indiv", "CreamIce", $creamIcePID, $creamIceFlavs, $creamIceSizes);
syncMenu( $creamIcePID, $result, "standard");
//echo "<h2>Cream Ice Mobile Listing</h2>";
$result = createMobileMenuHTML("indiv", "CreamIce", $creamIcePID, $creamIceFlavs, $creamIceSizes);
syncMenu( $creamIcePID, $result, "mobile");

//echo "<hr/><h2>Custard Primary Listing</h2>";
$result = createMenuHTML("indiv", "Custard", $custardPID, $custardFlavs, $custardSizes);
syncMenu( $custardPID, $result, "standard");
//echo "<h2>Custard Mobile Listing</h2>";
$result = createMobileMenuHTML("indiv", "Custard", $custardPID, $custardFlavs, $custardSizes);
syncMenu( $custardPID, $result, "mobile");

//echo "<hr/><h2>Misto Primary Listing</h2>";
$result = createMenuHTML("hybrid", "Misto", $mistoPID, $mistoFlavs, $mistoSizes);
syncMenu( $mistoPID, $result, "standard");
//echo "<h2>Misto Mobile Listing</h2>";
$result = createMobileMenuHTML("hybrid", "Misto", $mistoPID, $mistoFlavs, $mistoSizes);
syncMenu( $mistoPID, $result, "mobile");

//echo "<hr/><h2>Gelati Primary Listing</h2>";
$result = createMenuHTML("hybrid", "Gelati", $gelatiPID, $gelatiFlavs, $gelatiSizes);
syncMenu( $gelatiPID, $result, "standard");
//echo "<h2>Gelati Mobile Listing</h2>";
$result = createMobileMenuHTML("hybrid", "Gelati", $gelatiPID, $gelatiFlavs, $gelatiSizes);
syncMenu( $gelatiPID, $result, "mobile");

//echo "<hr/><h2>Blendini Primary Listing</h2>";
$result = createMenuHTML("hybrid", "Blendini", $blendiniPID, $blendiniFlavs, $blendiniSizes);
syncMenu( $blendiniPID, $result, "standard");
//echo "<h2>Blendini Mobile Listing</h2>";
$result = createMobileMenuHTML("hybrid", "Blendini", $blendiniPID, $blendiniFlavs, $blendiniSizes);
syncMenu( $blendiniPID, $result, "mobile");

//echo "<hr/><h2>Toppings Primary Listing</h2>";
$result = createMenuHTML("indiv", "Toppings", $toppingPID, $toppingFlavs, $toppingSizes);
syncMenu( $toppingPID, $result, "standard");
//echo "<h2>Toppings Mobile Listing</h2>";
$result = createMobileMenuHTML("indiv", "Toppings", $toppingPID, $toppingFlavs, $toppingSizes);
syncMenu( $toppingPID, $result, "mobile");

//echo "<hr/><h2>Milkshakes Primary Listing</h2>";
$result = createMenuHTML("indiv", "Milkshakes", $milkshakePID, $milkshakeFlavs, $milkshakeSizes);
syncMenu( $milkshakePID, $result, "standard");
//echo "<h2>Milkshakes Mobile Listing</h2>";
$result = createMobileMenuHTML("indiv", "Milkshakes", $milkshakePID, $milkshakeFlavs, $milkshakeSizes);
syncMenu( $milkshakePID, $result, "mobile");


?>