<?php
/*
Template Name: Manual Sync Page
Description:
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

				    <div id="main" class="first clearfix" role="main">

					    <?php if (have_posts()) : the_post(); ?>

					    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
						    <header class="article-header">

							    <h1 class="page-title"><?php the_title(); ?></h1>
						    </header> <!-- end article header -->

						    <section class="entry-content">
                  <?php the_content(); ?>
						    </section> <!-- end article section -->

						    <footer class="article-footer">
							    <p class="clearfix"><?php the_tags('<span class="tags">' . __('Tags:', 'ritastheme') . '</span> ', ', ', ''); ?></p>

						    </footer> <!-- end article footer -->

						    <?php comments_template(); ?>

					    </article> <!-- end article -->

					    <?php endif; ?>

            <?php require_once('product_menu_sync.php'); ?>

            <?php require_once('store_locator_sync.php'); ?>

    				</div> <!-- end #main -->

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>
