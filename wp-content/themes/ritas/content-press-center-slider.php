
    
    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix press-content press-center-slider'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
        
      <section class="slider-section">
          <div class="press-slider-image"> <?php the_post_thumbnail(array(620,420)); ?></div>                          
          <!-- Display the Title as a link to the Post's permalink. -->
          <div class="press-slider-info">
            <h3 class="press-slider-headline"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
          </div>
       
      </section> 
      
    </article> <!-- end article -->   
    

       
    

  