<?php

/*
*  File: store_locator_sync.php
*  description: This file contains the JSON/mySQL database sync function *      that should be run as a cron job
*  author: Anson W. Han, The Star Group
*/

//declare array of states with Ritas locations and array of all Ritas locations
$statesArray = array();
$convertedDataArray = array();

//declare cron status log message
$cronResultsMsg = "";


/*
*
* reads JSON data and syncs mySQL database
*/
function ritas_location_sync() {
  global $cronResultsMsg;
  parseLocationJSON();
  syncLocationDB();
  return $cronResultsMsg;
} /* end ritas_location_sync() */



/*
* Function: parseLocationJSON()
* description: reads JSON data and parses into PHP array
*/
function parseLocationJSON() {
  global $statesArray, $convertedDataArray, $cronResultsMsg;
  // establish variable to point to json location data file
  //$filepath3 = 'http://' . $_SERVER['SERVER_NAME'] . '/data/star_store_detail.txt';
  $filepath3 = 'http://www.ritasfranchises.com/vendors_gateway/data/star_store_detail.txt';

  $jsonString3 = file_get_contents($filepath3);
  $jsonString3 = utf8_decode($jsonString3);
  $jsonArray3 = json_decode($jsonString3, true);
  /*switch (json_last_error()) {
      case JSON_ERROR_NONE:
  //      echo ' - No errors';
          break;
      case JSON_ERROR_DEPTH:
          echo ' - Maximum stack depth exceeded';
          break;
      case JSON_ERROR_STATE_MISMATCH:
          echo ' - Underflow or the modes mismatch';
          break;
      case JSON_ERROR_CTRL_CHAR:
          echo ' - Unexpected control character found';
          break;
      case JSON_ERROR_SYNTAX:
          echo ' - Syntax error, malformed JSON';
          break;
      case JSON_ERROR_UTF8:
          echo ' - Malformed UTF-8 characters, possibly incorrectly encoded';
          break;
      default:
          echo ' - Unknown error';
          break;
  }
  */

  /* preformatted array has additional wrappers, assign product array without extra wrapper */
  $jsonGlobalStoreArray3 = $jsonArray3['STORES'];

  $jsonStoreQuantity3 = count($jsonGlobalStoreArray3);
  $cronResultsMsg = "parseLocationJSON has detected " . $jsonStoreQuantity3 . " stores in the JSON array." . "\n\n";


  /*
  *  for loop below parses the JSON data fore each location and
  *  forms a php array; it also concatenates the store hours for HTML output
  */
  for($i=0; $i<$jsonStoreQuantity3; $i++) {
    $storeDataArray = $jsonGlobalStoreArray3[$i];
    $storeID = $storeDataArray['INTRITASSTOREID'];
    $storeNameID = $storeDataArray['VCHSTORENAME'] . " (" . $storeDataArray['INTRITASSTOREID'] . ")";
    $storeAddr1 = $storeDataArray['VCHSTORESTREETADDRESS1'];
    $storeAddr2 = $storeDataArray['VCHSTORESTREETADDRESS2'];
    $storeCity = $storeDataArray['VCHSTORECITY'];
    $storeState = $storeDataArray['VCHSTORESTATE'];
    $storeZip = $storeDataArray['INTSTOREZIPCODE'];
    $storeCountry = $storeDataArray['VCHCOUNTRY'];
    $storeLatitude = $storeDataArray['VCHSTORELATITUDE'];
    $storeLongitude = $storeDataArray['VCHSTORELONGITUDE'];
    $storeURL = $storeDataArray['VCHSTORELINKURL'];
    $storeFacebookURL = $storeDataArray['VCHFACEBOOKURL'];
    $storeTwitterURL = $storeDataArray['VCHTWITTERURL'];
    $storePhone = $storeDataArray['VCHSTOREPHONENUMBER'];

    $storeCoolAlerts = $storeDataArray['BITFODPART'];
    $storeCookieSandwiches = $storeDataArray['BITCOOKIESANDWICHPACK'];
    $storeMobileCart = $storeDataArray['MOBILECARTAVAILABLE'];
    $storeFrozenCustardCakes = $storeDataArray['BITFROZENCAKES'];
    $storeCoolCatering = $storeDataArray['BITCATERING'];
    $storeComingSoon = $storeDataArray['BITCOMINGSOON'];

    $storeHours = "";
    for($dow = 0; $dow < 7; $dow++) {
      switch($dow) {
        case 0:
          $day = "Mon";
          break;
        case 1:
          $day = "Tues";
          break;
        case 2:
          $day = "Wed";
          break;
        case 3:
          $day = "Thur";
          break;
        case 4:
          $day = "Fri";
          break;
        case 5:
          $day = "Sat";
          break;
        default:
          $day = "Sun";
          break;
      }
      if( array_key_exists('HOURS', $storeDataArray) && array_key_exists($dow, $storeDataArray['HOURS']) ) {
        if( $storeDataArray['HOURS'][$dow]['BITCLOSED'] == 0 ) {
          $storeHours .= "<div class='dow'>" . $day . "</div><div class='hrs'>" . $storeDataArray['HOURS'][$dow]['OPEN'] . " - " . $storeDataArray['HOURS'][$dow]['CLOSE'] . "</div>";
        } else {
          $storeHours .= "<div class='dow'>" . $day . "</div><div class='hrs'>" . "Closed" . "</div>";
        } /* end if-else for store hours HTML output */
      } /* end if(array_key_exists) */
    }
    $storeDescription = "";
    if($storeDataArray['VCHSEASONALITY'] == "Seasonal") {
      $storeDescription = "This is a seasonal location. ";
      if( ($storeDataArray['DTESEASONALOPENDATE'] <> "") && ($storeDataArray['DTESEASONALCLOSEDATE'] <> "") ) {
        $storeDescription .= "The store is open between " . str_replace(" 00:00:00", "", $storeDataArray['DTESEASONALOPENDATE']) . " and " . str_replace(" 00:00:00", "", $storeDataArray['DTESEASONALCLOSEDATE']) . ".";
      }
      else if ($storeDataArray['DTESEASONALOPENDATE'] <> "") {
        $storeDescription .= "The store opens as of " . str_replace(" 00:00:00", "", $storeDataArray['DTESEASONALOPENDATE']) . ".";
      }
      else {
        $storeDescription .= "The store closes as of " . str_replace(" 00:00:00", "", $storeDataArray['DTESEASONALCLOSEDATE']) . ".";
      }
    }


    /* conditionally generate list of tags to include */
    $storeTags = null;
    if( ($storeCoolAlerts === true) || ($storeCoolAlerts == "false")  || ($storeCoolAlerts == "1") || ($storeCoolAlerts == 1) ) {
        if($storeTags != null) {
          $storeTags .= ",Cool Alerts";
        } else {
          $storeTags .= "Cool Alerts";
        }
    }
    if( ($storeCookieSandwiches === true) || ($storeCookieSandwiches == "false") || ($storeCookieSandwiches == "1") || ($storeCookieSandwiches == 1) ) {
        if($storeTags != null) {
          $storeTags .= ",Custard Cookie Sandwiches";
        } else {
          $storeTags .= "Custard Cookie Sandwiches";
        }
    }
    if( ($storeMobileCart === true) || ($storeMobileCart == "false")  || ($storeMobileCart == "1") || ($storeMobileCart == 1) ) {
        if($storeTags != null) {
          $storeTags .= ",Mobile Cart";
        } else {
          $storeTags .= "Mobile Cart";
        }
    }
    if( ($storeFrozenCustardCakes === true) || ($storeFrozenCustardCakes == "false")  || ($storeFrozenCustardCakes == "1") || ($storeFrozenCustardCakes == 1)  ) {
        if($storeTags != null) {
          $storeTags .= ",Frozen Custard Cakes";
        } else {
          $storeTags .= "Frozen Custard Cakes";
        }
    }
    if( ($storeCoolCatering === true) || ($storeCoolCatering == "false")  || ($storeCoolCatering == "1") || ($storeCoolCatering == 1) ) {
        if($storeTags != null) {
          $storeTags .= ",Cool Catering";
        } else {
          $storeTags .= "Cool Catering";
        }
    }
    if( ($storeComingSoon === true) || ($storeComingSoon == "false")  || ($storeComingSoon == "1") || ($storeComingSoon == 1) ) {
        if($storeTags != null) {
          $storeTags .= ",Coming Soon";
        } else {
          $storeTags .= "Coming Soon";
        }
    }
    /* debug */ /* echo '<script>console.log("' . $storeNameID . ' tags: ' . $tags . '");</script>'; */

    /* Create a single array of location data to be appended to PHP Array of locations */
    $convertedStoreDataArray =  array(
      "ritasStoreID" => $storeID,
      "NameID" => $storeNameID,
      "Addr1" => $storeAddr1,
      "Addr2" => $storeAddr2,
      "City" => $storeCity,
      "State" => $storeState,
      "Zip" => $storeZip,
      "Country" => $storeCountry,
      "Lat" => $storeLatitude,
      "Lon" => $storeLongitude,
      "URL" => $storeURL,
      "Facebook" => $storeFacebookURL,
      "Twitter" => $storeTwitterURL,
      "Phone" => $storePhone,
      "Hours" => $storeHours,
      "Description" => $storeDescription,
      "Tags" => $storeTags
    );

    if(!(in_array($storeState, $statesArray))) {
      array_push($statesArray, $storeState);
    }

    array_push($convertedDataArray, $convertedStoreDataArray);


/*
  echo "<ul>";
    echo "<li>";
    echo $storeID . "<br/>";
    echo $storeNameID . "<br/>";
    echo $storeAddr1 . "<br/>" . $storeAddr2 . "<br/>";
    echo $storeCity . ", " . $storeState . "  " . $storeZip . "   " . $storeCountry;
    echo "<ul>";
    echo "<li>URL: " . $storeURL . "</li>";
    echo "<li>Facebook: " . $storeFacebookURL . "</li>";
    echo "<li>Twitter: " . $storeTwitterURL . "</li>";
    echo "</ul>";
    echo "Phone: " . $storePhone . "<br/>";
    echo "Hours: " . $storeHours . "<br/>";
    echo "Description: " . $storeDescription . "<br/>";
    echo "Tags: " . $storeTags;
    echo "</li>";
  echo "</ul><br/><br/>";
*/

  }
  sort($statesArray);

  $cronResultsMsg .= "There are " . count($statesArray) . " states with a cumulative total of " . count($convertedDataArray) . " stores in the phpArray created by parseLocationJSON." . "\n\n";

} /* end parseLocationJSON() */




/*
* Function: syncLocationDB()
* description: reads PHP array and syncs wp_store_locator database
* note: function only performs inserts and updates, no deletions
*/
function syncLocationDB() {
  global $convertedDataArray, $cronResultsMsg;

  require_once(ABSPATH . '/wp-load.php');
  global $wpdb;

  $table = 'wp_store_locator';
  $formatInsert = array ("%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%d", "%s");
  $rowsInserted = 0;
  $rowsUpdated = 0;
  for($key=0; $key<count($convertedDataArray); $key++) {

      $data = array(
        "sl_store" => $convertedDataArray[$key]['NameID'],
        "sl_address" => $convertedDataArray[$key]['Addr1'],
        "sl_address2" => $convertedDataArray[$key]['Addr2'],
        "sl_city" => $convertedDataArray[$key]['City'],
        "sl_state" => $convertedDataArray[$key]['State'],
        "sl_zip" => $convertedDataArray[$key]['Zip'],
        "sl_country" => $convertedDataArray[$key]['Country'],
        "sl_latitude" => $convertedDataArray[$key]['Lat'],
        "sl_longitude" => $convertedDataArray[$key]['Lon'],
        "sl_description" => $convertedDataArray[$key]['Description'],
        /* fb */ "sl_email" => $convertedDataArray[$key]['Facebook'],
        "sl_url" => $convertedDataArray[$key]['URL'],
        "sl_hours" => $convertedDataArray[$key]['Hours'],
        "sl_phone" => $convertedDataArray[$key]['Phone'],
        /* tw */ "sl_fax" => $convertedDataArray[$key]['Twitter'],
        "sl_tags" => $convertedDataArray[$key]['Tags'],
        "ritas_id" => $convertedDataArray[$key]['ritasStoreID'],
        "sl_lastupdated" => date('Y-m-d H:i:s')
      );

      $lookupSQL = "SELECT * FROM " . $table . " WHERE ritas_id = " . $convertedDataArray[$key]['ritasStoreID'];

      $wpdb->get_row($lookupSQL);

      if( $wpdb->num_rows == 1 ) {
        $whereCondition = array( "ritas_id" => $convertedDataArray[$key]['ritasStoreID']);
        $wpdb->update( $table, $data, $whereCondition, $format = null, $where_format = null );
        $rowsUpdated++;
      } else {
        $wpdb->insert( $table, $data, $formatInsert);
        $rowsInserted++;
      }
  } //end for loop

  $cronResultsMsg .= "A total of " . $rowsUpdated . " rows were updated, and " . $rowsInserted . " rows were inserted by the syncLocationDB function.";

}; /* end syncLocationDB() */


/*
*
* call function to trigger JSON read and syncs mySQL database
*/
ritas_location_sync();


?>