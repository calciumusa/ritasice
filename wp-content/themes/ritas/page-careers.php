<?php
/*
Template Name: Ritas Career Page Template
Description:
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

				    <div id="main" class="first clearfix" role="main">

					    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix flash-embed'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

						    <header class="article-header">
							    <h1 class="page-title" itemprop="headline">
							      <?php the_title(); ?>
							    </h1>
						    </header> <!-- end article header -->

                <section class="entry-content">
                    <?php the_content(); ?>
                </section><!-- end article section -->


						    <footer class="article-footer <?php echo get_post_type(); ?>">
                  <?php the_tags('<span class="tags">' . __('Tags:', 'ritastheme') . '</span> ', ', ', ''); ?>
						    </footer> <!-- end article footer -->

					    </article> <!-- end article -->

					    <?php endwhile; endif; ?>


					   <div class="job-postings">

					   <h2>Current Openings</h2>

                <?php
                  $args = array( 'post_type' => 'job-posting', 'order_by' => 'menu_order', 'order' => 'ASC', 'posts_per_page' => '-1');
                  $customQuery = new WP_Query( $args );

                  if ( $customQuery->have_posts() ) : while ( $customQuery->have_posts() ) : $customQuery->the_post();
                  ?>


                  <h3><a href="/job-posting/<?php echo $post->post_name ?>"><?php echo the_title() ?></a></h3>


                   <?php
                     endwhile;
                     endif;
                  ?>

                </div> <!-- end job-postings -->


    				</div> <!-- end #main -->

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>
