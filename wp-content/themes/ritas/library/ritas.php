<?php
/* Welcome to ritas :)
This is the core ritas file where most of the
main functions & features reside. If you have
any custom functions, it's best to put them
in the functions.php file.

Developed by: Eddie Machado
URL: http://themble.com/ritas/
*/

/*********************
LAUNCH ritas
Let's fire off all the functions
and tools. I put it up here so it's
right up top and clean.
*********************/

// we're firing all out initial functions at the start
add_action('after_setup_theme','ritas_ahoy', 15);

function ritas_ahoy() {

    // launching operation cleanup
    add_action('init', 'ritas_head_cleanup');
    // remove WP version from RSS
    add_filter('the_generator', 'ritas_rss_version');
    // remove pesky injected css for recent comments widget
    add_filter( 'wp_head', 'ritas_remove_wp_widget_recent_comments_style', 1 );
    // clean up comment styles in the head
    add_action('wp_head', 'ritas_remove_recent_comments_style', 1);
    // clean up gallery output in wp
    add_filter('gallery_style', 'ritas_gallery_style');

    // enqueue base scripts and styles
    add_action('wp_enqueue_scripts', 'ritas_scripts_and_styles', 999);
    // ie conditional wrapper
    add_filter( 'style_loader_tag', 'ritas_ie_conditional', 10, 2 );


    // launching this stuff after theme setup
    add_action('after_setup_theme','ritas_theme_support');
    // adding sidebars to Wordpress (these are created in functions.php)

    add_action( 'widgets_init', 'ritas_register_sidebars' );
    // adding the ritas search form (created in functions.php)

    add_filter( 'get_search_form', 'ritas_wpsearch' );

    // cleaning up random code around images
    add_filter('the_content', 'ritas_filter_ptags_on_images');
    // cleaning up excerpt
    add_filter('excerpt_more', 'ritas_excerpt_more');

} /* end ritas ahoy */

/*********************
WP_HEAD GOODNESS
The default wordpress head is
a mess. Let's clean it up by
removing all the junk we don't
need.
*********************/

function ritas_head_cleanup() {
	// category feeds
	// remove_action( 'wp_head', 'feed_links_extra', 3 );
	// post and comment feeds
	// remove_action( 'wp_head', 'feed_links', 2 );
	// EditURI link
	remove_action( 'wp_head', 'rsd_link' );
	// windows live writer
	remove_action( 'wp_head', 'wlwmanifest_link' );
	// index link
	remove_action( 'wp_head', 'index_rel_link' );
	// previous link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	// start link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	// links for adjacent posts
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	// WP version
	remove_action( 'wp_head', 'wp_generator' );
  // remove WP version from css
  add_filter( 'style_loader_src', 'ritas_remove_wp_ver_css_js', 9999 );
  // remove Wp version from scripts
  add_filter( 'script_loader_src', 'ritas_remove_wp_ver_css_js', 9999 );

} /* end ritas head cleanup */

// remove WP version from RSS
function ritas_rss_version() { return ''; }

// remove WP version from scripts
function ritas_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}

// remove injected CSS for recent comments widget
function ritas_remove_wp_widget_recent_comments_style() {
   if ( has_filter('wp_head', 'wp_widget_recent_comments_style') ) {
      remove_filter('wp_head', 'wp_widget_recent_comments_style' );
   }
}

// remove injected CSS from recent comments widget
function ritas_remove_recent_comments_style() {
  global $wp_widget_factory;
  if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
    remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
  }
}

// remove injected CSS from gallery
function ritas_gallery_style($css) {
  return preg_replace("!<style type='text/css'>(.*?)</style>!s", '', $css);
}


/*********************
SCRIPTS & ENQUEUEING
*********************/

// loading modernizr and jquery, and reply script
function ritas_scripts_and_styles() {
  if (!is_admin()) {

    // modernizr (without media query polyfill)
    wp_register_script( 'ritas-modernizr', get_stylesheet_directory_uri() . '/library/js/libs/modernizr.custom.min.js', array(), '2.5.3', false );

    // register main stylesheet
    wp_register_style( 'ritas-stylesheet', get_stylesheet_directory_uri() . '/library/css/style.css', array(), '', 'all' );

    // ie-only style sheet
    wp_register_style( 'ritas-ie-only', get_stylesheet_directory_uri() . '/library/css/ie.css', array(), '' );

    // comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }

    //adding scripts file in the footer
    wp_register_script( 'ritas-js', get_stylesheet_directory_uri() . '/library/js/scripts.min.js', array( 'jquery' ), '', true );
    wp_register_script( 'ritas-nutrition', get_stylesheet_directory_uri() . '/library/js/ritas-nutrition.min.js', array( 'jquery' ), '', true );
    wp_register_script( 'countdown', get_stylesheet_directory_uri() . '/library/js/countdown.js', array( 'jquery' ), '', true );


    // enqueue styles and scripts
    wp_enqueue_script( 'ritas-modernizr' );
    wp_enqueue_style( 'ritas-stylesheet' );
    wp_enqueue_style('ritas-ie-only');
    wp_enqueue_style('ritas-mozilla-only');
    /*
    I recommend using a plugin to call jQuery
    using the google cdn. That way it stays cached
    and your site will load faster.
    */
    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'ritas-js' );
    wp_enqueue_script( 'ritas-nutrition' );
    wp_enqueue_script( 'countdown' );
  }
}

// adding the conditional wrapper around ie stylesheet
// source: http://code.garyjones.co.uk/ie-conditional-style-sheets-wordpress/
function ritas_ie_conditional( $tag, $handle ) {
	if ( 'ritas-ie-only' == $handle )
		$tag = '<!--[if lt IE 9]>' . "\n" . $tag . '<![endif]-->' . "\n";
	return $tag;
}


/*********************
THEME SUPPORT
*********************/

// Adding WP 3+ Functions & Theme Support
function ritas_theme_support() {

	// wp thumbnails (sizes handled in functions.php)
	add_theme_support('post-thumbnails');

	// default thumb size
	set_post_thumbnail_size(125, 125, true);

	// wp custom background (thx to @bransonwerner for update)
	add_theme_support( 'custom-background',
	    array(
	    'default-image' => '',  // background image default
	    'default-color' => '', // background color default (dont add the #)
	    'wp-head-callback' => '_custom_background_cb',
	    'admin-head-callback' => '',
	    'admin-preview-callback' => ''
	    )
	);

	// rss thingy
	add_theme_support('automatic-feed-links');

	// to add header image support go here: http://themble.com/support/adding-header-background-image-support/

	// adding post format support
	add_theme_support( 'post-formats',
		array(
//			'aside',             // title less blurb
			'gallery',           // gallery of images
//			'link',              // quick link to other site
			'image'             // an image
//			'quote',             // a quick quote
//			'status',            // a Facebook like status update
//			'video',             // video
//			'audio',             // audio
//			'chat'               // chat transcript
		)
	);

	// wp menus
	add_theme_support( 'menus' );


} /* end ritas theme support */

if (!function_exists( 'register_ritas_menus')) {
	// registering wp3+ menus
	function register_ritas_menus() {
	register_nav_menus(
		array(
			'main-nav' => __( 'The Main Menu', 'ritastheme' ),   // main nav in header
			'footer-links' => __( 'Footer Links', 'ritastheme' ), // secondary nav in footer
			'main_nav_mobile' => __( 'Mobile Main Menu', 'ritastheme' ),   // mobile main nav in header
			'footer_links_mobile' => __( 'Mobile Footer Links', 'ritastheme' ) // mobile secondary nav in footer
		)
	);
	}
	add_action( 'init', 'register_ritas_menus' );
}



/*********************
MENUS & NAVIGATION
*********************/


// the main menu
function ritas_main_nav() {
	// display the wp3 menu if available
    wp_nav_menu(array(
    	'container' => false,                           // remove nav container
    	'container_class' => 'menu clearfix',           // class of container (should you choose to use it)
    	'menu' => __( 'The Main Menu', 'ritastheme' ),  // nav name
    	'menu_class' => 'top-nav clearfix menu-even menu-main-menu',         // adding custom nav class
    	'theme_location' => 'main-nav',                 // where it's located in the theme
    	'before' => '',                                 // before the menu
      'after' => '',                                  // after the menu
      'link_before' => '',                            // before each link
      'link_after' => '',                             // after each link
      'depth' => 2,                                   // limit the depth of the nav
    	'fallback_cb' => 'ritas_main_nav_fallback'      // fallback function
	));
} /* end ritas main nav */


// this is the fallback for header menu
function ritas_main_nav_fallback() {
	wp_page_menu( array(
		'show_home' => true,
    	'menu_class' => 'nav top-nav clearfix',      // adding custom nav class
		'include'     => '',
		'exclude'     => '',
		'echo'        => true,
        'link_before' => '',                            // before each link
        'link_after' => ''                             // after each link
	) );
}


class ritas_select_menu_walker extends Walker_Nav_Menu {

function start_lvl(&$output, $depth) {
		$indent = str_repeat("\t", $depth);
		$output .= "";
	}


	function end_lvl(&$output, $depth) {
		$indent = str_repeat("\t", $depth);
		$output .= "";
	}

	 function start_el(&$output, $item, $depth, $args) {
		global $wp_query;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';

		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

		//check if current page is selected page and add selected value to select element
		  $selc = '';
		  $curr_class = 'current-menu-item';
		  $is_current = strpos($class_names, $curr_class);
		  if($is_current === false){
	 		  $selc = "";
		  }else{
	 		  $selc = "selected ";
		  }

		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

		$sel_val =  ' value="'   . esc_attr( $item->url        ) .'"';

		//check if the menu is a submenu
		switch ($depth){
		  case 0:
			   $dp = "";
			   break;
		  case 1:
			   $dp = "-";
			   break;
		  case 2:
			   $dp = "--";
			   break;
		  case 3:
			   $dp = "---";
			   break;
		  case 4:
			   $dp = "----";
			   break;
		  default:
			   $dp = "";
		}


		$output .= $indent . '<option'. $sel_val . $id . $value . $class_names . $selc . '>'.$dp;

		$item_output = $args->before;
		//$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		//$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

	function end_el(&$output, $item, $depth) {
		$output .= "</option>\n";
	}

} // ritas_dropdown_walker_nav_menu


// the footer menu (should you choose to use one)
function ritas_mobile_main_nav() {
    wp_nav_menu(array(
/* the following two lines can be uncommented to toggle a select control instead of a UL */
      'items_wrap' => '<span class="select-title">Menu</span><select name="-Menu-">%3$s</select>',
    	'walker' => new ritas_select_menu_walker,    //custom walker function
      'theme_location' => 'main_nav_mobile', // your theme location here
    	'fallback_cb' => 'ritas_mobile_main_nav_fallback'  // fallback function
	));
} /* end ritas_mobile_main_nav */


// this is the fallback for mobile nav main menu
function ritas_mobile_main_nav_fallback() {
	/* you can put a default here if you like */
}


// the footer menu (should you choose to use one)
function ritas_mobile_footer_links() {
    wp_nav_menu(array(
      'items_wrap' => '<span class="select-title">Menu</span><select name="-Menu-">%3$s</select>',
    	'walker' => new ritas_select_menu_walker,    //custom walker function
      'theme_location' => 'footer_links_mobile', // your theme location here
    	'fallback_cb' => 'ritas_mobile_footer_links_fallback'  // fallback function
	));
} /* end ritas_mobile_footer_links */


// this is the fallback for mobile footer nav
function ritas_mobile_footer_links_fallback() {
	/* you can put a default here if you like */
}




class ritasFooter_walker_nav_menu extends walker {
	var $tree_type = array( 'post_type', 'taxonomy', 'custom' );

	var $db_fields = array( 'parent' => 'menu_item_parent', 'id' => 'db_id' );

	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul class=\"subpage-listing\">\n";
	}

	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent</ul>\n";
	}

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

		$output .= $indent . '<li' . $id . $value . $class_names .'>';

		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= "</li>\n";
	}
} // ritasFooter_walker_nav_menu


// the footer menu (should you choose to use one)
function ritas_footer_links() {
	// display the wp3 menu if available
    wp_nav_menu(array(
    	'container' => '',                              // remove nav container
    	'container_class' => 'footer-links clearfix',   // class of container (should you choose to use it)
    	'menu' => __( 'Footer Links', 'ritastheme' ),   // nav name
    	'menu_class' => 'footer-nav clearfix',          // adding custom nav class
    	'theme_location' => 'footer-links',             // where it's located in the theme
    	'before' => '',                                 // before the menu
        'after' => '',                                // after the menu
        'link_before' => '',                          // before each link
        'link_after' => '',                           // after each link
        'depth' => 2,                                 // limit the depth of the nav
    	'walker' => new ritasFooter_walker_nav_menu,    //custom walker function
    	'fallback_cb' => 'ritas_footer_links_fallback'  // fallback function
	));
} /* end ritas footer link */


// this is the fallback for footer menu
function ritas_footer_links_fallback() {
	/* you can put a default here if you like */
}


//function below globally reverses entire menu output
//add_filter( 'wp_nav_menu_objects', create_function( '$menu', 'return array_reverse( $menu );' ) );

function ritas_inverted_main_nav() {
// Get the nav menu based on $menu_name (same as 'theme_location' or 'menu' arg to wp_nav_menu)
  // This code based on wp_nav_menu's code to get Menu ID from menu slug
  $menu_name = 'main-nav';

  if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
    $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );

    $menu_items = wp_get_nav_menu_items($menu->term_id);

    $menu_list = '<ul class="menu-main-menu top-nav clearfix parent-menu menu-up">';
    $submenu_item = "";
    $submenu_list = array();
    $submenu_index = 0;
    $rootMenuID = -1;
    $menuParent = 0;
    $mainMenuElement = 0;
    foreach ( (array) $menu_items as $key => $menu_item ) {
      $title = $menu_item->title;
      $url = $menu_item->url;

      //determine what the main parent ID of the main menu is
      if($rootMenuID == -1) {
        $menuParent = $rootMenuID = $menu_item->menu_item_parent;
      }

      //check to see if parent menu item ID is different from current parent menu item ID
      if($menu_item->menu_item_parent != $menuParent) {
        if( $menu_item->menu_item_parent != $rootMenuID ){
          $menu_list .= '<ul class="sub-menu">';
          $submenu_list = array();
        }

        else //if it is the end of a submenu, closeout the submenu ul and the wrapper list item
        {
          $submenu = array_reverse($submenu_list);

          foreach($submenu as $submenu_item) {
            $menu_list .= $submenu_item;
          }

          $menu_list .= "\n" . '</ul><!--/.sub-menu-->';
          $menu_list .= "\n" . '</li><!--/.menu-item-->';
          $mainMenuElement++;
          $menu_list .= '<li class="parent-menu-item menu-item';
          if($mainMenuElement % 2 == 0) {
            $menu_list .= ' even-menu-item';
          } else {
            $menu_list .= ' odd-menu-item';
          }
          $menu_list .= '"><a href="' . $url . '">' . $title . '</a>';
        }
        $menuParent = $menu_item->menu_item_parent;
      }

      //check to see if parent menu item ID is same as root menu ID
      else if($menu_item->menu_item_parent == $rootMenuID) {
          $menu_list .= '</li><!--/.menu-item-->';
          $menu_list .= '<li class="parent-menu-item menu-item';
          $mainMenuElement++;
          if($mainMenuElement % 2 == 0) {
            $menu_list .= ' even-menu-item';
          } else {
            $menu_list .= ' odd-menu-item';
          }
          $menu_list .= '"><a href="' . $url . '">' . $title . '</a>';
      }

      if( $menu_item->menu_item_parent != $rootMenuID ){
        $submenu_item = "<li class='menu-item'><a href='" . $url . "'>" . $title . "</a></li>";
        $submenu_list[$submenu_index] = $submenu_item;
        $submenu_index++;
      }


    } // end foreach

    $submenu = array_reverse($submenu_list);

    foreach($submenu as $submenu_item) {
      $menu_list .= $submenu_item;
    }

    $menu_list .= "\n" . '</ul><!--/.sub-menu-->';
    $menu_list .= "\n" . '</li><!--/.menu-item-->';

	  $menu_list .= '</ul><!--/top-nav-->';
  } // no menu available
  else {
	  $menu_list = '<ul><li>Menu "' . $menu_name . '" not defined.</li></ul>';
  }
  echo $menu_list;
}  // ritas_inverted_main_nav



/***************************************
CUSTOM POST AND PAGE OUTPUT FUNCTIONS
***************************************/

function ritas_the_title() {
/* functionality to output page title in alternating color mode */
  $page_title = get_the_title();
  $new_title = "";
  $pos = strpos($page_title, " ");


    echo '<span class="ritas-green">';
    if ($pos === false) {
      echo $page_title;
    }
    else {
      echo substr($page_title, 0, $pos);
    }
    echo '</span>';

    if ($pos !== false) {
    /* title is more than one word, wrap first word in span class to display in green and red */
    echo '<span class="ritas-red">';
    echo substr($page_title, $pos);
    echo '</span>';
  }
}

function ritas_faux_title($value) {
/* functionality to output passed string value in alternating color mode */
  $page_title = $value;
  $new_title = "";
  $pos = strpos($page_title, " ");

    $htmlOutput = '<span class="ritas-green">';
    if ($pos === false) {
      $htmlOutput .= $page_title;
    }
    else {
      $htmlOutput .= substr($page_title, 0, $pos);
    }
    $htmlOutput .=  '</span>';

    if ($pos !== false) {
      /* title is more than one word, wrap first word in span class to display in green and red */
      $htmlOutput .= '<span class="ritas-red">';
      $htmlOutput .=  substr($page_title, $pos);
      $htmlOutput .=  '</span>';
    }
    return $htmlOutput;
}

/*********************
RELATED POSTS FUNCTION
*********************/

// Related Posts Function (call using ritas_related_posts(); )
function ritas_related_posts() {
	echo '<ul id="ritas-related-posts">';
	global $post;
	$tags = wp_get_post_tags($post->ID);
	if($tags) {
		foreach($tags as $tag) { $tag_arr .= $tag->slug . ','; }
        $args = array(
        	'tag' => $tag_arr,
        	'numberposts' => 5, /* you can change this to show more */
        	'post__not_in' => array($post->ID)
     	);
        $related_posts = get_posts($args);
        if($related_posts) {
        	foreach ($related_posts as $post) : setup_postdata($post); ?>
	           	<li class="related_post"><a class="entry-unrelated" href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
	        <?php endforeach; }
	    else { ?>
            <?php echo '<li class="no_related_post">' . __( 'No Related Posts Yet!', 'ritastheme' ) . '</li>'; ?>
		<?php }
	}
	wp_reset_query();
	echo '</ul>';
} /* end ritas related posts function */

/*********************
PAGE NAVI
*********************/

// Numeric Page Navi (built into the theme by default)
function ritas_page_navi($before = '', $after = '') {
	global $wpdb, $wp_query;
	$request = $wp_query->request;
	$posts_per_page = intval(get_query_var('posts_per_page'));
	$paged = intval(get_query_var('paged'));
	$numposts = $wp_query->found_posts;
	$max_page = $wp_query->max_num_pages;
	if ( $numposts <= $posts_per_page ) { return; }
	if(empty($paged) || $paged == 0) {
		$paged = 1;
	}
	$pages_to_show = 7;
	$pages_to_show_minus_1 = $pages_to_show-1;
	$half_page_start = floor($pages_to_show_minus_1/2);
	$half_page_end = ceil($pages_to_show_minus_1/2);
	$start_page = $paged - $half_page_start;
	if($start_page <= 0) {
		$start_page = 1;
	}
	$end_page = $paged + $half_page_end;
	if(($end_page - $start_page) != $pages_to_show_minus_1) {
		$end_page = $start_page + $pages_to_show_minus_1;
	}
	if($end_page > $max_page) {
		$start_page = $max_page - $pages_to_show_minus_1;
		$end_page = $max_page;
	}
	if($start_page <= 0) {
		$start_page = 1;
	}
	echo $before.'<nav class="page-navigation"><ol class="ritas_page_navi clearfix">'."";
	if ($start_page >= 2 && $pages_to_show < $max_page) {
		$first_page_text = __( "First", 'ritastheme' );
		echo '<li class="bpn-first-page-link"><a href="'.get_pagenum_link().'" title="'.$first_page_text.'">'.$first_page_text.'</a></li>';
	}
	echo '<li class="bpn-prev-link">';
	previous_posts_link('<<');
	echo '</li>';
	for($i = $start_page; $i  <= $end_page; $i++) {
		if($i == $paged) {
			echo '<li class="bpn-current">'.$i.'</li>';
		} else {
			echo '<li><a href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
		}
	}
	echo '<li class="bpn-next-link">';
	next_posts_link('>>');
	echo '</li>';
	if ($end_page < $max_page) {
		$last_page_text = __( "Last", 'ritastheme' );
		echo '<li class="bpn-last-page-link"><a href="'.get_pagenum_link($max_page).'" title="'.$last_page_text.'">'.$last_page_text.'</a></li>';
	}
	echo '</ol></nav>'.$after."";
} /* end page navi */

/*********************
RANDOM CLEANUP ITEMS
*********************/

// remove the p from around imgs (http://css-tricks.com/snippets/wordpress/remove-paragraph-tags-from-around-images/)
function ritas_filter_ptags_on_images($content){
   return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

// This removes the annoying […] to a Read More link
function ritas_excerpt_more($more) {
	global $post;
	// edit here if you like
	return '...  <a href="'. get_permalink($post->ID) . '" title="Read '.get_the_title($post->ID).'">Read more &raquo;</a>';
}

/*
 * This is a modified the_author_posts_link() which just returns the link.
 *
 * This is necessary to allow usage of the usual l10n process with printf().
 */
function ritas_get_the_author_posts_link() {
	global $authordata;
	if ( !is_object( $authordata ) )
		return false;
	$link = sprintf(
		'<a href="%1$s" title="%2$s" rel="author">%3$s</a>',
		get_author_posts_url( $authordata->ID, $authordata->user_nicename ),
		esc_attr( sprintf( __( 'Posts by %s' ), get_the_author() ) ), // No further l10n needed, core will take care of this one
		get_the_author()
	);
	return $link;
}


/****************************
Image Gallery Customization
****************************/

add_filter( 'post_gallery', 'ritas_post_gallery', 10, 2 );
function ritas_post_gallery( $output, $attr) {
    global $post, $wp_locale;

    static $instance = 0;
    $instance++;

    // We're trusting author input, so let's at least make sure it looks like a valid orderby statement
    if ( isset( $attr['orderby'] ) ) {
        $attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
        if ( !$attr['orderby'] )
            unset( $attr['orderby'] );
    }

    extract(shortcode_atts(array(
        'order'      => 'ASC',
        'orderby'    => 'menu_order ID',
        'id'         => $post->ID,
        'itemtag'    => 'dl',
        'icontag'    => 'dt',
        'captiontag' => 'dd',
        'columns'    => 3,
        'size'       => 'thumbnail',
        'include'    => '',
        'exclude'    => ''
    ), $attr));

    $id = intval($id);
    if ( 'RAND' == $order )
        $orderby = 'none';

    if ( !empty($include) ) {
        $include = preg_replace( '/[^0-9,]+/', '', $include );
        $_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

        $attachments = array();
        foreach ( $_attachments as $key => $val ) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    } elseif ( !empty($exclude) ) {
        $exclude = preg_replace( '/[^0-9,]+/', '', $exclude );
        $attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
    } else {
        $attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
    }

    if ( empty($attachments) )
        return '';

    if ( is_feed() ) {
        $output = "\n";
        foreach ( $attachments as $att_id => $attachment )
            $output .= wp_get_attachment_link($att_id, $size, true) . "\n";
        return $output;
    }

    $itemtag = tag_escape($itemtag);
    $captiontag = tag_escape($captiontag);
    $columns = intval($columns);
    $itemwidth = $columns > 0 ? floor(100/$columns) : 100;
    $float = is_rtl() ? 'right' : 'left';

    $selector = "gallery-{$instance}";

    $output = apply_filters('gallery_style', "
        <style type='text/css'>
            #{$selector} {
                margin: auto;
            }
            #{$selector} .gallery-item {
                float: {$float};
                margin-top: 10px;
                text-align: center;
                width: {$itemwidth}%;           }
            #{$selector} img {
                border: 2px solid #cfcfcf;
            }
            #{$selector} .gallery-caption {
                margin-left: 0;
            }
        </style>
        <!-- see gallery_shortcode() in wp-includes/media.php -->
        <div id='$selector' class='gallery galleryid-{$id}'>");

    $i = 0;
    foreach ( $attachments as $id => $attachment ) {
        $link = isset($attr['link']) && 'file' == $attr['link'] ? wp_get_attachment_link($id, $size, false, false) : wp_get_attachment_link($id, $size, true, false);
        $link = str_replace ('title=', 'rel="lightbox[gallery]" class="cboxElement" title=', $link);

        $output .= "<{$itemtag} class='gallery-item' id='gallery-image-$i'>";
        $output .= "
            <{$icontag} class='gallery-icon'>
                $link
            </{$icontag}>";
        if ( $captiontag && trim($attachment->post_excerpt) ) {
            $output .= "
                <{$captiontag} class='gallery-caption'>
                " . wptexturize($attachment->post_excerpt) . "
                </{$captiontag}>";
        }
        $output .= "</{$itemtag}>";
        if ( $columns > 0 && ++$i % $columns == 0 )
            $output .= '<br style="clear: both" />';
    }

    $output .= "
            <br style='clear: both;' />
        </div>\n";

    return $output;
}


function ritas_myme_types($mime_types){
    $mime_types['mov'] = 'video/quicktime'; //Adding mov extension
    return $mime_types;
}
add_filter('upload_mimes', 'ritas_myme_types', 1, 1);



/**
 * Adds Recent Job Postings widget.
 */

 class Job_Postings extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	public function __construct() {
		parent::__construct(
	 		'Job_Postings', // Base ID
			'Current Opportunities', // Name
			array( 'description' => __( 'Displays current opportunities from custom job posting post type', 'text_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );

		echo $before_widget;

    $featuredOpportunitiesHTML = '<div class="careers-widget">' . "\n";

		if ( ! empty( $title ) ) { echo '<h4 class="widgettitle">' . $title . '</h4>'; }

    $featuredOpportunitiesHTML .= '<ul>' . "\n";

    $post_limit = 10;
    $args = array( 'post_type' => 'job-posting', 'posts_per_page' => $post_limit );

    $loop = new WP_Query( $args );
    if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();

        $featuredOpportunitiesHTML .= '<li>' . '<a href="' . get_permalink() . '">' . get_the_title() . '</a>' . '</li>' . "\n";

    endwhile; endif;  //end if loop has posts

    $featuredOpportunitiesHTML .= '</ul>' . "\n";
    $featuredOpportunitiesHTML .= "<p><a href='/careers'>Return to the Rita's Careers page.</a></p>" . "\n";

    $featuredOpportunitiesHTML .= '</div><!--/.careers-widget -->' . "\n";

		echo $featuredOpportunitiesHTML;
		echo $after_widget;
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = strip_tags( $new_instance['title'] );

		return $instance;
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'text_domain' );
		}
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Current Opportunities Widget Custom Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<?php
	}

} // class Job_Postings

// register Job_Postings widget
add_action( 'widgets_init', create_function( '', 'register_widget( "Job_Postings" );' ) );



/**
 * Adds Recent Posts by Category widget.
 */

 class Recent_Posts_by_Category extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	public function __construct() {
		parent::__construct(
	 		'Recent_Posts_by_Category', // Base ID
			'Recent Posts by Category', // Name
			array( 'description' => __( 'Displays recent posts from any category you choose', 'text_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
    public function widget( $args, $instance ) {
      extract( $args );
      $title = apply_filters( 'widget_title', $instance['title'] );

      echo $before_widget;
      $widgetHTML = "<div class='rpbc_widget_inner'>";
      if ( ! empty( $title ) ) { echo '<h4 class="widgettitle">' . $title . '</h4>'; }
      $widgetHTML .= '<ul>';

      $catName = $instance['category_name'];
      if($catName == 'ALL'){
        $args = array('posts_per_page' => $instance['noofposts']);
      } else {
        $args = array('posts_per_page' => $instance['noofposts'], 'category_name' => $catName);
      }
      $loop = new WP_Query( $args );

      if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();
        $widgetHTML .= '<li>' . "\n";
        $widgetHTML .= '<a href="' . get_permalink() . '">' . get_the_title() . '</a>' . "\n";
        $widgetHTML .= '</li>';
      endwhile; endif;  //end if loop has posts

      $widgetHTML .= '</ul>';
      $widgetHTML .= "</div>";

      echo $widgetHTML;
      echo $after_widget;
    }


    /**
    * Sanitize widget form values as they are saved.
    *
    * @see WP_Widget::update()
    *
    * @param array $new_instance Values just sent to be saved.
    * @param array $old_instance Previously saved values from database.
    *
    * @return array Updated safe values to be saved.
    */
    public function update( $new_instance, $old_instance ) {
      $instance = array();
      $instance['title'] = strip_tags( $new_instance['title'] );
      $instance['noofposts'] = strip_tags( $new_instance['noofposts'] );
      $instance['category_name'] = strip_tags( $new_instance['category_name'] );
      return $instance;
    }


    /**
    * Back-end widget form.
    *
    * @see WP_Widget::form()
    *
    * @param array $instance Previously saved values from database.
    */
    public function form( $instance ) {
      if ( isset( $instance[ 'title' ] ) ) {
          $title = $instance[ 'title' ];
      }
      if ( isset( $instance[ 'noofposts' ] ) ) {
        $noofposts = $instance[ 'noofposts' ];
      } else {
        $noofposts = "5";
      }
      ?>
      <p>
      <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
      </p>

      <p>
      <label for="<?php echo $this->get_field_id( 'noofposts' ); ?>"><?php _e( 'No Of Posts:' ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'noofposts' ); ?>" name="<?php echo $this->get_field_name( 'noofposts' ); ?>" type="text" value="<?php echo esc_attr( $noofposts ); ?>" />
      </p>

      <p>
      <label for="<?php echo $this->get_field_id('category_name'); ?>"><?php _e('Select a Category:'); ?></label>
      <select class="widefat" id="<?php echo $this->get_field_id('category_name'); ?>" name="<?php echo $this->get_field_name('category_name'); ?>">
        <option class="widefat" value="ALL" <?php ($instance['category_name'] == "ALL" ? "selected='selected'" : " ") ?>>ALL</option>
        <?php
        $categories = get_categories();
        foreach($categories as $category){
          ?>
          <option class="widefat" value="<?php echo $category->slug; ?>" <?php if( $instance['category_name'] == $category->slug ) { echo "selected='selected'
          "; } else { echo " "; }?> ><?php echo $category->name; ?></option>
          <?php
        } //end foreach
        ?>
      </select>
      </p>
      <?php
    } //function form($instance)

  } // class Recent_Posts_by_Category

// register Recent_Posts_by_Category widget
add_action( 'widgets_init', create_function( '', 'register_widget( "Recent_Posts_by_Category" );' ) );


/*
*  function: togglebox
*  description:  register toggle shortcode for content, creating expanding/collapsing divs
*/
if ( ! function_exists( 'togglebox' ) ) :
function togglebox($atts, $content = null) {
	extract(shortcode_atts(array(
        'title'      => '',
    ), $atts));

	$out .= '<h3 class="toggle"><div class="toggle-icon"></div><strong><a href="#">' .$title. '</a></strong></h3>';
	$out .= '<div class="toggle-box" style="display: none;">';
	$out .= do_shortcode($content);
	$out .= '</div>';

   return $out;
}

add_shortcode('toggle', 'togglebox');
endif; // end of if(function_exists('togglebox'))

?>