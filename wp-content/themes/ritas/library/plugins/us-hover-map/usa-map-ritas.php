<!--
US Map CSS Rollover Image Map
Developed by: Stan Naspinski - 2006 (mrstan@gmail.com)
Feel free to use this code, recognition would be appreciated!-->
<!-- all states should redirect to /locations/locations-by-state/?state=PA -->
<div>
    <dl id="imap">
		<dt>
			<a id="us" href="#nogo" title="us">
				<span>United States</span>
			</a>
		</dt>
		<dd  id="img_al">
			<a id="al" href="/locations/locations-by-state/?state=AL" title="Alabama" target="_parent">
				<span>Rita's locations in Alabama
				</span>
			</a>
		</dd>
		<dd  id="img_ak">
			<a id="ak" href="/locations/locations-by-state/?state=AK" title="Alaska" target="_parent">
				<span>Rita's locations in Alaska
			    </span>
			</a>
		</dd>
		<dd  id="img_az">
			<a id="az" href="/locations/locations-by-state/?state=AZ" title="Arizona" target="_parent">
				<span>Rita's locations in Arizona
			    </span>
			</a>
		</dd>
		<dd  id="img_ar">
			<a id="ar" href="/locations/locations-by-state/?state=AR" title="Arkansas" target="_parent">
				<span>Rita's locations in Arkansas
			    </span>
			</a>
		</dd>
		<dd  id="img_ca">
			<a id="ca" href="/locations/locations-by-state/?state=CA" title="California" target="_parent">
				<span>Rita's locations in California
			    </span>
			</a>
		</dd>
		<dd  id="img_co">
			<a id="co" href="/locations/locations-by-state/?state=CO" title="Colorado" target="_parent">
				<span>Rita's locations in Colorado
			    </span>
			</a>
		</dd>
		<dd  id="img_ct">
			<a id="ct" href="/locations/locations-by-state/?state=CT" title="Connecticut" target="_parent">
				<span>Rita's locations in Connecticut
			    </span>
			</a>
		</dd>
		<dd  id="img_dc">
			<a id="dc" href="/locations/locations-by-state/?state=DC" title="Washington D.C." target="_parent">
				<span>Rita's locations in Washington D.C.
			    </span>
			</a>
		</dd>
		<dd  id="img_de">
			<a id="de" href="/locations/locations-by-state/?state=DE" title="Delaware" target="_parent">
				<span>Rita's locations in Delaware
			    </span>
			</a>
		</dd>
		<dd  id="img_fl">
			<a id="fl" href="/locations/locations-by-state/?state=FL" title="Florida" target="_parent">
				<span>Rita's locations in Florida
			    </span>
			</a>
		</dd>
		<dd  id="img_ga">
			<a id="ga" href="/locations/locations-by-state/?state=GA" title="Georgia" target="_parent">
				<span>Rita's locations in Georgia
			    </span>
			</a>
		</dd>
		<dd  id="img_hi">
			<a id="hi" href="/locations/locations-by-state/?state=HI" title="Hawaii" target="_parent">
				<span>Rita's locations in Hawaii
			    </span>
			</a>
		</dd>
		<dd  id="img_id">
			<a id="id" href="/locations/locations-by-state/?state=ID" title="Idaho" target="_parent">
				<span>Rita's locations in Idaho
			    </span>
			</a>
		</dd>
		<dd  id="img_il">
			<a id="il" href="/locations/locations-by-state/?state=IL" title="Illinois" target="_parent">
				<span>Rita's locations in Illinois
			    </span>
			</a>
		</dd>
		<dd  id="img_in">
			<a id="in" href="/locations/locations-by-state/?state=IN" title="Indiana" target="_parent">
				<span>Rita's locations in Indiana
			    </span>
			</a>
		</dd>
		<dd  id="img_ia">
			<a id="ia" href="/locations/locations-by-state/?state=IA" title="Iowa" target="_parent">
				<span>Rita's locations in Iowa
			    </span>
			</a>
		</dd>
		<dd  id="img_ks">
			<a id="ks" href="/locations/locations-by-state/?state=KS" title="Kansas" target="_parent">
				<span>Rita's locations in Kansas
			    </span>
			</a>
		</dd>
		<dd  id="img_ky">
			<a id="ky" href="/locations/locations-by-state/?state=KY" title="Kentucky" target="_parent">
				<span>Rita's locations in Kentucky
			    </span>
			</a>
		</dd>
		<dd  id="img_la">
			<a id="la" href="/locations/locations-by-state/?state=LA" title="Louisiana" target="_parent">
				<span>Rita's locations in Louisiana
			    </span>
			</a>
		</dd>
		<dd  id="img_me">
			<a id="me" href="/locations/locations-by-state/?state=ME" title="Maine" target="_parent">
				<span>Rita's locations in Maine
			    </span>
			</a>
		</dd>
		<dd  id="img_md">
			<a id="md" href="/locations/locations-by-state/?state=MD" title="Maryland" target="_parent">
				<span>Rita's locations in Maryland
			    </span>
			</a>
		</dd>
		<dd  id="img_ma">
			<a id="ma" href="/locations/locations-by-state/?state=MA" title="Massachusetts" target="_parent">
				<span>Rita's locations in Massachusetts
			    </span>
			</a>
		</dd>
		<dd  id="img_mi">
			<a id="mi" href="/locations/locations-by-state/?state=MI" title="Michigan" target="_parent">
				<span>Rita's locations in Michigan
			    </span>
			</a>
		</dd>
		<dd  id="img_mn">
			<a id="mn" href="/locations/locations-by-state/?state=MN" title="Minnesota" target="_parent">
				<span>Rita's locations in
				    Minnesota
			    </span>
			</a>
		</dd>
		<dd  id="img_ms">
			<a id="ms" href="/locations/locations-by-state/?state=MS" title="Mississippi" target="_parent">
				<span>Rita's locations in Mississippi
			    </span>
			</a>
		</dd>
		<dd  id="img_mo">
			<a id="mo" href="/locations/locations-by-state/?state=MO" title="Missouri" target="_parent">
				<span>Rita's locations in Missouri
			    </span>
			</a>
		</dd>
		<dd  id="img_mt">
			<a id="mt" href="/locations/locations-by-state/?state=MT" title="Montana" target="_parent">
				<span>Rita's locations in Montana
			    </span>
			</a>
		</dd>
		<dd  id="img_ne">
			<a id="ne" href="/locations/locations-by-state/?state=NE" title="Nebraska" target="_parent">
				<span>Rita's locations in Nebraska
			    </span>
			</a>
		</dd>
		<dd  id="img_nv">
			<a id="nv" href="/locations/locations-by-state/?state=NV" title="Nevada" target="_parent">
				<span>Rita's locations in Nevada
			    </span>
			</a>
		</dd>
		<dd  id="img_nh">
			<a id="nh" href="/locations/locations-by-state/?state=NH" title="New Hampshire" target="_parent">
				<span>Rita's locations in New Hampshire
			    </span>
			</a>
		</dd>
		<dd  id="img_nj">
			<a id="nj" href="/locations/locations-by-state/?state=NJ" title="New Jersey" target="_parent">
				<span>Rita's locations in New Jersey
			    </span>
			</a>
		</dd>
		<dd  id="img_nm">
			<a id="nm" href="/locations/locations-by-state/?state=NM" title="New Mexico" target="_parent">
				<span>Rita's locations in New Mexico
			    </span>
			</a>
		</dd>
		<dd  id="img_ny">
			<a id="ny" href="/locations/locations-by-state/?state=NY" title="New York" target="_parent">
				<span>Rita's locations in New York
			    </span>
			</a>
		</dd>
		<dd  id="img_nc">
			<a id="nc" href="/locations/locations-by-state/?state=NC" title="North Carolina" target="_parent">
				<span>Rita's locations in North Carolina
			    </span>
			</a>
		</dd>
		<dd  id="img_nd">
			<a id="nd" href="/locations/locations-by-state/?state=ND" title="North Dakota" target="_parent">
				<span>Rita's locations in North Dakota
			    </span>
			</a>
		</dd>
		<dd  id="img_oh">
			<a id="oh" href="/locations/locations-by-state/?state=OH" title="Ohio" target="_parent">
				<span>Rita's locations in Ohio
			    </span>
			</a>
		</dd>
		<dd  id="img_ok">
			<a id="ok" href="/locations/locations-by-state/?state=OK" title="Oklahoma" target="_parent">
				<span>Rita's locations in Oklahoma
			    </span>
			</a>
		</dd>
		<dd  id="img_or">
			<a id="or" href="/locations/locations-by-state/?state=OR" title="Oregon" target="_parent">
				<span>Rita's locations in Oregon
			    </span>
			</a>
		</dd>
		<dd  id="img_pa">
			<a id="pa" href="/locations/locations-by-state/?state=PA" title="Pennsylvania" target="_parent">
				<span>Rita's locations in Pennsylvania
			    </span>
			</a>
		</dd>
		<dd  id="img_ri">
			<a id="ri" href="/locations/locations-by-state/?state=RI" title="Rhode Island" target="_parent">
				<span>Rita's locations in Rhode Island
			    </span>
			</a>
		</dd>
		<dd  id="img_sc">
			<a id="sc" href="/locations/locations-by-state/?state=SC" title="South Carolina" target="_parent">
				<span>Rita's locations in South Carolina
			    </span>
			</a>
		</dd>
		<dd  id="img_sd">
			<a id="sd" href="/locations/locations-by-state/?state=SD" title="South Dakota" target="_parent">
				<span>Rita's locations in South Dakota
			    </span>
			</a>
		</dd>
		<dd  id="img_tn">
			<a id="tn" href="/locations/locations-by-state/?state=TN" title="Tennessee" target="_parent">
				<span>Rita's locations in Tennessee
			    </span>
			</a>
		</dd>
		<dd  id="img_tx">
			<a id="tx" href="/locations/locations-by-state/?state=TX" title="Texas" target="_parent">
				<span>Rita's locations in Texas
			    </span>
			</a>
		</dd>
		<dd  id="img_ut">
			<a id="ut" href="/locations/locations-by-state/?state=UT" title="Utah" target="_parent">
				<span>Rita's locations in Utah
			    </span>
			</a>
		</dd>
		<dd  id="img_vt">
			<a id="vt" href="/locations/locations-by-state/?state=VT" title="Vermont" target="_parent">
				<span>Rita's locations in Vermont
			    </span>
			</a>
		</dd>
		<dd  id="img_va">
			<a id="va" href="/locations/locations-by-state/?state=VA" title="Virginia" target="_parent">
				<span>Rita's locations in Virginia
			    </span>
			</a>
		</dd>
		<dd  id="img_wa">
			<a id="wa" href="/locations/locations-by-state/?state=WA" title="Washington" target="_parent">
				<span>Rita's locations in Washington
			    </span>
			</a>
		</dd>
		<dd  id="img_wv">
			<a id="wv" href="/locations/locations-by-state/?state=WV" title="West Virginia" target="_parent">
				<span>Rita's locations in West Virginia
			    </span>
			</a>
		</dd>
		<dd  id="img_wi">
			<a id="wi" href="/locations/locations-by-state/?state=WI" title="Wisconsin" target="_parent">
				<span>Rita's locations in Wisconsin
			    </span>
			</a>
		</dd>
		<dd  id="img_wy">
			<a id="wy" href="/locations/locations-by-state/?state=WY" title="Wyoming" target="_parent">
				<span>Rita's locations in Wyoming
			    </span>
			</a>
		</dd>
	</dl>
</div>