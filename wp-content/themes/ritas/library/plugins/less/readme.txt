This site's css was compiled utilizing LESS.  Refer to http://lesscss.org/index.html.  We recommend using the Crunch app (http://crunchapp.net/) to edit and compile the less files into css.

All LESS files for the site are in the "library/less" folder.
Compiles CSS files for the site are in the "library/css" folder.