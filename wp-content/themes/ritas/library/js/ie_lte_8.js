
var setCookie = function()
{
  var exdays = 1;
  var exdate=new Date();
  exdate.setDate(exdate.getDate() + exdays);
  var c_value=escape("true") + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
  document.cookie="ieDetection" + "=" + c_value;
};


var getCookie = function()
{
  var c_name = "ieDetection";
  var c_value = document.cookie;
  var c_start = c_value.indexOf(" " + c_name + "=");
  if (c_start == -1) { c_start = c_value.indexOf(c_name + "="); }
  if (c_start == -1) { c_value = null; }
  else {
    c_start = c_value.indexOf("=", c_start) + 1;
    var c_end = c_value.indexOf(";", c_start);
    if (c_end == -1) { c_end = c_value.length; }
    c_value = unescape(c_value.substring(c_start,c_end));
  }
  return c_value;
};


var ieChecked = function()
{
  var flag=getCookie("ieDetection");
  if (flag!=null && flag!="") { return true; }
  //ie detected, set cookie
  else { setCookie(); return false; }
};

jQuery.noConflict();
jQuery(document).ready(function($) {
  $('.fixed.nav').hide().css({'display':'none','visibility':'hidden'});
  if( !ieChecked() ) {
    alert('It looks like you are using an older version of Internet Explorer. This site is optimized for IE9 and higher, Google Chrome, Firefox, and Safari. Please consider upgrading your browser for a "COOLER" experience, not just with us- but across the internet!');
  }



}); //jQuery(document).ready()