/*
*    ritas-nutrition.js
*
*    Item-specific nutrition drill-down javascript functions to query by product/flavor/size, returning matching nutritional data
*    requires:
*      - jQuery library
*
*
*/



/***************************** define global session variables ***********************/

var $ = jQuery.noConflict();
var jsonFilePath = "/data/star_nutrition.txt"; /* local data file for testing */
//var jsonFilePath = "http://ritasfranchises.com/Vendors_Gateway/data/star_nutrition.txt"; /* server side file for testing */
var jsonData = "";
var jsonValid = false;
var jsonErrorMsg = "Sorry, nutritional info is not available.";
var secondFlavorID = '';
var secondFlavorName = '';
var toppingID = '';
var toppingName = '';
/***************************** define global session functions ************************/


/*
*  loadData()
*/
function loadData() {
  $.ajax({
    url: jsonFilePath,
    type: 'get',
    cache: true,
    async: true,
    contentType: "application/json; charset=utf-8",
    data: null,
    dataType: "json",
    success: function (jsonResults) {
      /*debug console.log('nutritional json file successfully loaded and ready for processing.  The json file: ',jsonResults);*/
      jsonValid = true;
      jsonData = jsonResults;
    },
    error: function (jsonResults) {
      /*debug console.log('JSON retrieval failure response: ', jsonResults);*/
      jsonValid = false;
    }
  });
} // loadData()



/*
*  arraySearch()
*  search an array for a value and return its key
*/
function arraySearch(arr, val, searchType) {
  switch (searchType) {
    case 'product':
      for (var i=0; i<arr.length; i++){
        if (arr[i].id == val) {
          return i;
        }
      }
      break;
    case 'size':
      for (var i=0; i<arr.length; i++){
        if (arr[i].id == val) {
          return i;
        }
      }
      break;
    case 'hybrid2':
      /*console.log('flavor ID to search for: ', val)*/
      for (var i=0; i<arr.length; i++) {
        subarray = arr[i].attribute;
//        console.log(subarray);
        for (var j=0; j<subarray.length; j++) {
          if ( (subarray[j] == val[0]) && (subarray[j+1] == val[1]) ) {
//            alert('a hybrid flavor match record has been found. click to continue');
//            console.log(i);
            return i;
          }
        }
      }
      break;
    case 'hybrid3':
      /*console.log('flavor ID to search for: ', val)*/
      for (var i=0; i<arr.length; i++) {
        subarray = arr[i].attribute;
//        console.log(subarray);
        for (var j=0; j<subarray.length; j++) {
          if ( (subarray[j] == val[0]) && (subarray[j+1] == val[1]) && (subarray[j+2] == val[2]) ) {
//            alert('a hybrid flavor match record has been found. click to continue');
//            console.log(i);
            return i;
          }
        }
      }
      break;
    default:
      /* must be a single flavor search */
      /*console.log('proceeding to search array for flavor id: ' + val);*/
      for (var i=0; i<arr.length; i++){
        subarray = arr[i].attribute;
        for (var j=0; j<subarray.length; j++){
          if (subarray[j] == val) {
            return i;
          }
        }
      }
      break;
  }
  return false;
} //arraySearch()



/*
*  renderNutritionInfo()
*  queries
*/
renderNutritionInfo = function (productTypeID, flavorID, sizeID) {
  if(!jsonValid) {
    alert(jsonErrorMsg);
  }
  else {
    var data = jsonData.products;

    /* search json array for matching row of data */
    arrayProductKey = arraySearch(data, productTypeID, 'product');
    //console.log('product ID [' + productTypeID + '] found at data[' + arrayProductKey + ']');
    data = data[arrayProductKey].size;
    //console.log('product specific data',data);
    arraySizeKey = arraySearch(data, sizeID, 'size');
    //console.log('size ID [' + sizeID + '] found at data[' + arraySizeKey + ']');
    data = data[arraySizeKey].nutr;
    //console.log('product & size specific data',data);
    if(flavorID.indexOf(',') > -1) {
      var flavorIdArray = flavorID.split(", ");
      for (var i=0; i<flavorIdArray.length; i++) {
        flavorIdArray[i] = flavorIdArray[i].replace('"','');
      }
      if(flavorIdArray.length == 2) {
//          console.log("2 flavors identified", flavorIdArray);
          arrayFlavorKey = arraySearch(data, flavorIdArray, 'hybrid2');
      }
      else {
//        console.log("3 flavors identified", flavorIdArray);
        arrayFlavorKey = arraySearch(data, flavorIdArray, 'hybrid3');
      }
    }
    else {
          arrayFlavorKey = arraySearch(data, flavorID, 'flavor');
    }
    //console.log('flavor ID [' + flavorID + '] found at data[' + arrayFlavorKey + ']');
    data = data[arrayFlavorKey];
    //console.log('product & size & flavor specific data',data);

    var jsonHTML = '';
    var nutritionArray = data;
    //console.log(nutritionArray);
    jsonHTML += '<table><tbody>';

  /*
  "calories": 160,
  "calories_from_fat": 0,
  */
      jsonHTML += '<tr><td class="nutritionCategory"><span class="nutritionTag" style="font-weight:bold;">Calories</span></td><td class="nutritionStat">' + nutritionArray.calories + '</td></tr>';
      jsonHTML += '<tr><td class="nutritionCategory"><span class="nutritionTag">Calories from Fat</span></td><td class="nutritionStat">' + nutritionArray.calories_from_fat + '</td></tr>';

  /*
  "fat": 0,
    "saturated_fat": 0,
    "trans_fat": 0,
  "cholesterol": 0,
  "sodium": 15,
  */
      jsonHTML += '<tr><td class="nutritionCategory"><span class="nutritionTag" style="font-weight:bold;">Total Fat</span> (g)</td><td class="nutritionStat">' + nutritionArray.fat + '</td></tr>';
      jsonHTML += '<tr><td class="nutritionCategory"><span class="nutritionTag">Saturated Fat</span> (g)</td><td class="nutritionStat">' + nutritionArray.saturated_fat + '</td></tr>';
      jsonHTML += '<tr><td class="nutritionCategory"><span class="nutritionTag" style="font-style:italic;">Trans Fat</span> (g)</td><td class="nutritionStat">' + nutritionArray.trans_fat + '</td></tr>';
      jsonHTML += '<tr><td class="nutritionCategory"><span class="nutritionTag" style="font-weight:bold;">Cholesterol</span> (mg)</td><td class="nutritionStat">' + nutritionArray.cholesterol + '</td></tr>';
      jsonHTML += '<tr><td class="nutritionCategory"><span class="nutritionTag" style="font-weight:bold;">Sodium</span> (mg)</td><td class="nutritionStat">' + nutritionArray.sodium + '</td></tr>';

  /*
  "total_carbohydrates": 41,
    "dietary_fiber": 0,
    "sugar": 40,
  "protein": 0
  */
      jsonHTML += '<tr><td class="nutritionCategory"><span class="nutritionTag" style="font-weight:bold;">Total Carbohydrate</span> (g)</td><td class="nutritionStat">' + nutritionArray.total_carbohydrates + '</td></tr>';
      jsonHTML += '<tr><td class="nutritionCategory"><span class="nutritionTag">Dietary Fiber</span> (g)</td><td class="nutritionStat">' + nutritionArray.dietary_fiber + '</td></tr>';
      jsonHTML += '<tr><td class="nutritionCategory"><span class="nutritionTag">Sugars</span> (g)</td><td class="nutritionStat">' + nutritionArray.sugar + '</td></tr>';
      jsonHTML += '<tr><td class="nutritionCategory"><span class="nutritionTag" style="font-weight:bold;">Proteins</span> (g)</td><td class="nutritionStat">' + nutritionArray.protein + '</td></tr>';

  /*
  "vitamin_a": 0,
  "vitamin_c": 15,
  "calcium": 0,
  "iron": 0,
  */
      jsonHTML += '<tr><td class="nutritionCategory"><span class="nutritionTag">Vitamin A</span></td><td class="nutritionStat">' + nutritionArray.vitamin_a + '</td></tr>';
      jsonHTML += '<tr><td class="nutritionCategory"><span class="nutritionTag">Vitamin C</span></td><td class="nutritionStat">' + nutritionArray.vitamin_c + '</td></tr>';
      jsonHTML += '<tr><td class="nutritionCategory"><span class="nutritionTag">Calcium</span></td><td class="nutritionStat">' + nutritionArray.calcium + '</td></tr>';
      jsonHTML += '<tr><td class="nutritionCategory"><span class="nutritionTag">Iron</span></td><td class="nutritionStat">' + nutritionArray.iron + '</td></tr>';

      jsonHTML += '</tbody></table>';

    /* console.log('parsed json data, reformatted in HTML:',jsonHTML); */
    return(jsonHTML);
  }
}; //renderNutritionInfo


/********************* define listeners/document.ready functions *********************/

/*
*  (document).ready functions
*     listeners:
*       load nutritional data into "<div class='nutritionInfo'></div>"
*
*/

jQuery(document).ready(function ($) {

  /* load the nutritional JSON file in the background for home page only*/
if( (window.location.pathname.indexOf('cool-treats/menu') > -1 ) ){
  loadData();
}

if( (window.location.pathname.indexOf('cool-treats/menu/blendini') > -1 ) ){
  $('#flav-Blendini-1').children('.nutritionInfoOverlay').children('.right-block').children('.custardFlavorSelectors').css({'display':'inline-block','visibility':'visible'});
}

  /*
  *  toCurrencyString()
  */
  Number.prototype.toCurrencyString = function() {
    return this.toFixed(2).replace(/(\d)(?=(\d{3})+\b)/g,'$1 ');
  } /* toCurrencyString() */



/*********************** Event handlers *******************************
*
*   Below are the individual event handlers for the site. This includes:
*    - click handler to close nutritionalInfoOverlay popup/hide functionality per flavor
*    - click handler nutritionalInfoOverlay popup/hide functionality
*    - click handler for displaying nutritional info per size for a given flavor
*
*/

  /*
  * click handler for displaying nutritional info per size for a given flavor
  */
      $('.servingSizeSelector').click(function (event) {
        var servingSize = $(this).html();
        var parentFlavorDiv = $(this).parents('.flavor').attr('id');
        var servingSizeSelectorIDstring = "";

        $(this).parent('.servingSizeSelectors').children('.servingSizeSelector').each(function () {
          $(this).removeClass('selectedServingSize').css({'background':'#fff'});
        });

        if (parentFlavorDiv.indexOf('Blendini') > -1) {
            if( $(this).parents('.flavor').attr('class').indexOf('NULL') > -1 ) {
              $(this).parent('.servingSizeSelectors').children('.servingSizeSelector:first').addClass('selectedServingSize');
              servingSizeSelectorIDstring = $(this).parent('.servingSizeSelectors').children('.servingSizeSelector:first').attr('id');
            } else {
              $(this).parent('.servingSizeSelectors').children('.servingSizeSelector:last').addClass('selectedServingSize');
              servingSizeSelectorIDstring = $(this).parent('.servingSizeSelectors').children('.servingSizeSelector:last').attr('id');
            }
        }
        // not Blendini, allow clicked element to have class assigned
        else {
          $(this).addClass('selectedServingSize');
          servingSizeSelectorIDstring = $(this).attr('id');
        }

        var flavorName = $(this).parents('.flavor').children('.toggleNutritionInfo').children('p').html();
        var flavorWrapperID = $(this).parents('.flavor').attr('id');

        var idString = servingSizeSelectorIDstring.split("-");
        productTypeID = idString[0];
        flavorID = idString[1];
        if(flavorID == "NULL") { flavorID = null; }
        sizeID = idString[2];

        if( (parentFlavorDiv.indexOf('Gelati') > -1) || (parentFlavorDiv.indexOf('Misto') > -1) || (parentFlavorDiv.indexOf('Blendini') > -1) ) {
          var parentFlavorDivDomID = "#" + parentFlavorDiv;
          var custardSelectionValid = $(parentFlavorDivDomID).children('.nutritionInfoOverlay').children('.selectorBlock').children('.secondaryFlavorSelectors').children('.selectedSecondFlavor').html();
          //console.log('custardSelectionValid: ', custardSelectionValid);
          if( (custardSelectionValid == null) || (custardSelectionValid == "") ||(custardSelectionValid == "Custard:") ) {
            alert('Please select a flavor of custard before choosing your size.');
            return false;
          } else {
            $(parentFlavorDivDomID).children('.nutritionInfoOverlay').children('.selectorBlock').children('.secondaryFlavorSelectors').children('.selectedSecondFlavor').html('Custard: ' + secondFlavorName)
            $(parentFlavorDivDomID).children('.nutritionInfoOverlay').children('.right-block').children('.custardFlavorSelectors').hide().css({'display':'none', 'visibility':'hidden'});
            $(parentFlavorDivDomID).children('.nutritionInfoOverlay').children('.selectorBlock').children('.secondaryFlavorSelectors').show().css({'display':'inline-block', 'visibility':'visible'});
          }

          if( (parentFlavorDiv.indexOf('Gelati') > -1) || (parentFlavorDiv.indexOf('Misto') > -1) ) {
            flavorName = flavorName + ' Italian Ice with<br/>' + secondFlavorName + ' Custard';
            primaryFlavorID = flavorID;
            secondaryFlavorID = secondFlavorID;
            flavorID = primaryFlavorID + ', ' + secondaryFlavorID;
  //          console.log('product id: ', productTypeID, 'flavor name combo (ice, custard): ', flavorName, 'primary flavor id combo (ice, custard): ', flavorID, 'size id: ', sizeID);
          }  // Gelati & misto nutritional overlay update
          else if (parentFlavorDiv.indexOf('Blendini') > -1) {
            var toppingSelectionValid = $(parentFlavorDivDomID).children('.nutritionInfoOverlay').children('.selectorBlock').children('.tertiaryFlavorSelectors').children('.selectedTopping').html();
            //console.log('toppingSelectionValid: ', toppingSelectionValid);
            if( (toppingSelectionValid == null) || (toppingSelectionValid == "") || (toppingSelectionValid == "Mix-in:") ) {
              alert('Please select a topping.');
              return false;
            }
            else {
              $(parentFlavorDivDomID).children('.nutritionInfoOverlay').children('.right-block').children('.toppingSelectors').hide().css({'display':'none', 'visibility':'hidden'});
              $(parentFlavorDivDomID).children('.nutritionInfoOverlay').children('.selectorBlock').children('.tertiaryFlavorSelectors').children('.selectedTopping').html('Mix-in: ' + toppingName)
              $(parentFlavorDivDomID).children('.nutritionInfoOverlay').children('.selectorBlock').children('.tertiaryFlavorSelectors').show().css({'display':'inline-block', 'visibility':'visible'});
              primaryFlavorID = flavorID;
              if (flavorID == null) {
                primaryFlavorID = "";
              }
              secondaryFlavorID = secondFlavorID;
              if(primaryFlavorID == "") {
                //all custard blendini
                flavorID = secondaryFlavorID + ', ' + toppingID;
                flavorName = secondFlavorName + ' Custard and<br/>' + toppingName;
              } else {
                //standard blendini
                flavorID = primaryFlavorID + ', ' + secondaryFlavorID + ', ' + toppingID;
                flavorName = flavorName + ' Italian Ice with<br/>' + secondFlavorName + ' Custard and<br/>' + toppingName;
              }
            }
          } // Blendini nutritional overlay update

        } // end hybrid nutritional overlay validation


        var flavorTitle = "<div class='flavorTitle'>" + flavorName + "</div>" + "\n";
        var nutritionTable = flavorTitle;
        nutritionTable += renderNutritionInfo(productTypeID, flavorID, sizeID);
        //populate nutrition info data into div identified by .nutritionInfo
        $(this).parents('.nutritionInfoOverlay').children('.right-block').children('.nutritionInfo').html(nutritionTable).show().css({'display':'inline-block','visibility':'visible'});

      });  // .nutritionalInfoOverlay clickHandler ends

  /*
  * click handler to close nutritionalInfoOverlay popup/hide functionality per flavor
  */
      $(document).click(function (e) {
        if( (!$(e.target).parents('.flavor').length) ) {
          hideNutritionalOverlays();
          $('.custardFlavorSelectors').show().css({'display':'inline-block', 'visibility':'visible'});
          $('.secondaryFlavorSelectors').hide().css({'display':'none', 'visibility':'hidden'});
          $('.toppingSelectors').hide().css({'display':'none', 'visibility':'hidden'});
          $('.tertiaryFlavorSelectors').hide().css({'display':'none', 'visibility':'hidden'});
        }
      });



  /*
  * click handler for secondary flavor selection
  */
      $('.toggleSecondFlavorSelection').click(function (event) {
        var parentFlavorDiv = "#" + $(this).parents('.flavor').attr('id');

        secondFlavorID = $(this).parent('.custardFlavorSelector').attr('class').split(/\s+/);
        secondFlavorID = secondFlavorID[2];
        secondFlavorID = secondFlavorID.split("-");
        secondFlavorID = secondFlavorID[1];
        secondFlavorName = $(this).children('span').html();

        if( (parentFlavorDiv.indexOf('Gelati') > -1) || (parentFlavorDiv.indexOf('Misto') > -1) ) {
          $(parentFlavorDiv).children('.nutritionInfoOverlay').children('.selectorBlock').children('.secondaryFlavorSelectors').children('.selectedSecondFlavor').html('Custard: ' + secondFlavorName)
          $(parentFlavorDiv).children('.nutritionInfoOverlay').children('.right-block').children('.custardFlavorSelectors').hide().css({'display':'none', 'visibility':'hidden'});
          $(parentFlavorDiv).children('.nutritionInfoOverlay').children('.selectorBlock').children('.secondaryFlavorSelectors').show().css({'display':'inline-block', 'visibility':'visible'});
          $(parentFlavorDiv).children('.nutritionInfoOverlay').children('.selectorBlock').children('.servingSizeSelectors').children('.servingSizeSelector.defaultSize').trigger('click');
        }
        else {
          // must be blendini, show toppings/mix-in selector;
          $(parentFlavorDiv).children('.nutritionInfoOverlay').children('.selectorBlock').children('.secondaryFlavorSelectors').children('.selectedSecondFlavor').html('Custard: ' + secondFlavorName)
          $(parentFlavorDiv).children('.nutritionInfoOverlay').children('.right-block').children('.custardFlavorSelectors').hide().css({'display':'none', 'visibility':'hidden'});
          $(parentFlavorDiv).children('.nutritionInfoOverlay').children('.selectorBlock').children('.secondaryFlavorSelectors').show().css({'display':'inline-block', 'visibility':'visible'});
          $(parentFlavorDiv).children('.nutritionInfoOverlay').children('.right-block').children('.custardFlavorSelectors').show().css({'display':'none', 'visibility':'hidden'});
          $(parentFlavorDiv).children('.nutritionInfoOverlay').children('.right-block').children('.toppingSelectors').show().css({'display':'inline-block', 'visibility':'visible'});
        }

      });  // .toggleSecondFlavorSelection clickHandler ends


  /*
  * click handler for secondary flavor change in selection
  */

    chooseAnotherFlavor = function(sizeSelectorID) {
//      console.log('secondaryFlavorSelector clicked',sizeSelectorID);
      var servingSizeSelectorDiv = "#" + sizeSelectorID;
//      console.log(servingSizeSelectorDiv);
      $(servingSizeSelectorDiv).parents('.selectorBlock').children('.secondaryFlavorSelectors').hide().css({'display':'none', 'visibility':'hidden'});
      $(servingSizeSelectorDiv).parents('.nutritionInfoOverlay').children('.right-block').children('.toppingSelectors').show().css({'display':'none', 'visibility':'hidden'});
      $(servingSizeSelectorDiv).parents('.nutritionInfoOverlay').children('.right-block').children('.nutritionInfo').hide().css({'display':'none', 'visibility':'hidden'});
      $(servingSizeSelectorDiv).parents('.selectorBlock').children('.tertiaryFlavorSelectors').hide().css({'display':'none', 'visibility':'hidden'});
     $(servingSizeSelectorDiv).parents('.nutritionInfoOverlay').children('.right-block').children('.custardFlavorSelectors').show().css({'display':'inline-block', 'visibility':'visible'});
    };



  /*
  * click handler for tertiary flavor/topping selection
  */
      $('.mixInSelector').change(function () {
        var parentFlavorDiv = "#" + $(this).parents('.flavor').attr('id');

        toppingFlavorID = $(this).children('option:selected').attr('value');

        toppingFlavorID = toppingFlavorID.split("-");
        toppingID = toppingFlavorID[1];

        toppingName = $(this).children('option:selected').text();

        var blendiniSelectorID = '';
        var blendiniAllCustardSelectorID = '';

        $(parentFlavorDiv).children('.nutritionInfoOverlay').children('.selectorBlock').children('.servingSizeSelectors').children('.servingSizeSelector').each(function(){
          if( $(this).html() == "Blendini" ) {
            blendiniSelectorID = '#' + $(this).attr('id');
          } else {
            blendiniAllCustardSelectorID = '#' + $(this).attr('id');
          }
        });

        $(parentFlavorDiv).children('.nutritionInfoOverlay').children('.right-block').children('.toppingSelectors').hide().css({'display':'none', 'visibility':'hidden'});
        $(parentFlavorDiv).children('.nutritionInfoOverlay').children('.selectorBlock').children('.tertiaryFlavorSelectors').children('.selectedTopping').html('Mix-in: ' + toppingName)
        $(parentFlavorDiv).children('.nutritionInfoOverlay').children('.selectorBlock').children('.tertiaryFlavorSelectors').show().css({'display':'inline-block', 'visibility':'visible'});

        var noIce = false;
        if( $(this).parents('.flavor').attr('class').indexOf('NULL') > -1 ) { noIce = true; }
        if(noIce) {
          //console.log('no italian ice selected, must be all custard blendini');
          $(parentFlavorDiv).children('.nutritionInfoOverlay').children('.selectorBlock').children('.servingSizeSelectors').children(blendiniAllCustardSelectorID).trigger('click');
        } else {
          //console.log('italian ice selected, must be regular blendini');
          $(parentFlavorDiv).children('.nutritionInfoOverlay').children('.selectorBlock').children('.servingSizeSelectors').children(blendiniSelectorID).trigger('click');
        }

      });  // .toggleToppingSelection clickHandler ends



  /*
  * click handler for tertiary flavor/topping change in selection
  */

    chooseAnotherTopping = function(sizeSelectorID) {
      var servingSizeSelectorDiv = "#" + sizeSelectorID;
      $(servingSizeSelectorDiv).parents('.selectorBlock').children('.tertiaryFlavorSelectors').hide().css({'display':'none', 'visibility':'hidden'});
      $(servingSizeSelectorDiv).parents('.nutritionInfoOverlay').children('.right-block').children('.nutritionInfo').hide().css({'display':'none', 'visibility':'hidden'});
      $(servingSizeSelectorDiv).parents('.nutritionInfoOverlay').children('.right-block').children('.toppingSelectors').show().css({'display':'inline-block', 'visibility':'visible'});
    };



  /*
  * click handler nutritionalInfoOverlay popup/hide functionality per flavor
  */
      $('.toggleNutritionInfo').click(function (event) {
        var parentDiv = "#" + $(this).parent('.flavor').attr('id');

        hideNutritionalOverlays();

        $(parentDiv).children('.nutritionInfoOverlay').show().css({'display':'block', 'visibility':'visible'});

        var productType = $(this).parent('.flavor').attr('id');

        if( (productType.indexOf('ItalianIce') > -1) || (productType.indexOf('CreamIce') > -1)  || (productType.indexOf('SugarFreeItalianIce') > -1) || (productType.indexOf('Custard') > -1)  || (productType.indexOf('Milkshakes') > -1) ) {
          $(parentDiv).children('.nutritionInfoOverlay').children('.selectorBlock').children('.servingSizeSelectors').children('.servingSizeSelector.defaultSize').trigger('click');
        } // end click handler for italian ice and custard (single flavor products)

        if( (productType.indexOf('Toppings') > -1) ) {
          $(parentDiv).children('.nutritionInfoOverlay').children('.selectorBlock').children('.servingSizeSelectors').children('.servingSizeSelector').trigger('click');
        } // end click handler for italian ice and custard (single flavor products)
      });  // .toggleNutritionInfo clickHandler ends










/*********************** Mobile/Small Device Event handlers *******************************
*
*   Below are the individual event handlers for the site. This includes:
*    - click handler for displaying nutritional info per size for a given flavor
*
*/

  /*
  * function to display mobile-friendly nutritional info per size for a given flavor combination
  */
  var getMobileNutrition = function(productTypeID, productName, flavorID, flavorName, sizeID, productFlavorImgURL) {

        if( (productName.indexOf('Gelati') > -1) || (productName.indexOf('Misto') > -1) )
        {
//          console.log('Gelati or misto detected');
          var custardSelected = $('.mobileNutritionWidget').children('.custardSelector').children('.custardSelect').find('option:selected');
          secondFlavorName = custardSelected.text();
          secondaryFlavorID = custardSelected.attr('class');

          flavorName = flavorName + ' Italian Ice with<br/>' + secondFlavorName + ' Custard';
          primaryFlavorID = flavorID;
          flavorID = primaryFlavorID + ', ' + secondaryFlavorID;
//          console.log('product id: ', productTypeID, 'flavor name combo (ice, custard): ', flavorName, 'primary flavor id combo (ice, custard): ', flavorID, 'size id: ', sizeID);
        }
        else if (productName.indexOf('Blendini') > -1)
        {
//          console.log('Blendini detected');
          var custardSelected = $('.mobileNutritionWidget').children('.custardSelector').children('.custardSelect').find('option:selected');
          secondFlavorName = custardSelected.text();
          secondaryFlavorID = custardSelected.attr('class');

          var toppingSelected = $('.mobileNutritionWidget').children('.toppingSelector').children('.toppingSelect').find('option:selected');
          toppingName = toppingSelected.text();
          toppingID = toppingSelected.attr('class');


          primaryFlavorID = flavorID;
          if (flavorID == "NULL") { primaryFlavorID = ""; }
          flavorID = primaryFlavorID + ', ' + secondaryFlavorID;
          //console.log('product id: ', productTypeID, 'flavor name combo (ice, custard): ', flavorName, 'primary flavor id combo (ice, custard): ', flavorID, 'size id: ', sizeID);

          if(primaryFlavorID == "") {
            //all custard blendini
            flavorID = secondaryFlavorID + ', ' + toppingID;
            flavorName = secondFlavorName + ' Custard and<br/>' + toppingName;
          } else {
            //standard blendini
            flavorID = primaryFlavorID + ', ' + secondaryFlavorID + ', ' + toppingID;
            flavorName = flavorName + ' Italian Ice with<br/>' + secondFlavorName + ' Custard and<br/>' + toppingName;
          }
        }

        var flavorTitle = "<div class='flavorTitle'>" + flavorName + " flavored " + productName + "</div>" + "\n";
        var productImage = "<img class='productThumbnail' src='" + productFlavorImgURL + "' alt='' />";
        var nutritionTable = flavorTitle + productImage;
        nutritionTable += renderNutritionInfo(productTypeID, flavorID, sizeID);
        //populate nutrition info data into div identified by .nutritionInfo

        $('.mobile-product-menu').children('.nutritionInfo').html(nutritionTable);
        var posY = $('.mobile-product-menu').children('.nutritionInfo').offset().top - 10;
        $("html, body").animate({ scrollTop: posY }, 500);

  }; // getMobileNutrition


  var lookupProductName = function(jsonProductID) {
    var productName = "";

    switch (jsonProductID) {
      case "1":
        productName = "Italian Ice";
        break;
      case "2":
        productName = "Cream Ice";
        break;
      case "8":
        productName = "Sugar-Free Italian Ice";
        break;
      case "5":
        productName = "Frozen Custard";
        break;
      case "3":
        productName = "Gelati"
        break;
      case "4":
        productName = "Misto";
        break;
      case "6":
        productName = "Blendini";
        break;
      case "45":
        productName = "Milkshakes";
        break;
      case "55":
        productName = "Toppings";
        break;
      default:
        break;
    }
    return productName;
  };


  /*
  * click handlers for mobile primary flavor selection
  */
  $('.flavorSelect').change(function (event) {
        var productFlavorImgURL = $(this).find('option:selected').attr('value');
        if( productFlavorImgURL != "tbd" ) {
          var jsonProductID = $(this).parent('.selectorWrapper').attr('id').replace('flavorSelector','');
          var productName = lookupProductName(jsonProductID);
          var flavorID = $(this).find('option:selected').attr('class');
          var flavorName = $(this).find('option:selected').text();
          var servingSizeID =  $(this).parents('.mobileNutritionWidget').children('.sizeSelector').children('.sizeSelect').find('option:selected').attr('value');

          //test to make sure it is not a Hybrid product ID
          if( (jsonProductID != '3') && (jsonProductID != '4') && (jsonProductID != '6') ) {
            //console.log('product: ' + productName + '[' + jsonProductID + '], flavor: ' + flavorName + '[' + flavorID + '], sizeID: [' + servingSizeID + '], img url: ' + productFlavorImgURL);
            getMobileNutrition(jsonProductID, productName, flavorID, flavorName, servingSizeID, productFlavorImgURL);
          }
          // must be hybrid
          else {
            var activeCustardSelection = $(this).parents('.mobileNutritionWidget').children('.custardSelector').children('.custardSelect').find('option:selected').attr('value');
            if(activeCustardSelection != "tbd") {
              if ( (jsonProductID == '3') || (jsonProductID == '4') ) {
                  getMobileNutrition(jsonProductID, productName, flavorID, flavorName, servingSizeID, productFlavorImgURL);
              } // Misto & Gelati types
              else {
                // must be blendini
                var activeToppingSelection = $(this).parents('.mobileNutritionWidget').children('.toppingSelector').children('.toppingSelect').find('option:selected').attr('value');
                if(activeToppingSelection != "tbd") {
                  var noIce = false;
                  if( flavorID.indexOf('NULL') > -1 ) { noIce = true; }
                  if(noIce) {
                    $(this).parents('.mobileNutritionWidget').children('.sizeSelector').children('.sizeSelect').children('option:first').attr('selected','selected');
                  } else {
                    $(this).parents('.mobileNutritionWidget').children('.sizeSelector').children('.sizeSelect').children('option:last').attr('selected','selected');
                  }
                  servingSizeID = $(this).parents('.mobileNutritionWidget').children('.sizeSelector').children('.sizeSelect').find('option:selected').attr('value');

                  //console.log('blendini detected, custard selected: ' + activeCustardSelection + ', topping selected: ' + activeToppingSelection);
                  getMobileNutrition(jsonProductID, productName, flavorID, flavorName, servingSizeID, productFlavorImgURL);
                }
              }  // blendini select test
            } //if(activeCustardSelection)
          } //else
        } //if(productFlavorImgURL != "tbd")
    });

  /*
  * click handlers for mobile size selection
  */
  $('.sizeSelect').change(function (event) {
    var servingSizeID = $(this).find('option:selected').attr('value');
    if( servingSizeID != "tbd" ) {

      var productFlavorImgURL = $(this).parents('.mobileNutritionWidget').children('.flavorSelector').children('.flavorSelect').find('option:selected').attr('value');
      if(productFlavorImgURL != "tbd") {
        var flavorID = $(this).parents('.mobileNutritionWidget').children('.flavorSelector').children('.flavorSelect').find('option:selected').attr('class');
        var flavorName = $(this).parents('.mobileNutritionWidget').children('.flavorSelector').children('.flavorSelect').find('option:selected').text();
        var jsonProductID = $(this).parents('.mobileNutritionWidget').children('.flavorSelector').attr('id').replace('flavorSelector','');
        var productName = lookupProductName(jsonProductID);

        //console.log('product: ' + productName + '[' + jsonProductID + '], flavor: ' + flavorName + '[' + flavorID + '], sizeID: [' + servingSizeID + '], img url: ' + productFlavorImgURL);
        getMobileNutrition(jsonProductID, productName, flavorID, flavorName, servingSizeID, productFlavorImgURL);
      } //if(productFlavorImgURL != "tbd")
    } //if(servingSizeID != "tbd")
  });

  /*
  * click handlers for mobile secondary flavor selection
  */
  $('.custardSelect').change(function (event) {
    var custardFlavorID = $(this).find('option:selected').attr('value');
    if(custardFlavorID != "tbd") {
      var custardLabel = $(this).find('option:selected').text();

      var productFlavorImgURL = $(this).parents('.mobileNutritionWidget').children('.flavorSelector').children('.flavorSelect').find('option:selected').attr('value');
      if(productFlavorImgURL != "tbd") {
        var flavorID = $(this).parents('.mobileNutritionWidget').children('.flavorSelector').children('.flavorSelect').find('option:selected').attr('class');
        var flavorName = $(this).parents('.mobileNutritionWidget').children('.flavorSelector').children('.flavorSelect').find('option:selected').text();
        var jsonProductID = $(this).parents('.mobileNutritionWidget').children('.flavorSelector').attr('id').replace('flavorSelector','');
        var productName = lookupProductName(jsonProductID);

        var servingSizeID = $(this).parents('.mobileNutritionWidget').children('.sizeSelector').children('.sizeSelect').find('option:selected').attr('value');
        if( servingSizeID != "tbd" ) {

          //test for Gelati or Misto
          if( (jsonProductID == '3') || (jsonProductID == '4') ) {
            //console.log('product: ' + productName + '[' + jsonProductID + '], flavor: ' + flavorName + '[' + flavorID + '], sizeID: [' + servingSizeID + '], img url: ' + productFlavorImgURL);
            getMobileNutrition(jsonProductID, productName, flavorID, flavorName, servingSizeID, productFlavorImgURL);
          }
          // must be blendini
          else {
            var toppingID = $(this).parents('.mobileNutritionWidget').children('.toppingSelector').children('.toppingSelect').find('option:selected').attr('value');
            if ( toppingID != "tbd" ) {
              var noIce = false;
              if( flavorID.indexOf('NULL') > -1 ) { noIce = true; }
              if(noIce) {
                $(this).parents('.mobileNutritionWidget').children('.sizeSelector').children('.sizeSelect').children('option:first').attr('selected','selected');
              } else {
                $(this).parents('.mobileNutritionWidget').children('.sizeSelector').children('.sizeSelect').children('option:last').attr('selected','selected');
              }
              servingSizeID = $(this).parents('.mobileNutritionWidget').children('.sizeSelector').children('.sizeSelect').find('option:selected').attr('value');

              //console.log('product: ' + productName + '[' + jsonProductID + '], flavor: ' + flavorName + '[' + flavorID + '], sizeID: [' + servingSizeID + '], img url: ' + productFlavorImgURL);
              getMobileNutrition(jsonProductID, productName, flavorID, flavorName, servingSizeID, productFlavorImgURL);
            }
          }
        } //if(servingSizeID != "tbd")
      } //if(productFlavorImgURL != "tbd")
    } //if(custardFlavorID != "tbd")
  });


  /*
  * click handlers for mobile tertiary flavor selection
  */
  $('.toppingSelect').change(function (event) {
  var toppingID = $(this).find('option:selected').attr('value');
  if ( toppingID != "tbd" ) {

    var custardFlavorID = $(this).find('option:selected').attr('value');
    if(custardFlavorID != "tbd") {
      var custardLabel = $(this).parents('.mobileNutritionWidget').children('.custardSelector').children('.custardSelect').find('option:selected').text();

        var productFlavorImgURL = $(this).parents('.mobileNutritionWidget').children('.flavorSelector').children('.flavorSelect').find('option:selected').attr('value');
        if(productFlavorImgURL != "tbd") {
          var flavorID = $(this).parents('.mobileNutritionWidget').children('.flavorSelector').children('.flavorSelect').find('option:selected').attr('class');
          var flavorName = $(this).parents('.mobileNutritionWidget').children('.flavorSelector').children('.flavorSelect').find('option:selected').text();
          var jsonProductID = $(this).parents('.mobileNutritionWidget').children('.flavorSelector').attr('id').replace('flavorSelector','');
          var productName = lookupProductName(jsonProductID);

          var noIce = false;
          if( flavorID.indexOf('NULL') > -1 ) { noIce = true; }
          if(noIce) {
            $(this).parents('.mobileNutritionWidget').children('.sizeSelector').children('.sizeSelect').children('option:first').attr('selected','selected');
          } else {
            $(this).parents('.mobileNutritionWidget').children('.sizeSelector').children('.sizeSelect').children('option:last').attr('selected','selected');
          }
          var servingSizeID = $(this).parents('.mobileNutritionWidget').children('.sizeSelector').children('.sizeSelect').find('option:selected').attr('value');

          //console.log('product: ' + productName + '[' + jsonProductID + '], flavor: ' + flavorName + '[' + flavorID + '], sizeID: [' + servingSizeID + '], img url: ' + productFlavorImgURL);
          getMobileNutrition(jsonProductID, productName, flavorID, flavorName, servingSizeID, productFlavorImgURL);
      }  //if(productFlavorImgURL != "tbd")
    } //if(custardFlavorID != "tbd")
  } //if(toppingID != "tbd")
});



}); //jQuery(document).ready
