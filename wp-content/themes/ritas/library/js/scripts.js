/*
ritas Scripts File
Author: Anson W. Han (ahan@stargroup1.com)

This file should contain any js scripts you want to add to the site.
Instead of calling it in the header or throwing it inside wp_head()
this file will be called automatically in the footer so as not to
slow the page load.

*/

// IE8 ployfill for GetComputed Style (for Responsive Script below)
if (!window.getComputedStyle) {
    window.getComputedStyle = function(el, pseudo) {
        this.el = el;
        this.getPropertyValue = function(prop) {
            var re = /(\-([a-z]){1})/g;
            if (prop == 'float') prop = 'styleFloat';
            if (re.test(prop)) {
                prop = prop.replace(re, function () {
                    return arguments[2].toUpperCase();
                });
            }
            return el.currentStyle[prop] ? el.currentStyle[prop] : null;
        }
        return this;
    }
}

jQuery.fn.isChildOf = function(b){
    return (this.parents(b).length > 0);
};


// as the page loads, call these scripts
jQuery.noConflict();
jQuery(document).ready(function($) {

$(function() {
  $("td, h1, h2, h3, h4, h5, h6, p, span").each(function() {
      var content = $(this).html();
      content = content.replace("&reg;","<sup>&#0174;</sup>");
      content = content.replace("&#0174;","<sup>&#0174;</sup>");
      content = content.replace("®","<sup>&#0174;</sup>");
      $(this).html(content);
  });
});
/*********************** Variable Declarations *******************************
*
*   Below are the global variables for this script file
*
*/
    var contentCtrlPanelID = "";
    var easingDuration = 400;
    var collapsedHeight = "200px";
    var nHeight = "";
    var articleID = "";


/*********************** Function Declarations *******************************
*
*   Below are the individual function declarations for the site. This includes:
*    - toggling display of "Back to Top" link
*    - hide Nutritional Info Overlays
*/

    // automatically determine whether to display Back to Top link
    toggleScrollToTop = function () {
      if( $('#main').height() < $(window).height() ) {
        $('#autoTop').hide().css({'display':'none','visibility':'hidden'});
      }
    }; // toggleScrollToTop()


    // hide Nutritional Info overlays
    hideNutritionalOverlays = function () {
      $('.nutritionInfoOverlay').each(function () {
        $(this).hide().css({'display':'none', 'visibility':'hidden'});
      });
    }; // hideNutritionalOverlays


/*********************** On Page Load/Document.ready functions *******************************
*
*   Below are the scripts that run on every page load throughout the site.
*
*/
  // force Own a Rita's link in main menu
  $('.sub-menu').children('.menu-item').each(function() {
    if( $(this).children('a').attr('href') == 'http://www.ownaritas.com/' ) {
      $(this).children('a').attr('target','_blank');
    }
  });


  //add active state to main menu for selected page
  var currentURL = window.location.toString();
  $('.sub-menu').children('.menu-item').each(function() {
    if( $(this).children('a').attr('href') == currentURL ) {
      $(this).children('a').attr('class','activeMenuItem');
      $(this).parent('.sub-menu').parent('.parent-menu-item').children('a').attr('class','activeMenuItem');
    }
  });
  $('.menu-main-menu').children('.menu-item').each(function() {
    if( $(this).children('a').attr('href') == currentURL ) {
      $(this).children('a').attr('class','activeMenuItem');
    }
  });


  /*
  * hide links on tags for job postings
  */
  $('.job-posting').children('.article-footer').children('.tags').children('a').each(function() {
    $(this).replaceWith(function() { return this.innerHTML; });
  });


  /*
  * fix drop down menu for sticky menu on safari
  */
  var safariUser = ( $.browser.safari && /chrome/.test(navigator.userAgent.toLowerCase()) ) ? false : true;
  if(safariUser) {
    $('#sticky-nav').children('#menu-main-menu').children('.parent-menu-item').children('.sub-menu').css({'top':'35px'});
  }

  /*
  *   load gravatars
  */
    $('.comment img[data-gravatar]').each(function(){
        $(this).attr('src',$(this).attr('data-gravatar'));
    });



  /*
  *   automatically determine whether to display "Back to Top" link
  */
    toggleScrollToTop();




/*
* MOBILE APP SLIDER
*/

if(window.location.pathname.indexOf('mobile-app') > -1) {
  $("#mobile-app-slider").carouFredSel({
    circular: true,
    infinite: true,
    auto 	: false,
    height: 399,
    align : "center",
    prev	: {
      button	: "#mobile-app-slider-prev",
      key		: "left"
    },
    next	: {
      button	: "#mobile-app-slider-next",
      key		: "right"
    },
    pagination	: "#mobile-app-slider-pag",
    scroll : {
      fx: "fade",
      duration: 1500,
      pauseOnHover : true
    }
  });
  $("#mobile-app-slider").children("article").each(function() {
    $(this).css({ 'width': $(window).width() });
    $(this).children('.mobile-app-slide-content').children('section').css({ 'width': $(window).width() });
  });
  $("#mobile-app-slideshow").children(".pagination-wrapper").css({ 'width': $(window).width() });
}


/*
* PRESS CENTER SLIDER
*/
if(window.location.pathname.indexOf('press-center') > -1) {
  $("#featured-press-slider").carouFredSel({
    circular: true,
    infinite: true,
    auto 	: true,
    height: 440,
    align : "center",
    items : {
      visible : 1
    },
    prev	: {
      button	: "#press-slider-prev",
      key		: "left"
    },
    next	: {
      button	: "#press-slider-next",
      key		: "right"
    },
    pagination	: "#press-slider-pag",
    scroll : {
      fx: "fade",
      duration: 1800,
      pauseOnHover : true
    }
  });
} /*Close if statement to check if it's the Press Center page*/



/*
* Job Listing Contact Form Automated Subject Line
*/
if(window.location.pathname.indexOf('job-posting') > -1) {
  var jobTitle = $('.entry-title.single-title').text();
  var jobInquirySubjectText = "Job Inquiry/Application for " + jobTitle + " from RitasIce.com";
  $('.wpcf7-form-control-wrap.your-subject').children('input').val(jobInquirySubjectText).attr('');

} /*Close if statement to check if it's a job posting page*/


/*********************** Event handlers *******************************
*
*   Below are the individual event handlers for the site. This includes:
*    - window scroll handler to toggle fixed menu
*    - window resize handler to recenter slide navigation controls
*    - click handlers for expand/collapse functionality on Mission Statement and Press pages
*    - click handler for "Back to Top" functionality
*
*/

/*
*   window scroll handler to toggle fixed menu *
*/
  $(window).scroll(function () {
    var sliderHeight = 350; // height of slider atop menu in header
    var offset = $(window).scrollTop();
    //test to see if slider has been scrolled off viewport, if so- display fixed nav; otherwise conceal it
    if (offset > sliderHeight) {
      $('.fixed.nav').removeClass('hidden-nav');
    } else if( !($('.fixed.nav').hasClass('hidden-nav')) ) {
      $('.fixed.nav').addClass('hidden-nav');
    }
  });



/*
*  click handler for "Back to Top" functionality
*/
    $('#autoTop').click(function() {
      $("html, body").animate({ scrollTop: 0 }, 500);
      return false;
    });



/*
*  click handler for submit of Press Filtering functionality
*/

  var filteredPressURL = '';

    $('.press-center-button').click(function () {
      var parentDivID = $(this).parent('.press-topics-filter').parent('.press-center-block').attr('id');
      var category = $(this).parent('.press-topics-filter').children('.press-topics-dropdown').children('option:selected').attr('value');
      filteredPressURL = "/category/press-by-topics/" + category;
    });


   filterPressContent = function() {
      console.log(filteredPressURL);
      window.location.replace(filteredPressURL);
    };



/*
*  click handler for mobile Header Nav
*/
$('.menu-mobile-main-menu-container').children('select').change(function() {
var url = $('.menu-mobile-main-menu-container').children('select').children('option:selected').attr('value');
      window.location.replace(url);
});


/*
*  click handler for mobile Footer Nav
*/
$('.menu-mobile-footer-links-container').children('select').change(function() {
var url = $('.menu-mobile-footer-links-container').children('select').children('option:selected').attr('value');
      window.location.replace(url);
});



/*
*  click handler Toggle functionality, primarily on FAQs page and Rita's Rewards
*/
	$(".toggle-box").hide();

	$(".toggle").toggle(function(){
		$(this).addClass("toggle-active");
		}, function () {
		$(this).removeClass("toggle-active");
	});

	$(".toggle").click(function(){
		$(this).next(".toggle-box").slideToggle();
	});



}); /* end of as page load scripts */




var hideFrames = function() {
	$('.slideframe').each(function() {
	  $(this).css({'display':'none','height':'auto'});
	});

  $('.content-slideshow-wrapper').each(function() {
    $(this).css({'height':'auto'});
    $(this).parents('.entry-content').children('.ritas-productLink').show().css({'display':'inline-block','visibility':'visible'});
	});

  document.body.onclick = '';
} //hideFrames()


var loadFrame = function(postID,permalink) {
    var parentContentWrapperID = "#slideshow" + postID;
    $( parentContentWrapperID ).css({'height':'485px'}).parents('.entry-content').children('.ritas-productLink').hide().css({'display':'none','visibility':'hidden'});

    var iframe = "#frame" + postID;
    var slideFrame = "#slideframe" + postID;
    var iframeLoaderDivID = "#iframeLoader" + postID;
    $(slideFrame).parents('.entry-content').children('.ritas-productLink').hide().css({'display':'none','visibility':'hidden'});

    $(iframeLoaderDivID).addClass('loading');

    $(iframe).attr('src', permalink);
    $(slideFrame).css({'display':'inline-block','height':'485px'});
    $(iframeLoaderDivID).css({'z-index':'9999'});
    $(iframe).load(function() {
      $(iframeLoaderDivID).removeClass('loading');
    });

    document.onclick = hideFrames;

    $( slideFrame ).contents().find('.header').remove();

} //loadFrame(postID,permalink)

