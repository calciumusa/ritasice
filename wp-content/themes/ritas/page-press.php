<?php
/*
Template Name: Ritas Press Releases Page
Description:
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

				    <div id="main" class="first clearfix" role="main">

					    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

						    <header class="article-header">
							    <h1 class="page-title" itemprop="headline">
							      <?php
							      ritas_the_title();
							      ?>
							    </h1>
						    </header> <!-- end article header -->

                <section class="entry-content clearfix" itemprop="articleBody">
                  <?php the_content(); ?>
                </section> <!-- end article section -->

						    <footer class="article-footer">
                  <?php the_tags('<span class="tags">' . __('Tags:', 'ritastheme') . '</span> ', ', ', ''); ?>
						    </footer> <!-- end article footer -->

					    </article> <!-- end article -->

              <?php
                endwhile;
                endif;

                if (get_the_ID() == "110" ) { query_posts('cat=5'); }
                else if (get_the_ID() == "112" ) { query_posts('cat=6'); }

                if ( have_posts() ) : while ( have_posts() ) : the_post();
              ?>

                  <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix press-content'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

                    <header class="article-header">


                      <div class="press-release-header">
                      <!-- Display the date (November 16th, 2012 format) and a link to other posts by this posts author. -->
                      <?php the_time('m/d/Y') ?>

                      <!-- Display the Title as a link to the Post's permalink. -->
                      <h3 class="press-headline"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
                      </div>


                    </header> <!-- end article header -->


                    <section class="entry-content clearfix collapsible-content" itemprop="articleBody">
                      <div class="collapsible-control"><a id="pressCTRL<?php the_ID();?>" href="javascript:void(0);" class="expander-ui"></a></div>

                      <div class="press-release-excerpt">
                        <div class="press-featured-image">
                        <?php the_post_thumbnail(array(60,60)); ?>
                      </div>

                        <?php the_excerpt(); ?>
                      </div>

                      <div class="collapsed collapsible-area">
                          <div class="press-featured-image">
                            <?php the_post_thumbnail('thumbnail'); ?>
                          </div>
                          <div class="press-release-content">
                            <?php the_content(); ?>
                          </div>
                      </div><!--/.collapsible-area-->


                    </section> <!-- end article section -->


                    <footer class="article-footer">
                      <!-- Add information about pagination -->
                    </footer>  <!-- end article footer -->

                  </article> <!-- end article -->

              <?php endwhile; else: ?>

                <p>Sorry, no posts matched your criteria.</p>

              <?php endif; ?>




              <div id="autoTop">Back to Top</div><!--/#autoTop-->

    				</div> <!-- end #main -->

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>
