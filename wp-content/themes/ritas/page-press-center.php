<?php
/*
Template Name: Ritas Press Center Page
Description:
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

				    <div id="main" class="first clearfix" role="main">

				      <!-- PAGE CONTENT (IF ANY) -->

					    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						    <header class="article-header">
							    <h1 class="page-title" itemprop="headline">
							      <?php ritas_the_title(); ?>
							    </h1>


						    </header> <!-- end article header -->

                <section class="entry-content clearfix" itemprop="articleBody">
                  <?php the_content(); ?>
                </section> <!-- end article section -->

						    <footer class="article-footer">
                  <?php the_tags('<span class="tags">' . __('Tags:', 'ritastheme') . '</span> ', ', ', ''); ?>
						    </footer> <!-- end article footer -->

					    </article> <!-- end article -->

              <?php
                endwhile;
                endif;
              ?>

             <!-- END PAGE CONTENT (IF ANY) -->


				    <?php
				      $orderby = 'name';
				      $child_of = 11;


 				     $args = array (
				        'orderby'          => $orderby,
	 			        'child_of'         => $child_of
				     );

				     $catArray = get_categories( $args );
             $setSelect = false;
				     $topicDropdown = '<form class="press-topics-filter" action="javascript:filterPressContent();">';
				     $topicDropdown .= '<select class="press-topics-dropdown">';
				      foreach( $catArray as $catItem ) {
  				      $categoryName = $catItem->name;
  				      $categorySlug = $catItem->slug;
  				      $topicDropdown .= '<option value="' . $categorySlug . '"';
  				      if(!$setSelect) { $topicDropdown .= ' selected="selected"'; $setSelect = true; }
  				      $topicDropdown .= '>' . $categoryName . '</option>';
  				      }

				     $topicDropdown .= '</select>';
				     $topicDropdown .= '<input type="submit" class="press-center-button" value="GO"></input>';
				     $topicDropdown .= '</form>';

				     $sliderPagination = '<div class="clearfix"></div>';
             $sliderPagination .= '<div class="pagination-wrapper-press">';
             $sliderPagination .= '<div class="pagination" id="press-slider-pag"></div>';
             $sliderPagination .= '</div>';
				     ?>


             <?php
             for ($blockID = 0; $blockID < 4; $blockID++) {
              $parentCategoryID = null;
              $topicCategoryID = null;
              $limit = null;
              $featuredStatus = false;


               switch($blockID) {
                 case 0:
                  /* Press Releases Slider */
                  $featuredStatus = true;
                  $parentCategoryID = 5;
                  $limit = 6;
                  break;
                 case 1:
                  /* Press Releases*/
                  $parentCategoryID = 5;
                  $limit = 4;
                  break;
                 case 2:
                  /* A/V Library */
                  $parentCategoryID = 9;
                  $limit = 4;
                  break;
                default:
                 /* Image Library */
                  $parentCategoryID = 10;
                  $limit = 4;
                  break;
                }


               $args = array(
                 'post_type' => 'post',
                 'cat' => $parentCategoryID,
                 'posts_per_page' => $limit,
                 'order' => 'desc'
               );
               if($blockID == 0 ) {
                 $args = array(
                   'post_type' => 'post',
                   'cat' => $parentCategoryID,
                   'posts_per_page' => $limit,
                    'post__in'  => get_option( 'sticky_posts' ),
                    'order' => 'desc'
                 );
               }
               if($featuredStatus === true) {
                 array_push($args,"'post__in'  => get_option('sticky_posts')");
               } ?>

                <div class="press-center-block" id="press-block-<?php echo $blockID; ?>">

             <?php
                $moreLink = "";

                switch($blockID) {
                   case 0:
                    echo "<h2><strong>News</strong> Headlines</h2>";
                    echo "<div id='featured-press-slider'>";
                    break;
                   case 1:
                    echo "<h2><strong>Press</strong> Releases</h2>";
                    $moreLink = "/category/press-releases";
                    break;
                   case 2:
                    echo "<h2><strong>A/V</strong> Library</h2>";
                    $moreLink = "/category/av-library";
                    break;
                   default:
                    echo "<h2><strong>Image</strong> Library</h2>";
                    $moreLink = "/category/image-library";
                    break;
                }

              $query = new WP_Query($args);
              if ( $query->have_posts() ) {

                while ( $query->have_posts() ) : $query->the_post();


                switch($blockID) {
                   case 0:
                    if (is_sticky()) {
                      get_template_part( 'content-press-center-slider','page' );
                    }
                    break;
                   case 1:
                    get_template_part( 'content-press-release-block','page' );
                    break;
                   case 2:
                    get_template_part( 'content-press-libraries','page' );
                    break;
                   default:
                    get_template_part( 'content-press-libraries','page' );
                    break;
                } /* End switch($blockID) */

                endwhile;

                if($moreLink != "") { ?>
                  <a href="<?php echo $moreLink; ?>" class="press-center-button">See More</a>
<?php } /* link output to read more */ ?>

<?php } else { ?>
                  <p>Sorry, no posts matched your criteria.</p>
<?php } /*end if-else*/

                if($blockID == 0) {
                  echo "</div> <!--/#featured-press-slider-->";
                  echo $sliderPagination;
                }
                ?>

             </div> <!-- /#press-block-<?php echo $blockID; ?> -->



<?php
            } /* END OF FOR LOOP */
            ?>




              <div id="autoTop">Back to Top</div><!--/#autoTop-->

    				</div> <!-- end #main -->

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>
