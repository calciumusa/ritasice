<?php
/*
Template Name: Ritas Social Media Page
Description:
*/
?>

<style>
.social-media-feed {
  width: 48%;
  display: inline-block;
  float: left;
}
</style>
<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

				    <div id="main" class="first clearfix" role="main">
					    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">

						    <header class="article-header">
							    <h1 class="page-title" itemprop="headline">
							      <?php ritas_the_title(); ?>
							    </h1>
						    </header> <!-- end article header -->

                <section class="entry-content clearfix">
                  <?php the_content();?>

<div id="twitter-feed" class="social-media-feed">
<a class="twitter-timeline"  href="https://twitter.com/RitasItalianIce"  data-widget-id="349974409798369281">Tweets by @RitasItalianIce</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</div>

<div id="facebook-feed" class="social-media-feed" >
<iframe style="border: none; overflow: hidden; width:100%; height: 590px;" src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FRitasItalianIceCompany&amp;width=300&amp;height=590&amp;show_faces=true&amp;colorscheme=light&amp;stream=true&amp;show_border=true&amp;header=true&amp;appId=132208833570048" frameborder="0" scrolling="no"></iframe>
</div>

<div id="instagram-feed" class="social-media-feed">
  <h2>Rita's on Instagram</h2>
  <div id="instagram-placeholder-wrapper">
    <?php echo do_shortcode('[instapress userid="ritasice" piccount="10"]'); ?>
  </div>
</div>

                </section><!-- end article section -->

						    <footer class="article-footer">
                  <?php the_tags('<span class="tags">' . __('Tags:', 'ritastheme') . '</span> ', ', ', ''); ?>
						    </footer> <!-- end article footer -->

					    </article> <!-- end article -->

					    <?php endwhile; endif; ?>

    				</div> <!-- end #main -->

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->


<?php get_footer(); ?>
