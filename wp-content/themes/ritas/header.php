<!doctype html>
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->

	<head>
		<meta charset="utf-8">

		<title><?php wp_title(''); ?></title>

		<!-- Google Chrome Frame for IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<!-- mobile meta (hooray!) -->
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<script type="text/javascript" src="//use.typekit.net/ioz7ocu.js"></script>
        <script type="text/javascript">try{Typekit.load();}catch(e){}</script>

<!--[if IE]>
<style type="text/css">
img, div, a, input { behavior: url(<?php echo get_template_directory_uri(); ?>/library/plugins/iepngfix/iepngfix.htc) }
</style>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/plugins/iepngfix/iepngfix_tilebg.js"></script>
<![endif]-->

		<!-- icons & favicons (for more: http://themble.com/support/adding-icons-favicons/) -->
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" >
  		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<!-- wordpress head functions -->
		<?php wp_head(); ?>
		<!-- end of wordpress head -->
<!--[if lt IE 9]><script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/ie_lte_8.js"></script><![endif]-->
<script>
jQuery.noConflict();
  var safariUser = ( jQuery.browser.safari && /chrome/.test(navigator.userAgent.toLowerCase()) ) ? false : true;

if( (jQuery.browser.mozilla) || (safariUser) ){
  document.write('<link rel="stylesheet" href="/wp-content/themes/ritas/library/css/mozilla.css" type="text/css" media="all" />');
}
</script>
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/library/css/print.css" media="print">

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/plugins/carouFredSel-6.2.0/jquery.carouFredSel-6.2.0-packed.js" ></script>

</head>

<body>
<nav role="navigation" class="fixed nav hidden-nav">
<div id="sticky-nav">
<?php ritas_main_nav();?>
</div>
</nav>

<div id="container">

<header class="header" role="banner">

<?php if( is_front_page() || is_home() ) : ?>
<?php else: ?>
<?php endif; ?>


<div id="static-header-banner">
<img class="mobile-portrait" src="<?php bloginfo('template_directory'); ?>/library/images/ritas/RitasBannerL.png" alt=""/>
<img class="mobile-landscape" src="<?php bloginfo('template_directory'); ?>/library/images/ritas/RitasBannerL.png" alt=""/>
<div id="header-logo-mobile">
<a href="<?php echo home_url(); ?>" rel="nofollow"><img class="logo" src="<?php bloginfo('template_directory'); ?>/library/images/ritas/ritas-logo.png" alt="Rita's" /></a>
</div><!--/#header-logo-->

</div><!--/#static-header-banner-->


<div id="header-slideshow-wrapper">
<?php include('content-ritas-slider.php'); ?>
<div id="header-logo">
<a href="<?php echo home_url(); ?>" rel="nofollow"><img class="logo" src="<?php bloginfo('template_directory'); ?>/library/images/ritas/ritas-logo.png" alt="Rita's" /></a>
</div><!--/#header-logo-->
</div><!--/#header-slideshow-wrapper-->

<nav role="navigation" class="nav">
    <?php ritas_inverted_main_nav(); ?>
</nav>

<div id="find-ritas-slider-overlay" class="find-ritas"><a href="/locations"><img src="<?php bloginfo('template_directory'); ?>/library/images/ritas/location-icon.png"/></a></div>

<div id="header-links">
<div id="find-ritas-social-icons" class="find-ritas">
    <a href="/locations"> <img src="<?php bloginfo('template_directory'); ?>/library/images/ritas/location-icon.png"/></a>
</div>

<div id="mobile-nav">
<?php ritas_mobile_main_nav();?>
</div>

<div class="social-icons-header">
    <div class="social-icon">
    <a href="http://www.facebook.com/RitasItalianIceCompany" target="_blank" class="facebook"></a>
    </div>
    <div class="social-icon">
    <a href="http://www.twitter.com/RitasItalianIce" target="_blank" class="twitter"></a>
    </div>
</div>
</div><!--/#header-links-->

<script>
  var $ = jQuery.noConflict();
  var ie_lte_8 = false;
  if( $.browser.msie && $.browser.version < 9  && $.browser.version > 5 ) { ie_lte_8 = true; }


/*
* initialize header carousel for newer browsers
*/
var loadModernCarousel = function () {
  $("#ritas-banner-slider").carouFredSel({
    circular: true,
    infinite: true,
    auto 	: true,
    align : "center",
    items : {
      visible : 1,
      height: "350px"
    },
    prev	: {
      button	: "#ritas-banner-slider-prev",
      key		: "left"
    },
    next	: {
      button	: "#ritas-banner-slider-next",
      key		: "right"
    },
    pagination	: {
      container : "#ritas-banner-slider-pag"
    },
    scroll : {
      fx: 'crossfade',
      duration: 2500,
      pauseOnHover : true
    },
    swipe : {
      onTouch: true,
      onMouse: true
    }
  });

  $("#ritas-banner-slider").css({ 'width': $(window).width() });
  $("#header-slideshow").children(".pagination-wrapper").css({ 'width': $(window).width() });

}; //loadModernCarousel()



/*
* carousel is unavailable for IE8 and lower, instead, randomly pick/statically display one slide
*/
var loadFallbackCarousel = function () {
  var slideCount = 0;
  var slideIDs = new Array();
  $("#ritas-banner-slider").children('.ritas-slide').each(function() {
    slideIDs[slideCount] = "#" + $(this).attr('id');
    slideCount++;
    $(this).hide().css({'display':'none','visibility':'hidden'});
  });

  var randomSlideKey = Math.floor((Math.random()*slideCount)+1) - 1;
  $(slideIDs[randomSlideKey]).show().css({'display':'inline-block','visibility':'visible','width': $(window).width()});
  $(slideIDs[randomSlideKey]).children('.ritas-banner-slide-content').children('section').css({ 'width': $(window).width() });
  $("#header-slideshow").children(".pagination-wrapper").hide().css({ 'display':'none','visibility':'hidden' });
  $(".ritas-banner-prev,.ritas-banner-next").hide().css({ 'display':'none','visibility':'hidden' });

  $("#ritas-banner-slider").show().css({'display':'inline-block','visibility':'visible'});

};  //loadFallbackCarousel()



/*
* detect url of page and set mobile menu selected state as needed
*/
var mobileMenuUpdate = function() {
    var currentURL = window.location.pathname;
    $('.menu-mobile-main-menu-container').children('select').find('option:selected').removeAttr("selected");
    $('.menu-mobile-main-menu-container').children('select').children('option').each(function() {
      if( $(this).attr('value').indexOf(currentURL) > -1 ) {
        $(this).attr("selected","selected");
      }
    });
}

  /*
  * initialize Ritas Banner Slider
  */
  if(!ie_lte_8) {
    loadModernCarousel();
  } else {
    loadFallbackCarousel();
  }


/*
*   window resize handler to recenter slide navigation controls *
*/
  $(window).resize(function() {
  if($(window).width() >= 768) {
      if(!ie_lte_8) {
        loadModernCarousel();
      } else {
        loadFallbackCarousel();
      }
  } else {
    $("#ritas-banner-slider").trigger("stop");
  }
});


/*
* detect if page is loaded in an iframe, in which case, conceal header and footer, showing only content
*/
var isEmbed = false;
if(window != window.parent) { isEmbed = true; }
if(isEmbed) {
  $('.header').remove();
  $('.nav').remove();
}

</script>

</header> <!-- end header -->
