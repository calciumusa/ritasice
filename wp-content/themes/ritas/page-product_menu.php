<?php
/*
Template Name: Ritas Product Menu Template
Description:
*/
?>
<?php include(TEMPLATEPATH.'/header2.php'); ?>

<style>
#sticky-nav{display:none;}
.header, .social-crowd-wrapper, .footer{display:none;}
#content {visibility:hidden;}
</style>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

				    <div id="main" class="first clearfix" role="main">

<?php $slideshowDOMid = "slideshow" . get_the_ID(); ?>

<article id="post-<?php $this_ID; ?>" <?php post_class('clearfix menu-product-group'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

  <header class="article-header">
    <h1 class="product-title" itemprop="headline">
      <?php /*ritas_the_title();*/ ?>
    </h1>
  </header> <!-- end article header -->

  <section class="entry-content clearfix" itemprop="articleBody">
    <div id="<?php echo $slideshowDOMid; ?>" class="content-slideshow-wrapper">

      <?php the_content(); ?>

    </div><!--/.content-slideshow-wrapper-->
    <div class="clearfix"></div>

    <a class="ritas-product-prev" id="ritas-product-post-<?php $this_ID; ?>-slider-prev" style="display:block;" href="#"><span>prev</span></a>
    <a class="ritas-product-next" id="ritas-product-post-<?php $this_ID; ?>-slider-next" style="display:block;" href="#"><span>next</span></a>

  </section> <!-- end article section -->

    <footer class="article-footer">
      <?php the_tags('<span class="tags">' . __('Tags:', 'ritastheme') . '</span> ', ', ', ''); ?>
    </footer> <!-- end article footer -->

  </article> <!-- end article -->


  <script>
  /*
  * initialize Product Slider immediately after all the content for this product has been rendered
  */

  var $ = jQuery.noConflict();
  //    console.log("initalizing caroufredsel for : " + $("#<?php echo $slideshowDOMid; ?>").parent('section').parent('article').attr('id') );
      var prevButton = "#ritas-product-" + $("#<?php echo $slideshowDOMid; ?>").parent('section').parent('article').attr('id') + "-slider-prev";
      var nextButton = "#ritas-product-" + $("#<?php echo $slideshowDOMid; ?>").parent('section').parent('article').attr('id') + "-slider-next";
      $("#<?php echo $slideshowDOMid; ?>").parent('section').children('.content-slideshow-wrapper').carouFredSel({
        circular: true,
        infinite: true,
        auto : false,
        align : "center",
        height: "variable",
        items : {
          visible : 1,
          start: 0,
          height : 400
        },
        prev	: {
          button	: prevButton,
        },
        next	: {
          button	: nextButton,
        },
        scroll : {
          duration: 1500,
          pauseOnHover : true
        }
      });
  </script>

              </div> <!-- end #main -->

          </div> <!-- end #inner-content -->

        </div> <!-- end #content -->
  <style>
  #content {visibility:visible;}
  </style>

  <?php include(TEMPLATEPATH.'/footer2.php'); ?>
