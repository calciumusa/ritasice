<?php
/*
*  Template Name: Rita's Mobile App Slider
*  Description:  This template outputs the slider that showcases Rita's Mobile App;
*     dependencies include the fredcarousel jquery slider and the custom post type and
*     fields defined (using the Types plugin) and enumerated below:
*
*  Mobile App Image          wpcf-mobile-app-image                     image
*  Image Description		      wpcf-mobile-app-description        	      textarea
*  Mobile Image Status		    wpcf-mobile-image-status        	        checkbox
*
*  Author:  Sayra Lopez, The Star Group
*/
?>

<?php get_header(); ?>

<div id="content">


  	<div id="inner-content" class="wrap clearfix">


		  <div id="main" class="first clearfix" role="main">

		  <h1 class="mobile-app"> Rita's Italian Ice Mobile App </h1>

        <div id="mobile-app-slideshow" class="mobile-phone">

          <div id="mobile-app-slider">
            <?php
              $args = array( 'post_type' => 'mobile-app-slide', 'order_by' => 'menu_order', 'order' => 'ASC', 'posts_per_page' => '-1');
              $customQuery = new WP_Query( $args );
              $counter = 0;
              if ( $customQuery->have_posts() ) : while ( $customQuery->have_posts() ) : $customQuery->the_post();
            ?>

            <?php $counter++;  if (types_render_field("mobile-image-status", array("raw"=>"true") ) == 1 ) : ?>
            <?php
              $parameters = array( "raw" => "true", "url"=>"true" ); /*set parameters for parsing background image path from custom content record */
              $imgPath0 = types_render_field("mobile-app-image", $parameters);  /* get image path */
              $slideBackgroundStyle = "background:url('" . $imgPath0 . "') no-repeat center;" ;
            ?>

              <article id="post-<?php the_ID(); ?>" style="margin:0;position:relative;" <?php post_class('mobile-app-slide'); ?> >

                <img class="mobile-app-slide-background" src="<?php echo $imgPath0; ?>" alt=""/>  <!--/.slide-background-->

                <section class="mobile-app-description">
                  <?php
                  /* extract mobile appslide content for output to proper div*/
                  echo types_render_field('mobile-app-description', array("output"=>"html") );
                  ?>
                </section>
              </article>
            <?php endif; ?>

            <?php
            endwhile;
            endif;
            ?>

          </div><!--"mobile-app-slider"-->


              <div class="clearfix"></div>

            <div class="pagination-wrapper-mobile">

                <a class="mobile-app-prev" id="mobile-app-slider-prev" href="#"><span>previous</span></a>
                <a class="mobile-app-next" id="mobile-app-slider-next" href="#"><span>next</span></a>
                <div class="pagination" id="mobile-app-slider-pag"></div>
            </div>


            <div id="purchase-app">
            <div class="android">
              <a href="https://play.google.com/store/apps/details?id=com.app.ritas&hl=en" target="_blank"><img src="/wp-content/uploads/2013/04/android-app.png" /></a>
              <p>Download the Rita's App at the <br />official Apple Store. </p>
            </div>
            <div class="apple">
              <a class="desktopView" href="https://itunes.apple.com/us/app/ritas-ice/id532627057?mt=8" target="_blank"><img src="/wp-content/uploads/2013/04/apple-app.png" /></a>
              <a class="mobileView" href="itms-apps://itunes.apple.com/us/app/ritas-ice/id532627057?mt=8" target="_blank"><img src="/wp-content/uploads/2013/04/apple-app.png" /></a>
              <p>Download the Rita's App at the <br /> official Apple Store. </p>
            </div>
            <div class="clear"></div>
          </div>



          </div><!--/#mobile-app-slideshow-->


        </div> <!-- /#inner-content -->

  	</div> <!-- /#main -->

   <div class="blue-background"></div>

</div> <!-- /#content -->

<?php get_footer(); ?>