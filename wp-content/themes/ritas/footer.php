<style>
.social-crowd-wrapper, .footer { display: none; visibility: hidden; }
</style>

		 	<div class="social-crowd-wrapper">
		 	    <h1>Rita's On Social</h1>
  		  		<div class="facebook-stats">
  		  		  <a href="https://www.facebook.com/RitasItalianIceCompany" target="_blank"><img src="/wp-content/themes/ritas/library/images/ritas/facebook-crowd.png" /></a>
  		  		  <p><span class="social-crowd-numbers">
  		  		  <?php echo number_format( SocialCrowd_Stats('facebook'), 0 , '.' , ',' ); ?>
  		  		  </span><br />Likes</p>
  		  		</div>
  		  		<div class="twitter-stats">
  		  		  <a href="https://twitter.com/ritasitalianice" target="_blank"><img src="/wp-content/themes/ritas/library/images/ritas/twitter-crowd.png" /></a>
  		  		  <p><span class="social-crowd-numbers"><?php echo number_format( SocialCrowd_Stats('twitter'), 0 , '.' , ','  ); ?></span><br />Followers</p>
  		  		 </div>

  		  		<div class="youtube-stats">
  		  		  <a href="http://www.youtube.com/user/RitasItalianIce" target="_blank"><img src="/wp-content/themes/ritas/library/images/ritas/youtube-crowd.png" /></a>
  		  		  <p><span class="social-crowd-numbers"><?php echo number_format( SocialCrowd_Stats('youtube'), 0 , '.' , ','  ); ?></span><br />Followers</p>
  		  		</div>

  		  		<div class="instagram-stats">
  		  		  <a href="http://web.stagram.com/n/ritasice/" target="_blank"><img src="/wp-content/themes/ritas/library/images/ritas/instagram-crowd.png" /></a>
  		  		  <p><span class="social-crowd-numbers">
    		  		  <?php
                /* pull in and read JSON data from Instagram */
    		  		  /* Instagram requires an access token, sign into account and search help section */
    		  		  /* https://api.instagram.com/v1/users/USERID?access_token=USERACCESSTOKEN */

    		  		  $userInfoJSONPath = "https://api.instagram.com/v1/users/52998807?access_token=52998807.5b9e1e6.b1cb74a37e3e4fb8beb6880453bd40a0";
    		  		  $jsonString = file_get_contents($userInfoJSONPath);
    		  		  $jsonString  = utf8_decode($jsonString);
                $userInfoArray = json_decode($jsonString, true);

    		  		  $followers = $userInfoArray['data']['counts']['followed_by'];

    		  		  echo number_format( $followers, 0 , '.' , ','  );
    		  		  ?>
      		  		  </span><br />Followers</p>
  		  		</div>
  		  		<div class="footer-search-wrapper">
    		  		<form role="search" method="get" id="searchform" class="footer-search" action="<?php echo home_url( '/' ); ?>">
      		  		  <div>
        		  		  <input type="text" value="" name="s" id="s" class="footer-search-input" />
        		  		  <input type="submit" id="searchsubmit" class="search-submit" value="Search" />
        		  		  </div>
        		  </form>
  		  		</div>

  		   </div>

		  <footer class="footer" role="contentinfo">

				<div id="inner-footer" class="wrap clearfix">
				  <div id="footer-left-edge">
            <div id="footer-logo"><a href="/" target="_parent"><img src="<?php bloginfo('template_directory'); ?>/library/images/ritas/ritas-footer-logo.png" alt=""/></a></div>
          </div><!--/#footer-left-edge-->
          <nav role="navigation">
              <div id="footer-nav-desktop"><?php ritas_footer_links(); ?></div>
              <div id="footer-nav-mobile"><?php ritas_mobile_footer_links(); ?></div>
          </nav>
          <div id="footer-bottom-right">
            <p class="source-org copyright">
              <a href="/" target="_parent" class="mobile-footer-logo"><img src="<?php bloginfo('template_directory'); ?>/library/images/ritas/ritas-footer-logo.png" alt=""/></a>
              &copy; <?php echo date('Y'); ?> Rita's Franchise Company.
            </p>
          </div><!--/#footer-bottom-right-->
				</div> <!-- end #inner-footer -->

			</footer> <!-- end footer -->

		</div> <!-- end #container -->

		<!-- all js scripts are loaded in library/ritas.php -->
		<?php wp_footer(); ?>

<script>

/*
* detect if page is loaded in an iframe, in which case, conceal header and footer, showing only content
*/
var isEmbed = window != window.parent;
if(isEmbed) {
  $('.social-crowd-wrapper').remove();
  $('.footer').remove();
} else {
  $('.social-crowd-wrapper').css({'display':'block','visibility':'visible'});
  $('.footer').css({'display':'block','visibility':'visible'});
}

/*
* detect url of page and set mobile menu selected state as needed
*/

var currentURL = window.location.pathname;

$('.menu-mobile-footer-links-container').children('select').find('option:selected').removeAttr("selected");
$('.menu-mobile-footer-links-container').children('select').children('option').each(function() {
  if( $(this).attr('value').indexOf(currentURL) > -1 ) {
    $(this).attr("selected","selected");
  }
});


/*
* resize social crowd wrapper to match inner content
*/


</script>
</body>

</html> <!-- end page. what a ride! -->
