<?php
/*
Template Name: Featured Products List
Description:
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix" onclick="hideFrames();">

				    <div id="main" class="first clearfix" role="main">

					    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

						    <header class="article-header" style="display:none;visibility:hidden;" >
							    <h1 class="page-title" itemprop="headline">
							      <?php the_title(); ?>
							    </h1>
						    </header> <!-- end article header -->

                <section class="entry-content clearfix" itemprop="articleBody">
                  <?php the_content(); ?>
                </section><!-- end article section -->

						    <footer class="article-footer">
                  <?php the_tags('<span class="tags">' . __('Tags:', 'ritastheme') . '</span> ', ', ', ''); ?>
						    </footer> <!-- end article footer -->

					    </article> <!-- end article -->

					    <?php endwhile; endif; ?>

<?php
  /* page/post ID:     desktopMenu postID      mobileMenu postID  :  product index*/
  /*
  214: 4328: 4339 "Italian Ice";
  818: 4329: 4341 "Sugar-Free";
  820: 4330: 4342 "Cream Ice";
  238: 4332: 4343 "Custard";
  246: 4333: 4344 "Misto";
  244: 4334: 4345 "Gelati";
  541: 4336: 4346 "Blendini";
  554: 4337: 4347 "Toppings";
  556: 4338: 4348 "MilkShakes"; // no nutrition info
  558: "Frozen Lemonade";
  4191: "Sundaes";
  4214: "Frozen Custard Cakes";
  4218: "Custard Cookie Sandwiches";
  */

?>


          <?php
            $query = new WP_Query( array( 'post_type' => 'page', 'post__in' => array( 214, 820, 818, 238, 244, 246, 541, 554, 556, 558, 4191, 4214, 4218 ), 'orderby' => 'menu_order', 'order' => 'asc', 'posts_per_page'=>-1 ) );
            while ( $query->have_posts() ) : $query->the_post();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix menu-product-group'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

  <header class="article-header">
    <h1 class="product-title" itemprop="headline">
      <?php ritas_the_title(); ?>
    </h1>
  </header> <!-- end article header -->

  <section class="entry-content clearfix" itemprop="articleBody">
    <div id="slideshow<?php the_ID(); ?>" class="content-slideshow-wrapper" style="position:relative;">

      <div id="slideshow<?php the_ID(); ?>-slide0" class="content-slide">

        <?php if (has_post_thumbnail()) : ?>
        <div class="featured-image">
          <?php the_post_thumbnail(array(50,50)); ?>
        </div><!--/.featured-image-->
        <?php endif; ?>

        <div class="product-description">
          <?php the_content(); ?>

          <?php
          $postID = get_the_ID();
          if( ($postID == 214) || ($postID == 820) || ($postID == 818) || ($postID == 238) || ($postID == 244) || ($postID == 246) || ($postID == 541)|| ($postID == 556) || ($postID == 554) ) :
          ?>
            <div class="flavorNutritionLink">
              <h3>
                <a href="<?php the_permalink();?>" class="mobileView">Click to learn more about our flavors and nutrition information!</a>
                <a href="javascript:loadFrame('<?php the_ID(); ?>','<?php the_permalink(); ?>?iFramed=1');" class="desktopView"><img src="<?php bloginfo('template_directory'); ?>/library/images/ritas/nutritional_button.png" alt="Click to learn more about our flavors and nutrition information!"/></a>
              </h3>
            </div>

          <script>
            var descriptorHTML = $('.product-description').children('table').children('tbody').children('tr').children('td:last').html();
            descriptorHTML += $('.flavorNutritionLink').html();
            $('.product-description').children('table').children('tbody').children('tr').children('td:last').html(descriptorHTML);
            $('.flavorNutritionLink').remove();
          </script>

          <?php endif; ?>

        </div><!--/.product-description-->

      </div><!--/.content-slide-->

      <div id="slideframe<?php the_ID(); ?>" class="slideframe" style="text-align:right; position:absolute; top: 0; display:none; z-index:100; background-color:#fff;">
      <div id="iframeLoader<?php the_ID(); ?>" class='loading'></div>
        <iframe style="height:485px; border:none; frameborder:0; overflow:hidden;" scrolling="no" id="frame<?php the_ID(); ?>" src=""></iframe>
        <?php if(preg_match('/(?i)msie [5-8]/',$_SERVER['HTTP_USER_AGENT']))
          {
            // if IE<=8
              echo "<br /><a href='/'>close this window</a>";
          }
          ?>
      </div><!--/.slideframe-->

<script>
var $ = jQuery.noConflict();
var ie_lte_8 = false;
if( $.browser.msie && $.browser.version < 9  && $.browser.version > 5 ) { ie_lte_8 = true; }

  var setIFrameWidth = function () {
    var iFrameDivWrapperID = "#slideframe<?php the_ID(); ?>";
    var iFrameWidth = "100%";
    if(ie_lte_8) { iFrameWidth = "480px"; }
    var viewWidth = $(iFrameDivWrapperID).parents('.menu-product-group').width();

    var viewHorizOffset = $(iFrameDivWrapperID).parents('.menu-product-group').offset().left - $(iFrameDivWrapperID).parents('#content');
    var viewVertOffset = $(iFrameDivWrapperID).parents('.menu-product-group').offset().top - $(iFrameDivWrapperID).parents('.entry-content').offset().top;

    $(iFrameDivWrapperID).css({'width':viewWidth,'left':viewHorizOffset,'top':viewVertOffset }).children('iframe').css({'width':iFrameWidth});
    if(ie_lte_8) {
      $(iFrameDivWrapperID).children('iframe').css({'position':'absolute','left':'25%'});
      $(iFrameDivWrapperID).children('a').css({'position':'absolute','left':'48%','top':'0'});
    }

  };

  $(window).resize(function() {
    setIFrameWidth();
  });

  setIFrameWidth();

</script>

    </div><!--/.content-slideshow-wrapper-->

    <?php
    $postID = get_the_ID();
    if( ($postID == 214) || ($postID == 820) || ($postID == 818) || ($postID == 238) || ($postID == 244) || ($postID == 246) || ($postID == 541) || ($postID == 556) || ($postID == 554) ) :
    ?>

    <div class="clearfix"></div>
    <a class="ritas-productLink faux-prev desktopView" id="ritas-product-post-<?php the_ID();?>-iframe-toggle" href="javascript:loadFrame('<?php the_ID(); ?>','<?php the_permalink(); ?>?iFramed=1');"><span>prev</span></a>
    <a class="ritas-productLink faux-next desktopView" id="ritas-product-post-<?php the_ID();?>-iframe-toggle" href="javascript:loadFrame('<?php the_ID(); ?>','<?php the_permalink(); ?>?iFramed=1');"><span>next</span></a>
    <?php endif; ?>


  </section> <!-- end article section -->

  <footer class="article-footer">
    <?php the_tags('<span class="tags">' . __('Tags:', 'ritastheme') . '</span> ', ', ', ''); ?>
  </footer> <!-- end article footer -->

</article> <!-- end article -->



<?php
            endwhile;
          ?>



    				</div> <!-- end #main -->

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->
<script>
var $ = jQuery.noConflict();
var ie_lte_8 = false;
if( $.browser.msie && $.browser.version < 9  && $.browser.version > 5 ) { ie_lte_8 = true; }

  /*
  * on window resize, or initial load, set widths of tables and make appropriate product selectors visible
  */
  var makeProductMenusResponsive = function() {

    $('.content-slideshow-wrapper').each(function() {
    var productDescriptorTable = $(this).children('.content-slide').children('.product-description').children('table');

    if( $(window).width() >= 1024 ) {
      $(productDescriptorTable).css({'width':'940px'});
      $(productDescriptorTable).children('tbody').children('tr').children('td:first').css({'width':'300px','padding-right':'20px'}).attr('valign','top');
      if( ie_lte_8 ) {
        $('#main article img').each(function() { $(this).css({'width':'280px !important'}); });
        $(productDescriptorTable).children('tbody').children('tr').children('td:last').css({'width':'600px'}).attr('valign','top');
      }
    } else if( $(window).width() >= 960 ) {
      $(productDescriptorTable).css({'width':'860px'});
      $(productDescriptorTable).children('tbody').children('tr').children('td:first').css({'width':'250px','padding-right':'20px'}).attr('valign','top');
      if( ie_lte_8 ) {
        $('#main article img').each(function() { $(this).css({'width':'230px !important'}); });
        $(productDescriptorTable).children('tbody').children('tr').children('td:last').css({'width':'570px'}).attr('valign','top');
      }
    } else if( $(window).width() >= 768 ) {
      $(productDescriptorTable).css({'width':'100%'});
      $(productDescriptorTable).children('tbody').children('tr').children('td:first').css({'width':'30%','padding-right':'2%','vertical-align':'top'});
      $(productDescriptorTable).children('tbody').children('tr').children('td:last').css({'width':'66%'});
      if( ie_lte_8 ) {
        $('#main article img').each(function() { $(this).css({'width':'28% !important'}); });
      }
    } else {
      $(productDescriptorTable).css({'width':'95%'});
      $(productDescriptorTable).children('tbody').children('tr').children('td:first').css({'width':'120px'}).attr('valign','top');
    }

    if( $(window).width() < 768 ) {
      $('.desktop-product-menu').css({'visibility':'hidden','display':'none'});
      $('.mobile-product-menu').children('.product-description').children('table').css({'width':'95%'});
      $('.mobile-product-menu').css({'visibility':'visible','display':'block'});
    } else {
      $('.desktop-product-menu').css({'visibility':'visible','display':'block'});
      $('.mobile-product-menu').css({'visibility':'hidden','display':'none'});
    }

    });
  };


  $(window).resize(function() {
    makeProductMenusResponsive();
  });

  $('.mobile-product-menu').each(function() {
    $(this).children('.product-description').children('table').children('tbody').children('tr').children('td:last').children('h3').remove();
  });
  makeProductMenusResponsive();

</script>
<?php get_footer(); ?>