<?php
/*
Template Name: Ritas Clickable US Map
Description:
*/
?>


<?php get_header(); ?>
<?php require_once('store_locator_map_functions.php'); ?>
<?php if(isset($_GET['state'])) : ?>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDShLTzqILdrZxiaAVpTyJD4_tdd__a9vs&sensor=false"></script>
<!--<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/library/plugins/us-hover-map/css/style.css" />-->
<style>
html { height: 100% }
body { height: 100%; }
#resultsMapCanvas { height: 500px; width: 100%; }
</style>
<?php endif; ?>

<style>
#map_canvas_1 path:not([fill^="#dedede"]):hover {
  fill:#008047;
  stroke: #008047;
  stroke-width: 1px;
}
#map_table { display: none; visibility: hidden; }
#results_table { display: none; visibility: hidden; }
</style>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

				    <div id="main" class="first clearfix" role="main">
                <?php if(isset($_GET['state'])) : ?>
                  <article id='locationsByState' class='post-locationsByState page type-page status-publish hentry clearfix' role='article'>
                    <header class='article-header'>
                      <h1 class='page-title'>Rita's Locations in <?php echo $_GET['state']; ?></h1>
                    </header>
                    <section>
                      <div id="resultsMapCanvas"></div>
                      <?php filterStates($_GET['state']); ?>
                    </section><!-- end article section -->
                    <footer class="article-footer"></footer><!-- end article footer -->
                  </article>
                <?php endif; ?>

					    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">

						    <header class="article-header">
							    <h1 class="page-title" itemprop="headline">
							      <?php the_title(); ?>
							    </h1>
						    </header> <!-- end article header -->

                <section class="entry-content clearfix">
                    <?php the_content(); ?>

                    <form id="map-alternate-selector" action="/locations/locations-by-state/" >
                      <select id="state-selector" name="state">
                        <option value="tbd" selected="selected">Choose a State</option>
                        <option value="AL">Alabama</option>
                        <option value="AK">Alaska</option>
                        <option value="AZ">Arizona</option>
                        <option value="AR">Arkansas</option>
                        <option value="CA">California</option>
                        <option value="CO">Colorado</option>
                        <option value="CT">Connecticut</option>
                        <option value="DE">Delaware</option>
                        <option value="DC">District of Columbia</option>
                        <option value="FL">Florida</option>
                        <option value="GA">Georgia</option>
                        <option value="HI">Hawaii</option>
                        <option value="ID">Idaho</option>
                        <option value="IL">Illinois</option>
                        <option value="IN">Indiana</option>
                        <option value="IA">Iowa</option>
                        <option value="KS">Kansas</option>
                        <option value="KY">Kentucky</option>
                        <option value="LA">Louisiana</option>
                        <option value="ME">Maine</option>
                        <option value="MD">Maryland</option>
                        <option value="MA">Massachusetts</option>
                        <option value="MI">Michigan</option>
                        <option value="MN">Minnesota</option>
                        <option value="MS">Mississippi</option>
                        <option value="MO">Missouri</option>
                        <option value="MT">Montana</option>
                        <option value="NE">Nebraska</option>
                        <option value="NV">Nevada</option>
                        <option value="NH">New Hampshire</option>
                        <option value="NJ">New Jersey</option>
                        <option value="NM">New Mexico</option>
                        <option value="NY">New York</option>
                        <option value="NC">North Carolina</option>
                        <option value="ND">North Dakota</option>
                        <option value="OH">Ohio</option>
                        <option value="OK">Oklahoma</option>
                        <option value="OR">Oregon</option>
                        <option value="PA">Pennsylvania</option>
                        <option value="RI">Rhode Island</option>
                        <option value="SC">South Carolina</option>
                        <option value="SD">South Dakota</option>
                        <option value="TN">Tennessee</option>
                        <option value="TX">Texas</option>
                        <option value="UT">Utah</option>
                        <option value="VT">Vermont</option>
                        <option value="VA">Virginia</option>
                        <option value="WA">Washington</option>
                        <option value="WV">West Virginia</option>
                        <option value="WI">Wisconsin</option>
                        <option value="WY">Wyoming</option>
                      </select>
                    </form>


                <?php build_i_world_map(1); ?>

                <?php /*include("library/plugins/us-hover-map/usa-map-ritas.php"); */?>
                <?php /*classifyActiveStates(); */ ?>

                </section><!-- end article section -->

						    <footer class="article-footer">
                  <?php the_tags('<span class="tags">' . __('Tags:', 'ritastheme') . '</span> ', ', ', ''); ?>
						    </footer> <!-- end article footer -->

					    </article> <!-- end article -->

					    <?php endwhile; endif; ?>

    				</div> <!-- end #main -->

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<script>
var $ = jQuery.noConflict();

$(document).ready(function() {
  var url = window.location.toString();
  if( url.indexOf("/?state=") > -1 ) {
    //console.log("address variable detected");
    var state = url.substring(url.indexOf("=")+1);
    $("#results_table").children("#map_sidebar").children('h2:first').html("Rita's locations in " + state);
    $("#results_table").css({'display':'inline-block','visibility':'visible'});
    $("#resultsMapCanvas").css({'display':'inline-block','visibility':'visible'});
  }

  $('#state-selector').change(function (event){
    var stateSelected = $(this).find('option:selected').attr('value');
    if(stateSelected != "tbd") {
      $('#map-alternate-selector').submit();
    }
    else {
      return false;
    }
  });

});
</script>

<?php get_footer(); ?>
