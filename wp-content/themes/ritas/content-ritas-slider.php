<?php
/*
  Template Name: Rita's Slider
  Description:  This template outputs the slider that displays at the top of the homepage on the site; it depends on the custom post type and fields defined (using the Types plugin) and enumerated below:

  Display/Activate Slide?		wpcf-ritas-slider-status	                checkbox
  Background Image          wpcf-ritas-slider-background-image        image
  Content Headline		      wpcf-ritas-slider-content-headline	      textarea
  Content Position          wpcf-ritas-slider-content-position        radio
  Content Blurb             wpcf-ritas-slider-content                 wysiwyg
  Content Button Text		    wpcf-ritas-slider-button-text           	textfield
  Content Button Link		    wpcf-ritas-slider-button-link	            url
  Featured Image		        wpcf-ritas-slider-featured-image	        image
  Featured Image Link		    wpcf-ritas-slider-featured-image-link	    url
  Featured Image Caption		wpcf-ritas-slider-featured-image-caption	textarea

*/
?>

<div id="header-slideshow">

  <div id="ritas-banner-slider" style="height:350px;width:100%;">
    <?php
      $args = array( 'post_type' => 'ritas-slide', 'orderby' => 'menu_order', 'order' => 'ASC', 'posts_per_page' => '-1');
      $customQuery = new WP_Query( $args );
      $counter = 0;
      if ( $customQuery->have_posts() ) : while ( $customQuery->have_posts() ) : $customQuery->the_post();
    ?>

    <?php $counter++;  if (types_render_field("ritas-slider-status", array("raw"=>"true") ) == 1 ) : ?>
    <?php
        $parameters = array( "raw" => "true", "url"=>"true" ); /*set parameters for parsing background image path from custom content record */
        $imgPath0 = types_render_field("ritas-slider-background-image", $parameters);  /* get image path */
        $urlPath0 = types_render_field("ritas-slider-featured-image-link", $parameters);  /* get url path */
        $slideBackgroundStyle = "background:url('" . $imgPath0 . "') no-repeat top center;" ;
    ?>
    <?php if ($urlPath0 != "") { ?>
    <div class="ritas-banner-slide" id="ritas-banner-slide-<?php the_ID();?>" style="margin:0 auto;height:350px;<?php echo $slideBackgroundStyle;?>"><a href="<?php echo $urlPath0; ?>" style="margin:0 auto;display:inline-block;height:100%;width:100%;"></a></div>
    <?php } else { ?>
    <div class="ritas-banner-slide" id="ritas-banner-slide-<?php the_ID();?>" style="margin:0 auto;height:350px;<?php echo $slideBackgroundStyle;?>"></div>
    <?php } ?>

    <?php endif; ?>
    <?php
      endwhile;
      endif;
    ?>

  </div><!--"ritas-banner-slider"-->
  <div class="clearfix"></div>

  <a class="ritas-banner-prev" id="ritas-banner-slider-prev" href="#"><span>prev</span></a>
  <a class="ritas-banner-next" id="ritas-banner-slider-next" href="#"><span>next</span></a>
  <div class="pagination-wrapper">
    <div class="pagination" id="ritas-banner-slider-pag"></div>
  </div>

</div><!--/#header-slideshow-->
