<!doctype html>
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->

	<head>
		<meta charset="utf-8">

		<title><?php wp_title(''); ?></title>

		<!-- Google Chrome Frame for IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<!-- mobile meta (hooray!) -->
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<script type="text/javascript" src="//use.typekit.net/ioz7ocu.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>

<!--[if IE]>
<style type="text/css">
img, div, a, input { behavior: url(<?php echo get_template_directory_uri(); ?>/library/plugins/iepngfix/iepngfix.htc) }
</style>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/plugins/iepngfix/iepngfix_tilebg.js"></script>
<![endif]-->

		<!-- icons & favicons (for more: http://themble.com/support/adding-icons-favicons/) -->
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" >
  		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<!-- wordpress head functions -->
		<?php wp_head(); ?>
		<!-- end of wordpress head -->
<!--[if lt IE 9]><script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/ie_lte_8.js"></script><![endif]-->
<script>
jQuery.noConflict();
  var safariUser = ( jQuery.browser.safari && /chrome/.test(navigator.userAgent.toLowerCase()) ) ? false : true;

if( (jQuery.browser.mozilla) || (safariUser) ){
  document.write('<link rel="stylesheet" href="/wp-content/themes/ritas/library/css/mozilla.css" type="text/css" media="all" />');
}
</script>
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/library/css/print.css" media="print">

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/plugins/carouFredSel-6.2.0/jquery.carouFredSel-6.2.0-packed.js" ></script>

</head>

<body>

<div id="container">








