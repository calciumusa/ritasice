<?php
/*
File Description: Content For Blocks in the Press Center Page
*/


?>

<article class="latest-news" id="post-<?php the_ID(); ?>" <?php post_class('clearfix press-content'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

  <header>
    <div class="latest-news-header">
      <div class="latest-news-image"> <?php the_post_thumbnail('thumbnail'); ?></div>                      
      <!-- Display the Title as a link to the Post's permalink. -->
      <h3 class="latest-news-headline"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
      <!-- Display the date (November 16th, 2012 format) and a link to other posts by this posts author. -->
   
    </div><!-- end press-release-header -->
   
  </header> 
  
  <footer class="article-footer">
    <!-- Add information about pagination -->
  </footer>  <!-- end article footer -->
</article> <!-- end article -->
