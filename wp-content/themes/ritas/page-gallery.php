<?php
/*
Template Name: Ritas Page with Gallery Template
Description:
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

				    <div id="main" class="first clearfix" role="main">

					    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix flash-embed'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

						    <header class="article-header">
							    <h1 class="page-title" itemprop="headline">
							      <?php the_title(); ?>
							    </h1>
						    </header> <!-- end article header -->

                <section class="entry-content clearfix collapsible-content" itemprop="articleBody">
                  <?php the_content(); ?>

                  <div id="nextgen-gallery" class="gallery">
                    <?php
                      switch(get_the_id()) {
                        case 33:
                          /* first day of spring */
                          echo do_shortcode('[nggallery id=3]');
                          break;
                        case 35:
                          /* alex's lemonade stand */
                          echo do_shortcode('[nggallery id=2]');
                          break;
                        case 61:
                          /* flat ice guy */
                          echo do_shortcode('[nggallery id=4]');
                          break;
                        default:
                          break;
                        }
                    ?>
                  </div><!--/#mission-page-gallery-->

                </section><!-- end article section -->

						    <footer class="article-footer">
                  <?php the_tags('<span class="tags">' . __('Tags:', 'ritastheme') . '</span> ', ', ', ''); ?>
						    </footer> <!-- end article footer -->

					    </article> <!-- end article -->

					    <?php endwhile; endif; ?>

    				</div> <!-- end #main -->

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->
<script>
var $ = jQuery.noConflict();
$('#nextgen-gallery').width('100%').height('auto');
</script>
<?php get_footer(); ?>
