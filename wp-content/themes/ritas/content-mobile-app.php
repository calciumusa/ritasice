<?php
 $parameters = array( "raw" => "true", "url"=>"true" ); /*set parameters for parsing background image path from custom content record */
 $imgPath0 = types_render_field("mobile-app-image", $parameters);  /* get image path */
?>

<article id="post-<?php the_ID(); ?>" style="margin:0;position:relative;" <?php post_class('mobile-app-slide'); ?> >
  <img class="mobile-app-slide-background" src="<?php echo $imgPath0; ?>" alt=""/>
  <section class="mobile-app-description">
    <?php
    /* extract mobile appslide content for output to proper div*/
    echo types_render_field('mobile-app-description', array("output"=>"html") );
    ?>
  </section>
</article>
