<?php
/*
Template Name: Product Nutrition Selector Menu
Description: optimized template, pulls in introductory descriptor post and subsequent flavor/size selection menu for nutritional info.
*/
?>
<?php
$iFrame = false;
if(isset($_GET['iFramed'])) { $iFrame = true; }
$UApatternString = '/(iPhone|iPod|iPad|Android|BlackBerry|Phone)/';
$isMobile = preg_match($UApatternString, $_SERVER['HTTP_USER_AGENT']);
if ( (!$isMobile) && ($iFrame) ) {
  include(TEMPLATEPATH.'/header2.php');
}
else {
  get_header();
}
?>

<div id="content">
  <div id="inner-content" class="wrap clearfix <?php if($iFrame){ echo 'iframed-container'; } ?>" onclick="hideFrames();">
    <div id="main" class="first clearfix" role="main">

<?php if (have_posts()) : the_post(); ?>
<?php $productDescriptorContent = get_the_content(); ?>
<?php $slideshowDOMid = "slideshow" . get_the_ID(); ?>

  <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix menu-product-group'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

    <header class="article-header">
      <h1 class="product-title" itemprop="headline"><?php ritas_the_title(); ?></h1>
    </header> <!-- end article header -->

    <section class="entry-content clearfix" itemprop="articleBody">

      <div class="desktop-product-menu">
        <div id="<?php echo $slideshowDOMid; ?>" class="content-slideshow-wrapper">
<?php if ( (!$isMobile) && (!$iFrame) ) : ?>
          <div id="slideshow<?php the_ID(); ?>-slide0" class="content-slide">
            <div class="product-description">
              <?php the_content(); ?>
            </div><!--/.product-description-->
          </div><!--/.content-slide-->
<?php endif; ?>

  <?php
  /* page/post ID:     desktopMenu postID      mobileMenu postID  :  product index*/
  /*
  214: 4328: 4339 "Italian Ice";
  818: 4329: 4341 "Sugar-Free";
  820: 4330: 4342 "Cream Ice";
  238: 4332: 4343 "Custard";
  246: 4333: 4344 "Misto";
  244: 4334: 4345 "Gelati";
  541: 4336: 4346 "Blendini";
  554: 4337: 4347 "Toppings";
  556: 4338: 4348 "MilkShakes"; // no nutrition info
  558: "Frozen Lemonade";
  4191: "Sundaes";
  4214: "Frozen Custard Cakes";
  4218: "Custard Cookie Sandwiches";
  */

      $wp_post_ID = '';
      $wp_post_mobileID = '';
      $productDescriptorPostID = get_the_ID();
      switch($productDescriptorPostID) {
        case  214:
        /* italianIce */
          $wp_post_ID = '4328';
          $wp_post_mobileID = '4339';
          break;
        case 818:
        /* sugarFreeItalianIce */
          $wp_post_ID = '4329';
          $wp_post_mobileID = '4341';
          break;
        case 820:
        /* creamIce */
          $wp_post_ID = '4330';
          $wp_post_mobileID = '4342';
          break;
        case 238:
        /* custard */
          $wp_post_ID = '4332';
          $wp_post_mobileID = '4343';
          break;
        case 246:
        /* misto */
          $wp_post_ID = '4333';
          $wp_post_mobileID = '4344';
          break;
        case 244:
        /* gelati */
          $wp_post_ID = '4334';
          $wp_post_mobileID = '4345';
          break;
        case 541:
        /* blendini */
          $wp_post_ID = '4336';
          $wp_post_mobileID = '4346';
          break;
        case 556:
        /* milkshakes */
          $wp_post_ID = '4338';
          $wp_post_mobileID = '4348';
          break;
        case 554:
        /* toppings */
          $wp_post_ID = '4337';
          $wp_post_mobileID = '4347';
          break;
        default:
          break;
      }  // end switch

      $query = new WP_Query( array( 'post_type' => 'product-menu', 'p' => $wp_post_ID ) );
      if ( $query->have_posts() ) : $query->the_post();
        the_content();
      endif;

  ?>
        </div><!--/#<?php echo $slideshowDOMid; ?>.content-slideshow-wrapper-->
        <div class="clearfix"></div>
        <a class="ritas-product-prev desktop-product-menu-nav" id="ritas-product-post-<?php echo $productDescriptorPostID; ?>-slider-prev" href="#"><span>prev</span></a>
        <a class="ritas-product-next desktop-product-menu-nav" id="ritas-product-post-<?php echo $productDescriptorPostID; ?>-slider-next" href="#"><span>next</span></a>
      </div><!--/.desktop-product-menu-->

<?php
    echo '<div class="mobile-product-menu">';
    $mobileQuery = new WP_Query( array( 'post_type' => 'product-menu', 'p' => $wp_post_mobileID ) );
    if ( $mobileQuery->have_posts() ) : $mobileQuery->the_post();
      the_content();
    endif;
    echo '</div>';
    echo '<div class="nutritionInfo"></div>';
?>

    </section><!-- end article section -->

    <footer class="article-footer">
      <?php the_tags('<span class="tags">' . __('Tags:', 'ritastheme') . '</span> ', ', ', ''); ?>
    </footer> <!-- end article footer -->

  </article> <!-- end article -->

<?php endif; ?>

    </div><!--/#main -->
  </div><!--/#inner-content -->
</div><!--/#content -->


<script>
var $ = jQuery.noConflict();
var ie_lte_8 = false;
if( $.browser.msie && $.browser.version < 9  && $.browser.version > 5 ) { ie_lte_8 = true; }

  var parentPostID = $('.content-slideshow-wrapper').attr('id');
  var parentPostDOMID = "#" + parentPostID;
  $(parentPostDOMID).children('.content-slide').each(function() {
    $(this).attr('id', ( $(this).attr('id').replace("slideshow1",parentPostID) ) ).css({'height':'420px'});
  });
  $(parentPostDOMID).parents('.entry-content').css({'margin-top':'10px'});


  /*
  * initialize/resume Product Slider immediately after all the content for this product has been rendered
  */
  //    console.log("initalizing caroufredsel for : " + $("#<?php echo $slideshowDOMid; ?>").parent('section').parent('article').attr('id') );



      var prevButton = "#ritas-product-" + $("#<?php echo $slideshowDOMid; ?>").parent('.desktop-product-menu').parent('section').parent('article').attr('id') + "-slider-prev";
      var nextButton = "#ritas-product-" + $("#<?php echo $slideshowDOMid; ?>").parent('.desktop-product-menu').parent('section').parent('article').attr('id') + "-slider-next";
    $("#<?php echo $slideshowDOMid; ?>").parent('.desktop-product-menu').children('.content-slideshow-wrapper').carouFredSel({
        circular: true,
        infinite: true,
        auto 	: false,
        align : "center",
        height: 420,
        items : {
          visible : 1,
          height: 420
        },
        prev	: {
          button	: prevButton,
        },
        next	: {
          button	: nextButton,
        },
        scroll : {
          duration: 1500,
          pauseOnHover : true
        }
      });


  /*
  * on window resize, or initial load, set widths of tables and make appropriate product selectors visible
  */
  var makeProductMenusResponsive = function() {

    var productDescriptorTable = $('.content-slideshow-wrapper').children('.content-slide').children('.product-description').children('table');
    if( $(window).width() >= 1024 ) {
      $(productDescriptorTable).css({'width':'940px'});
      $(productDescriptorTable).children('tbody').children('tr').children('td:first').css({'width':'300px','padding-right':'20px'}).attr('valign','top');
      if( ie_lte_8 ) {
        $('#main article img').each(function() { $(this).css({'width':'280px !important'}); });
        $(productDescriptorTable).children('tbody').children('tr').children('td:last').css({'width':'600px'}).attr('valign','top');
      }
    } else if( $(window).width() >= 960 ) {
      $(productDescriptorTable).css({'width':'860px'});
      $(productDescriptorTable).children('tbody').children('tr').children('td:first').css({'width':'250px','padding-right':'20px'}).attr('valign','top');
      if( ie_lte_8 ) {
        $('#main article img').each(function() { $(this).css({'width':'230px !important'}); });
        $(productDescriptorTable).children('tbody').children('tr').children('td:last').css({'width':'570px'}).attr('valign','top');
      }
    } else if( $(window).width() >= 768 ) {
      $(productDescriptorTable).css({'width':'640px'});
      $(productDescriptorTable).children('tbody').children('tr').children('td:first').css({'width':'30%','padding-right':'2%','vertical-align':'top'});
      $(productDescriptorTable).children('tbody').children('tr').children('td:last').css({'width':'66%'});
      if( ie_lte_8 ) {
        $('#main article img').each(function() { $(this).css({'width':'28% !important'}); });
      }
    } else {
      $(productDescriptorTable).css({'width':'95%'});
      $(productDescriptorTable).children('tbody').children('tr').children('td:first').css({'width':'120px'}).attr('valign','top');
    }

    if( $(window).width() < 768 ) {
      $('.desktop-product-menu').css({'visibility':'hidden','display':'none'});
      $('.mobile-product-menu').children('.product-description').children('table').css({'width':'95%'});
      $('.mobile-product-menu').css({'visibility':'visible','display':'block'});
    } else {
      $('.desktop-product-menu').css({'visibility':'visible','display':'block'});
      $('.mobile-product-menu').css({'visibility':'hidden','display':'none'});
    }
  };


  $(window).resize(function() {
    $("#<?php echo $slideshowDOMid; ?>").parent('.desktop-product-menu').children('.content-slideshow-wrapper').trigger('stop');
    makeProductMenusResponsive();
    $("#<?php echo $slideshowDOMid; ?>").parent('.desktop-product-menu').children('.content-slideshow-wrapper').carouFredSel();
  });

  $('.mobile-product-menu').children('.product-description').children('table').children('tbody').children('tr').children('td:last').children('h3').remove();

  makeProductMenusResponsive();


  $('.flavor-NULL').children('a').children('img').attr('alt','All Custard Blendini');
  $('.flavor-NULL').children('a').children('p').html('All Custard Blendini');
  $('#flavorSelector6').children('.flavorSelect').find('option.NULL').text('All Custard Blendini');

  $('#content-slideshow-wrapper').children('.content-slide').each(function() {
    $(this).css({'height':'420px'});
  });

</script>

<?php
if ( (!$isMobile) && ($iFrame) ) {
  include(TEMPLATEPATH.'/footer2.php');
}
else {
  get_footer();
}
?>
