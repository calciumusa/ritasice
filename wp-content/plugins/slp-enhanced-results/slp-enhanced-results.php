<?php
/**
 * Plugin Name: Store Locator Plus : Enhanced Results
 * Plugin URI: http://www.charlestonsw.com/product/store-locator-plus-enhanced-results/
 * Description: A premium add-on pack for Store Locator Plus that adds enhanced search results to the plugin.
 * Version: 0.8
 * Author: Charleston Software Associates
 * Author URI: http://charlestonsw.com/
 * Requires at least: 3.3
 * Test up to : 3.5.1
 *
 * Text Domain: csa-slp-er
 * Domain Path: /languages/
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// No SLP? Get out...
//
include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); 
if ( !function_exists('is_plugin_active') ||  !is_plugin_active( 'store-locator-le/store-locator-le.php')) {
    return;
}

/**
 * The Enhanced Results Add-On Pack for Store Locator Plus.
 *
 * @package StoreLocatorPlus\EnhancedResults
 * @author Lance Cleveland <lance@charlestonsw.com>
 * @copyright 2012-2013 Charleston Software Associates, LLC
 */
class SLPEnhancedResults {

    //------------------------------------------------------
    // Properties
    //------------------------------------------------------
    private $dir;        
    private $metadata;
    private $slug;
    private $url;

    /**
     * The minimum version of SLP that is required to use this version.
     *
     * Set this whenever we depend on a specific version of SLP.
     *
     * @var string $min_slp_version
     */
    private $min_slp_version        = '3.11';



    /**
     * The name of the plugin.
     * 
     * @var string $name
     */
    private $name;

    /**
     * The main plugin.
     * 
     * @var SLPlus $plugin
     */
    public  $plugin;

    //------------------------------------------------------
    // METHODS
    //------------------------------------------------------

    /**
     * Invoke the Enhanced Results plugin.
     *
     * @static
     */
    public static function init() {
        static $instance = false;
        if ( !$instance ) {
            load_plugin_textdomain( 'csa-slp-er', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
            $instance = new SLPEnhancedResults;
        }
        return $instance;
    }

    /**
     * Constructor
     */
    function SLPEnhancedResults() {
        add_action('slp_init_complete'          ,array($this,'slp_init')                            );
        add_action('slp_admin_menu_starting'    ,array($this,'admin_menu'));
    }

    /**
     * After SLP initializes, do this.
     *
     * SLP Action: slp_init_complete
     *
     * @return null
     */
    function slp_init() {
        if (!$this->setPlugin()) { return; }

        // Check main plugin version is good enough.
        //
        if (version_compare(SLPLUS_VERSION, $this->min_slp_version, '<')) {
            if (is_admin()) {
                $this->plugin->notifications->add_notice(
                        4,
                        sprintf(
                            __('You have %s version %s. You need version %s or greater for this version of %s.','csa-slp-es'),
                            __('Store Locator Plus','csa-slp-er'),
                            $this->plugin->version,
                            $this->min_slp_version,
                            __('Enhanced Results'   ,'csa-slp-er')
                            )
                        );
            }
            return;
        }

        // Set Properties
        //
        $this->url = plugins_url('',__FILE__);
        $this->dir = plugin_dir_path(__FILE__);
        $this->slug = plugin_basename(__FILE__);
        $this->name = __('Enhanced Results','csa-slp-er');

        // Tell SLP we are here
        //
        $this->plugin->register_addon($this->slug);

        // Hooks and Filters
        //
        add_filter('slp_mysql_search_query'         ,array($this,'tweak_mysql_search'   ),90);
        add_filter('slp_results_marker_data'        ,array($this,'modify_marker'        )   );
        add_filter('slp_javascript_results_string'  ,array($this,'mangle_results_output'),90);
    }

    //====================================================
    // Helpers
    //====================================================

    /**
     * Set the plugin property to point to the primary plugin object.
     *
     * Returns false if we can't get to the main plugin object.
     *
     * @global wpCSL_plugin__slplus $slplus_plugin
     * @return boolean true if plugin property is valid
     */
    function setPlugin() {
        if (!isset($this->plugin) || ($this->plugin == null)) {
            global $slplus_plugin;
            $this->plugin = $slplus_plugin;
        }
        return (isset($this->plugin) && ($this->plugin != null));
    }


    //====================================================
    // WordPress Admin Actions
    //====================================================

    /**
     * WordPress admin_init hook.
     */
    function admin_init(){
        // Admin panel hooks for search form settings
        //
        add_filter('slp_settings_results_locationinfo'  ,array($this,'slp_add_results_settings_panel')  ,20 );
        add_filter('slp_settings_results_labels'        ,array($this,'filter_AddLabelSettings')             );
        add_action('slp_save_map_settings',
                array($this,'slp_save_map_settings'),
                '9'
                );

        // WordPress Update Checker - if this plugin is active
        //
        if (is_plugin_active($this->slug)) {
            $this->metadata = get_plugin_data(__FILE__, false, false);
            if (!$this->setPlugin()) { return; }    
            $this->Updates = new SLPlus_Updates(
                    $this->metadata['Version'],
                    $this->plugin->updater_url,
                    $this->slug
                    );
        }

        // Admin Filters
        //
        add_filter('slp_edit_location_right_column' ,array($this,'filter_AddFieldsToEditForm'                   ),15        );
        add_filter('slp_manage_location_columns'    ,array($this,'filter_AddFieldHeadersToManageLocations'      )           );
        add_filter('slp_column_data'                ,array($this,'filter_AddFieldDataToManageLocations'         ),90    ,3  );
    }

    /**
     * WordPress admin_menu hook for Enhanced Results.
     */
    function admin_menu(){
        if (!$this->setPlugin()) { return; }
        $this->adminMode = true;
        add_action('admin_init' , array($this,'admin_init'));
    }        

    /**
     * Add extra fields that show in results output to the edit form.
     *
     * SLP Filter: slp_edit_location_right_column
     *
     * @param string $theForm the original HTML form for the manage locations edit (right side)
     * @return string the modified HTML form
     */
    function filter_AddFieldsToEditForm($theHTML) {
        $locID = $this->plugin->currentLocation->id;
        $addform = $this->plugin->AdminUI->addingLocation;

        return $theHTML .=
            '<div id="slp_er_fields"  class="slp_editform_section"><strong>'.$this->name.'</strong><br/>'.
                "<input name='image-$locID' value='".($addform?'':$this->plugin->currentLocation->image)."'> ".
                '<small>'.
                   __('Bubble Image URL', 'csa-slp-er').
                '</small>' .
            '</div>'
            ;
    }

    /**
     * Add more settings to the Result / Label section of Map Setings.
     *
     * @param string $HTML the initial form HTML
     * @return string the modified form HTML
     */
    function filter_AddLabelSettings($HTML) {
        if (!$this->setPlugin()) { return ''; }
        return $HTML .
            '<strong>'.$this->name.'</strong><br/>'.
            $this->plugin->AdminUI->MapSettings->CreateInputDiv(
                    '_message_noresultsfound',
                    __('No Results Message', 'csa-slp-er'),
                    __('No results found message that appears under the map.','csa-slp-er'),
                    SLPLUS_PREFIX,
                    __('Results not found.','csa-slp-er')
                    );
    }

    /**
     * Add the images column header to the manage locations table.
     *
     * SLP Filter: slp_manage_location_columns
     *
     * @param mixed[] $currentCols column name + column label for existing items
     * @return mixed[] column name + column labels, extended with our extra fields data
     */
    function filter_AddFieldHeadersToManageLocations($currentCols) {
        return array_merge($currentCols,
                array(
                    'sl_image'       => __('Image'        ,'csa-slp-er'),
                )
            );

    }

    /**
     * Render the extra fields on the manage location table.
     *
     * SLP Filter: slp_column_data
     *
     * @param string $theData  - the option_value field data from the database
     * @param string $theField - the name of the field from the database (should be sl_option_value)
     * @param string $theLabel - the column label for this column (should be 'Categories')
     * @return type
     */
    function filter_AddFieldDataToManageLocations($theData,$theField,$theLabel) {
        if (
            ($theField === 'sl_image') &&
            ($theLabel === __('Image'        ,'csa-slp-er'))
           ) {
            $theData =($this->plugin->currentLocation->image!='')?
                      "<a href='".$this->plugin->currentLocation->image.
                            "' target='blank'>".__("View", 'csa-slplus')."</a>" :
                      "" ;
        }
        return $theData;
    }

    //====================================================
    // Enhanced Results
    //====================================================

    /**
     * Modify the marker array after it is loaded with SLP location data.
     *
     * @param named array $marker - the SLP data for a single marker
     */
    function modify_marker($marker) {
        if (!$this->setPlugin()) { return ''; }
        if (($this->plugin->settings->get_item('enhanced_results_add_tel_to_phone',0) == 1)) {
            $marker['phone'] = sprintf('<a href="tel:%s">%s</a>',$marker['phone'],$marker['phone']);
        }
        if (($this->plugin->settings->get_item('enhanced_results_show_country',0) == 0)) {
            $marker['country'] = '';
        }
        return $marker;
    }

    /**
     * Method: slp_save_map_settings
     */
    function slp_save_map_settings() {
        if (!$this->setPlugin()) { return ''; }

        // Custom / pull downs
        //
        if (isset($_POST[SLPLUS_PREFIX.'-enhanced_results_orderby']   )) {
            update_option(
                    SLPLUS_PREFIX.'-enhanced_results_orderby',
                    $_POST[SLPLUS_PREFIX.'-enhanced_results_orderby']
                    );
        }

        // Checkboxes
        //
        $BoxesToHit = array(
            'enhanced_results_add_tel_to_phone',
            'enhanced_results_hide_distance_in_table',
            'enhanced_results_show_country',
            'enhanced_results_show_hours',
            );
        foreach ($BoxesToHit as $JustAnotherBox) {
            $this->plugin->helper->SaveCheckBoxToDB($JustAnotherBox);
        }

        // Text Areas
        //
        $BoxesToHit = array(
            SLPLUS_PREFIX.'-enhanced_results_string',
            SLPLUS_PREFIX.'_message_noresultsfound' ,
            );
        foreach ($BoxesToHit as $JustAnotherBox) {
            $this->plugin->helper->SavePostToOptionsTable($JustAnotherBox);
        }

    }


    /**
     * Add settings the the SLP Map Settings Results panel.
     *
     * @param string $HTML - the default (or current) results string HTML
     * @return string - the modified HTML
     */
    function slp_add_results_settings_panel($HTML) {
        if (!$this->setPlugin()) { return ''; }

        $slpDescription = $HTML;
        $slpDescription .= $this->plugin->helper->CreateCheckboxDiv(
            '-enhanced_results_add_tel_to_phone',
            __('Use tel URI','csl-slplus'),
            __('When checked, wraps the phone number in the results in a tel: href tag.', 'csl-slplus')
            );
        $slpDescription .= $this->plugin->helper->CreateCheckboxDiv(
            '-enhanced_results_hide_distance_in_table',
            __('Hide Distance','csl-slplus'),
            __('Do not show the distance to the location in the results table.', 'csl-slplus')
            );
        $slpDescription .= $this->plugin->helper->CreateCheckboxDiv(
            '-enhanced_results_show_country',
            __('Show Country','csl-slplus'),
            __('Display the country in the results table address.', 'csl-slplus')
            );
        $slpDescription .= $this->plugin->helper->CreateCheckboxDiv(
            '-enhanced_results_show_hours',
            __('Show Hours','csl-slplus'),
            __('Display the hours in the results table under the Directions link.', 'csl-slplus')
            );

        $this->plugin->UI->setResultsString();
        $slpDescription .= $this->plugin->AdminUI->MapSettings->CreateTextAreaDiv(
            '-enhanced_results_string',
            __('Results Format','csl-splus'),
            __('The format of the results under the map.','csl-slplus') .
                ' ' .
                sprintf('<a href="%s" target="csa">%s</a>',
                        $this->plugin->support_url,
                        sprintf(__('Uses HTML plus %s shortcodes.','csl-slplus'),$this->name)
                        ) .
                ' ' .
                __('Overrides all other results settings.','csl-slplus')
                ,
            SLPLUS_PREFIX,
            $this->plugin->UI->resultsString
            );

        $slpDescription .=
            '<script language="JavaScript">'.
            'function resetResultString() {'.
            "if (confirm('Reset?')) {".
            "jQuery('textarea[name=\"csl-slplus-enhanced_results_string\"]').val('".$this->plugin->UI->resultsString."');".
            '}'.
            'return false;'.
            '}'.
            '</script>'
            ;

        $slpDescription .=
            '<div class="form_entry">'.
                '<label for="csl-slplus-reset_results_string">&nbsp;</label>'.
                '<span name="csl-slplus-reset_results_string" style="cursor: pointer; text-decoration: underline; " onclick="resetResultString();">Reset Results Format to default.</span>' .
            '</div>'
            ;

        $slpDescription .= $this->settings_orderby();
        return $slpDescription;
    }


    /**
     * Method: settings_orderby
     *
     * Create the order by setting string.
     * section.
     */
    function settings_orderby() {
        if (!$this->setPlugin()) { return ''; }
        $pulldownItems[__("Distance", SLPLUS_PREFIX)]   = "sl_distance ASC";
        $pulldownItems[__("Name", SLPLUS_PREFIX)]       = "sl_store ASC";

        $content =
            "<div class='form_entry'>" .
                "<label for='".$this->plugin->AdminUI->MapSettings->settings->prefix."-enhanced_results_orderby'>".
                __('Order Results By: ', SLPLUS_PREFIX) .
                "</label>";

        $content .= "<select name='".$this->plugin->AdminUI->MapSettings->settings->prefix."-enhanced_results_orderby'>";
        foreach ($pulldownItems as $key=>$value) {
            $selected=($this->plugin->AdminUI->MapSettings->settings->get_item('enhanced_results_orderby','sl_distance ASC')==$value)?" selected " : "";
            $content .= "<option value='$value' $selected>$key</option>\n";
        }
        $content .= "</select>";


        $content .= "</div>";

        return $content;
    }


    /**
     * Method: tweak_mysql_search
     */
    function tweak_mysql_search($query) {
        global $slplus_plugin;
        return preg_replace('/ORDER BY sl_distance ASC/',
                'ORDER BY '. $slplus_plugin->settings->get_item('enhanced_results_orderby','sl_distance ASC'),
                $query);
    }

    /**
     * Change how the information below the map is rendered.
     *
     * SLP Filter: slp_javascript_results_string
     *
     *              {0} aMarker.name,
     *              {1} parseFloat(aMarker.distance).toFixed(1),
     *              {2} slplus.distance_unit,
     *              {3} street,
     *              {4} street2,
     *              {5} city_state_zip,
     *              {6} thePhone,
     *              {7} theFax,
     *              {8} link,
     *              {9} elink,
     *              {10} slplus.map_domain,
     *              {11} encodeURIComponent(this.address),
     *              {12} encodeURIComponent(address),
     *              {13} slplus.label_directions,
     *              {14} tagInfo,
     *              {15} aMarker.id
     *              {16} aMarker.country
     *              {17} aMarker.hours
     *
     * The normal output for SLP3.6.2 is
     *          '<center>' .
     *           '<table width="96%" cellpadding="4px" cellspacing="0" class="searchResultsTable" id="slp_results_table">'  .
     *              '<tr class="slp_results_row" id="slp_location_{15}">'  .
     *                  '<td class="results_row_left_column" id="slp_left_cell_{15}"><span class="location_name">{0}</span><br>{1} {2}</td>'  .
     *                  '<td class="results_row_center_column" id="slp_center_cell_{15}">{3}{4}{5}{16}{6}{7}</td>'  .
     *                  '<td class="results_row_right_column" id="slp_right_cell_{15}">{8}{9}'  .
     *                      '<a href="http://{10}' .
     *                      '/maps?saddr={11}'  .
     *                      '&daddr={12}'  .
     *                      '" target="_blank" class="storelocatorlink">{13}</a>{14}</td>'  .
     *                  '</tr>'  .
     *              '</table>'  .
     *              '</center>';
     *
     */
    function mangle_results_output($resultString) {
        if (!$this->setPlugin()) { return ''; }
        
        // The shortcodes we will care about...
        //
        add_shortcode('slp_location',array($this,'process_LocationShortcode'));

        // Get our saved result string
        // escape that text area value
        // strip the slashes
        // then run through the shortcode processor.
        //
        $resultString = 
                do_shortcode(
                    stripslashes(
                        esc_textarea(
                            $this->plugin->settings->get_item('enhanced_results_string',$resultString)
                        )
                    )
                );

        // Hide Distance
        //
        if (($this->plugin->settings->get_item('enhanced_results_hide_distance_in_table',0) == 1)) {
            $pattern = '/<span class="location_distance"><br\/>{1\} {2\}<\/span>/';
            $resultString = preg_replace($pattern,'',$resultString,1);
        }

        // Show Country
        //
        if (($this->plugin->settings->get_item('enhanced_results_show_country',0) == 0)) {
            $pattern = '/<span class="slp_result_address slp_result_country">{16\}<\/span>/';
            $newPattern = '';
            $resultString = preg_replace($pattern,$newPattern,$resultString,1);
        }

        // Show Hours
        //
        if (($this->plugin->settings->get_item('enhanced_results_show_hours',0) == 1)) {
            $pattern = '/<span class="slp_result_contact slp_result_tags">{14\}<\/span>/';
            $newPattern = '<span class="slp_result_contact slp_result_tags">{14}</span><div class="slp_result_contact slp_hours">{17}</div>';
            $resultString = preg_replace($pattern,$newPattern,$resultString,1);
        }

        // Remove the shortcodes we will care about...
        //
        remove_shortcode('slp_location');

        // Send them back the string
        //
        return $resultString;
    }

    /**
     * Process the [slp_location] shortcode in a results string.
     * 
     * Attributes for [slp_location] include:
     *     <field name> where field name is a locations table field.
     *
     * Usage: [slp_location country]
     * 
     * @param array[] $atts
     */
    function process_LocationShortcode($atts) {
        $theKeys = array_map('strtolower',$atts);
        switch ($theKeys[0]):
            case 'address'      :
            case 'address2'     :
            case 'city'         :
            case 'country'      :
            case 'description'  :
            case 'distance'     :
            case 'email'        :
            case 'fax'          :
            case 'hours'        :
            case 'id'           :
            case 'image'        :
            case 'lat'          :
            case 'lng'          :
            case 'name'         :
            case 'option_value' :
            case 'phone'        :
            case 'sl_pages_url' :
            case 'state'        :
            case 'tags'         :
            case 'url'          :
            case 'zip'          :
                return '{18.'.$theKeys[0].'}';
            case 'distance_1'     :
                return '{1}';
            case 'distance_unit'  :
                return '{2}';
            case 'city_state_zip' :
                return '{5}';
            case 'web_link'       :
                return '{8}';
            case 'email_link'     :
                return '{9}';
            case 'map_domain'     :
                return '{10}';
            case 'search_address' :
                return '{11}';
            case 'location_address':
                return '{12}';
            case 'directions_text':
                return '{13}';
            case 'pro_tags':
                return '{14}';
            case 'id':
                return '{15}';
            default:
                return '';
        endswitch;
        return '';
    }    
}

add_action( 'init', array( 'SLPEnhancedResults', 'init' ) );
