=== Store Locator Plus : Enhanced Results ===
Plugin Name:  Store Locator Plus : Enhanced Results
Contributors: charlestonsw
Donate link: http://www.charlestonsw.com/product/store-locator-plus-enhanced-results/
Tags: search form, google maps
Requires at least: 3.3
Tested up to: 3.5.1
Stable tag: 0.8

A premium add-on pack for Store Locator Plus that adds enhanced search results to the plugin.

== Description ==

Get more control of the search results on Store Locator Plus with this enhanced results add-on.

= Features =

* Change sort order of search results:
** By Distance (closest first)
** By Name (a..z)

= Related Links =

* [Store Locator Plus](http://www.charlestonsw.com/product/store-locator-plus/)
* [Other CSA Plugins](http://profiles.wordpress.org/charlestonsw/)

== Installation ==

= Requirements =

* Store Locator Plus: 3.7.4+
* Wordpress: 3.3.2+
* PHP: 5.1+

= Install After SLP =

1. Go fetch and install Store Locator Plus version 3.7.4 or higher.
2. Purchase this plugin from CSA to get the latest .zip file.
3. Go to plugins/add new.
4. Select upload.
5. Upload the slp-enhanced-results.zip file.

== Frequently Asked Questions ==

= What are the terms of the license? =

The license is GPL.  You get the code, feel free to modify it as you
wish.  We prefer that our customers pay us because they like what we do and
want to support our efforts to bring useful software to market.  Learn more
on our [CSL License Terms](http://www.charlestonsw.com/products/general-eula/).

== Changelog ==

We update about once per month or more frequently as needed.

Visit the [CSA Website for details](http://www.charlestonsw.com/).


= v0.8 =

* Enhancement: better communication with SLP
* Fix: non-object errors.

= v0.7 =

* Enhancement: language support for i18n/l10n.
* Enhancement: Results string can now contain shortcodes.
* Enhancement: New [[slp_location <fieldname>]] shortcode for use in result string only
* Enhancement: Start localization/language support.
* Enhancement: Add ability to attach an image URL to a location.  Can use Results String to output.
* Enhancement: Add ability to customize the "No Results" message under the map, including message with single quotes.
* Fix: tests for SLP is active now works on WPMU.

= 0.6 =

* SLP 3.8.10 compatibility update.

= 0.5 =

* Hook in the CSA updater system.
* Convert text block to quoted string on JavaScript.  Some peope had quotes issues with this for some reason.

= 0.4 =

* Add a new "Result Format" text entry for near-complete customization of the results string.
* Update result formatting to match SLP3.7.6 HTML structure.

= 0.3 =

* Add option to show hours.

= 0.2 =

* Hide country option.

= 0.1 =

* Initial Release.
