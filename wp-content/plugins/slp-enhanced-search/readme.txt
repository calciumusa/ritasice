=== Store Locator Plus : Enhanced Search ===
Plugin Name:  Store Locator Plus : Enhanced Search
Contributors: charlestonsw
Donate link: http://www.charlestonsw.com/product/store-locator-plus-enhanced-search/
Tags: search form, google maps
Requires at least: 3.3
Tested up to: 3.5.1
Stable tag: 0.7

A premium add-on pack for Store Locator Plus that adds advanced search form modifications.

== Description ==

Get more control and customization of the Store Locator Plus search form.

= Features =

* Hide The Search Form
* Radio Button Tag Selector, requires [Pro Pack](http://www.charlestonsw.com/product/store-locator-plus)
* Radio Button Auto-Submit, requires [Pro Pack](http://www.charlestonsw.com/product/store-locator-plus)

= Related Links =

* [Store Locator Plus](http://www.charlestonsw.com/product/store-locator-plus/)
* [Pro Pack](http://www.charlestonsw.com/product/store-locator-plus)
* [Other CSA Plugins](http://profiles.wordpress.org/charlestonsw/)

== Installation ==

= Requirements =

* Store Locator Plus: 3.9.3+
* Wordpress: 3.3.2+
* PHP: 5.2.4+

= Optional =

* [Pro Pack](http://www.charlestonsw.com/product/store-locator-plus)

= Install After SLP =

1. Go fetch and install Store Locator Plus version 3.3 or higher.
2. Purchase this plugin from CSA to get the latest .zip file.
3. Go to plugins/add new.
4. Select upload.
5. Upload the zip file.

== Frequently Asked Questions ==

= What are the terms of the license? =

The license is GPL.  You get the code, feel free to modify it as you
wish.  We prefer that our customers pay us because they like what we do and
want to support our efforts to bring useful software to market.  Learn more
on our [CSL License Terms](http://www.charlestonsw.com/products/general-eula/).

== Changelog ==

We update about once per month or more frequently as needed.

Visit the [CSA Website for details](http://www.charlestonsw.com/).

= v0.7 (April 2013) =

* Update to work with SLP 3.9.6.

= v0.6  =

* Enhancement: Add search by store name.
* Enhancement: better communication with SLP

= v0.5 =

* Enhancement: new option "allow addy in URL", allows ?address=blah to pre-load search.
* Enhancement: new shortcode attribute allow_addy_in_url='true|false' does the same.
* Enhancement: Add languages support for l10n/i18n.
* Change: Update to work with SLP 3.9.3
* Fix: tests for SLP is active now works on WPMU.

= v0.4 =

* SLP 3.8.10 compatibility update.

= v0.3 =

* hide_search_form - Show/hide map shortcode attribute.
* new placeholder settings for address field, an HTML5 "instructions in the box" feature.
* new placeholder settings for search field, an HTML5 "instructions in the box" feature.

= v0.2 =

* Customize "Search By City" text on pulldown.
* Customize "Search By Country" text on pulldown.
* Customize "Search By State" text on pulldown.

= v0.1 =

* Initial Release
