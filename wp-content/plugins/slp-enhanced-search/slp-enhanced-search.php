<?php
/**
 * Plugin Name: Store Locator Plus : Enhanced Search
 * Plugin URI: http://www.charlestonsw.com/product/store-locator-plus-enhanced-search/
 * Description: A premium add-on pack for Store Locator Plus that adds enhanced search features to the plugin.
 * Version: 0.7
 * Author: Charleston Software Associates
 * Author URI: http://charlestonsw.com/
 * Requires at least: 3.3
 * Test up to : 3.5.1
 *
 * Text Domain: csa-slp-es
 * Domain Path: /languages/
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// No SLP? Get out...
//
include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); 
if ( !function_exists('is_plugin_active') ||  !is_plugin_active( 'store-locator-le/store-locator-le.php')) {
    return;
}

/**
 * The Enhanced Search Add-On Pack for Store Locator Plus.
 *
 * @package StoreLocatorPlus\EnhancedSearch
 * @author Lance Cleveland <lance@charlestonsw.com>
 * @copyright 2012-2013 Charleston Software Associates, LLC
 */
class SLPEnhancedSearch {

    //-------------------------------------
    // Properties
    //-------------------------------------

    /**
     * Properties
     */
    private $metadata               = null;
    private $msUI                   = null;
    public  $plugin                 = null;
    private $slug                   = null;
    private $url;

    /**
     * The minimum version of SLP that is required to use this version of Tagalong.
     *
     * Set this whenever we depend on a specific version of SLP.
     *
     * @var string $min_slp_version
     */
    private $min_slp_version        = '3.9.3';

    //------------------------------------------------------
    // METHODS
    //------------------------------------------------------

    /**
     * Invoke the Enhanced Search plugin.
     *
     * @static
     */
    public static function init() {
        static $instance = false;
        if ( !$instance ) {
            load_plugin_textdomain( 'csa-slp-es', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
            $instance = new SLPEnhancedSearch;
        }
        return $instance;
    }


    /**
     * Constructor
     */
    function SLPEnhancedSearch() {
        add_action('slp_init_complete'          ,array($this,'slp_init')                            );
        add_action('slp_admin_menu_starting'    ,array($this,'admin_menu')                          );
    }

    /**
     * After SLP initializes, do this.
     *
     * SLP Action: slp_init_complete
     *
     * @return null
     */
    function slp_init() {
        if ($this->setPlugin()) {

            // Plugin needs to be version 3.9+
            //
            if (version_compare(SLPLUS_VERSION, $this->min_slp_version, '<')) {
                if (is_admin()) {
                    $this->plugin->notifications->add_notice(
                            4,
                            sprintf(
                                __('You have %s version %s. You need version %s or greater for this version of %s.','csa-slp-es'),
                                __('Store Locator Plus','csa-slp-es'),
                                __('Enhanced Search'   ,'csa-slp-es'),
                                $this->plugin->version,
                                $this->min_slp_version
                                )
                            );
                }
                return;
            }

            $this->url = plugins_url('',__FILE__);
            $this->dir = plugin_dir_path(__FILE__);
            $this->slug = plugin_basename(__FILE__);

            // Tell SLP we are here
            //
            $this->plugin->register_addon($this->slug);

            // UI: Augment Search Form, Allow show/hide search form
            //
            add_action('slp_render_search_form'         ,array($this,'slp_render_search_form'           ),  9       );
            add_filter('slp_search_form_html'           ,array($this,'modifySearchForm'                 )           );
            add_filter('slp_search_form_divs'           ,array($this,'filter_SearchForm_AddNameSearch'  ), 50       );
            add_action('slp_render_search_form_tag_list',array($this,'slp_render_search_form_tag_list'  ),  9,  2   );
            add_filter('slp_search_default_address'     ,array($this,'set_SearchAddressFromRequest'     )           );

            // Hooks that allow us to directly output HTML in the pre-existing
            // SLP Admin UI / Map Settings / Search Form settings groups
            //
            add_action('slp_add_search_form_features_setting'   ,array($this,'slp_add_search_form_feature_settings' ),  9   );
            add_action('slp_add_search_form_tag_setting'        ,array($this,'slp_add_search_form_tag_settings'     ),  9   );
            add_action('slp_add_search_form_label_setting'      ,array($this,'slp_add_search_form_label_settings'   ),  9   );

            // Filter allows us to manipulate the Admin UI / Map Settings / Search Form HTML
            //
            add_filter('slp_map_settings_searchform'            ,array($this,'add_placeholders'                     )       );

            // Hook to prep the data to be written to the database
            //
            add_action('slp_save_map_settings'                  ,array($this,'slp_save_map_settings'                ),  9   );

            // Shortcode attributes
            //
            add_filter('slp_shortcode_atts'                     ,array($this,'extend_main_shortcode'                )       );
        }
    }

    /**
     * If we are in admin mode, run our admin updates.
     */
    function admin_init() {
        if (!$this->setPlugin()) { return ''; }

        // WordPress Update Checker - if this plugin is active
        //
        if (is_plugin_active($this->slug)) {
            $this->metadata = get_plugin_data(__FILE__, false, false);
            $this->Updates = new SLPlus_Updates(
                    $this->metadata['Version'],
                    $this->plugin->updater_url,
                    $this->slug
                    );
        }
    }

    /**
     * WordPress admin_menu hook.
     *
     */
    function admin_menu(){
        add_action('admin_init' ,array($this,'admin_init'));
    }

    /**
     * Extends the main SLP shortcode approved attributes list, setting defaults.
     *
     * This will extend the approved shortcode attributes to include the items listed.
     * The array key is the attribute name, the value is the default if the attribute is not set.
     *
     * @param array $valid_atts - current list of approved attributes
     */
    function extend_main_shortcode($valid_atts) {
        if (!$this->setPlugin()) { return array(); }

        return array_merge(
                array(
                    'hide_search_form'     => null,
                    'allow_addy_in_url'    => null,
                    ),
                $valid_atts
            );
    }

    /**
     * Set the plugin property to point to the primary plugin object.
     *
     * Returns false if we can't get to the main plugin object.
     *
     * @global wpCSL_plugin__slplus $slplus_plugin
     * @return type boolean true if plugin property is valid
     */
    function setPlugin() {
        if (!isset($this->plugin) || ($this->plugin == null)) {
            global $slplus_plugin;
            $this->plugin = $slplus_plugin;
        }
        return (isset($this->plugin) && ($this->plugin != null));
    }

    /**
     * Sets the search form address input on the UI to a post/get var.
     *
     * The option "allow address in URL" needs to be on.
     * 
     * @param string $currentVal
     * @return string the default input value
     */
    function set_SearchAddressFromRequest($currentVal) {
        
        // Checks shortcode AND default on admin panel settings
        // this is true if addy is allowed in URL.
        //
        $allowAddy =(
                (strcasecmp($this->plugin->data['allow_addy_in_url'],'true')==0)
                ||
                (($this->plugin->data['allow_addy_in_url'] === null) &&
                 (($this->plugin->settings->get_item('es_allow_addy_in_url',0) == 1))
                )
            );

        // If address is blank and we allow defaults, set it.
        //
        if (($currentVal === '') && ($allowAddy) && (isset($_REQUEST['address']))) { 
            return stripslashes_deep($_REQUEST['address']);
        }

        return $currentVal;
    }

    /**
     *
     * @param type $currentHTML
     */
    function add_placeholders($currentHTML) {
        if (!$this->loadCompoundOptions()) { return $currentHTML; }
        $this->msUI = $this->plugin->AdminUI->MapSettings;

        $newHTML =
            $this->msUI->CreateInputDiv(
                '_slpes[address_placeholder]',
                __('Address', 'csa-slp-es'),
                __('Instructions to place in the address input.','csa-slp-es')
                ) .
            $this->msUI->CreateInputDiv(
                '_slpes[name_placeholder]',
                __('Name', 'csa-slp-es'),
                __('Instructions to place in the name input.','csa-slp-es')
                )
            ;

        return
            $currentHTML .
            $this->msUI->createSettingsGroup(
                'slpes_input_placeholders',
                __('Placeholders','csa-slp-es'),
                __('Placeholders are text instructions that appear inside an input box before data is entered.','csa-slp-es'),
                $newHTML
                );
    }

    /**
     * Add name search to search form.
     *
     * @return string HTML of the augmented form
     */
    function filter_SearchForm_AddNameSearch($HTML) {
        $HTML .= $this->plugin->UI->create_input_div(
                'nameSearch',
                get_option('sl_name_label',__('Name of Store','csa-slplus')),
                '',
                (get_option(SLPLUS_PREFIX.'_show_search_by_name',0) == 0),
                'div_nameSearch'
                );
        return $HTML;
    }

    /**
     * Load the serialized data that we use.
     *
     * @return boolean
     */
    function loadCompoundOptions() {
        if (!$this->setPlugin()) { return false; }
        $this->plugin->Actions->getCompoundOption(SLPLUS_PREFIX.'_slpes[]');
        return true;
    }


    /**
     * Make inline changes to the search form.
     *
     * @param type $currentHTML
     * @return type
     */
    function modifySearchForm($currentHTML) {
        if (!$this->loadCompoundOptions()) { return $currentHTML; }

        // Address Placeholder
        //
        // <input type='text' id='addressInput' placeholder='' size='50' value='' />
        //
        $pattern = "/<input(.*?)id='addressInput'(.*?)placeholder=''(.*?)>/";
        $placeholder = $this->plugin->Actions->getCompoundOption(SLPLUS_PREFIX.'_slpes[address_placeholder]');
        $replacement = '<input${1}id="addressInput"${2}placeholder="'.$placeholder.'"${3}>';
        $currentHTML = preg_replace($pattern,$replacement,$currentHTML);

        // Name Placeholder
        //
        $pattern = "/<input(.*?)id='nameSearch'(.*?)placeholder=''(.*?)>/";
        $placeholder = $this->plugin->Actions->getCompoundOption(SLPLUS_PREFIX.'_slpes[name_placeholder]');
        $replacement = '<input${1}id="nameSearch"${2}placeholder="'.$placeholder.'"${3}>';
        $currentHTML = preg_replace($pattern,$replacement,$currentHTML);

        return $currentHTML;
    }

    /**
     * Manage search form hiding.
     *
     * Shortcode attribute takes precedence, then check the map settings hide search form.
     */
    function slp_render_search_form() {
        if (!$this->setPlugin()) { return; }

        // If the shortcode attribute hide_search_form = true
        // OR
        // hide_search_form attribute is not set (null) AND
        // the map settings hide search form is checked
        //
        // Then hide the search form.
        //
        if (
                (strcasecmp($this->plugin->data['hide_search_form'],'true')==0)
                ||
                (($this->plugin->data['hide_search_form'] === null) &&
                 (($this->plugin->settings->get_item('enhanced_search_hide_search_form',0) == 1))
                )
           ){
            remove_action('slp_render_search_form',array($this->plugin->UI,'create_DefaultSearchForm'));
            return;
        }

        // Placeholders
        //
        if (!$this->loadCompoundOptions()) { return $currentHTML; }


        return;
    }

    /**
     * Method: slp_render_search_form_tag_list
     *
     * Replace the tag pulldown with a radio button group.
     */
    function slp_render_search_form_tag_list($tags,$showany) {
        global $slplus_plugin;
        if (($slplus_plugin->settings->get_item('enhanced_search_show_tag_radio',0) == 1)) {
            $thejQuery = "onClick='" .
                    "jQuery(\"#tag_to_search_for\").val(this.value);".
                    (
                        ($slplus_plugin->settings->get_item('enhanced_search_auto_submit',0) == 1) ?
                            "jQuery(\"#searchForm\").submit();":
                            ''
                    ) .
                    "'"
                    ;
            $oneChecked = false;
            $hiddenValue = '';
            foreach ($tags as $tag) {
                $checked = false;
                $clean_tag = preg_replace("/\((.*?)\)/", '$1',$tag);
                if  (
                        ($clean_tag != $tag) ||
                        (!$oneChecked && !$showany && ($tag == $tags[0]))
                     ){
                    $checked = true;
                    $oneChecked = true;
                    $hiddenValue = $clean_tag;
                }
                print
                    '<span class="slp_checkbox_entry">' .
                    "<input type='radio' name='tag_to_search_for' value='$clean_tag' ".
                        $thejQuery.
                        ($checked?' checked':'').">" .
                        $clean_tag .
                    '</span>'
                    ;
            }
            if ($showany) {
                print
                    '<span class="slp_radio_entry">' .
                    "<input type='radio' name='tag_to_search_for' value='' ".
                        $thejQuery.
                        ($oneChecked?'':'checked').">" .
                        "Any" .
                    '</span>';
            }

            // Hidden field to store selected tag for processing
            //
            print "<input type='hidden' id='tag_to_search_for' name='hidden_tag_to_search_for' value='$hiddenValue'>";

            // JQuery to trigger the hidden field
            //
            print "
                ";


            remove_action('slp_render_search_form_tag_list',array($this->plugin->UI,'slp_render_search_form_tag_list'));
        }
        return;
    }

    /**
     * Add new settings for the search for to the map settings/search form section.
     *
     * @return null
     */
    function slp_add_search_form_feature_settings() {
        if (!$this->setPlugin()) { return; }

        echo '<p class="slp_admin_info"><strong>Enhanced Search</strong></p>';

        echo $this->plugin->helper->CreateCheckboxDiv(
            '_show_search_by_name',
            __('Show search by name', 'csa-slp-es'),
            __('Shows the name search entry box to the user.', 'csa-slp-es')
            );

        echo $this->plugin->helper->CreateCheckboxDiv(
            '-enhanced_search_hide_search_form',
            __('Hide Search Form','csa-slp-es'),
            __('Hide the user input on the search page, regardless of the SLP theme used.', 'csa-slp-es')
            );

        echo $this->plugin->helper->CreateCheckboxDiv(
            '-es_allow_addy_in_url',
            __('Allow Address In URL','csa-slp-es'),
            __('If checked an address can be pre-loaded via a URL string ?address=blah.', 'csa-slp-es')
            );

        return;
    }

    /**
     * Add new settings for the search for to the map settings/search form section.
     *
     */
    function slp_add_search_form_tag_settings() {
        echo $this->plugin->helper->CreateCheckboxDiv(
            '-enhanced_search_show_tag_radio',
            __('Show Radio Buttons','csa-slp-es'),
            __('Show radio buttons instead of the pulldown for the tag selection interface.', 'csa-slp-es')
            );
        echo $this->plugin->helper->CreateCheckboxDiv(
            '-enhanced_search_auto_submit',
            __('Tag Autosubmit','csa-slp-es'),
            __('Force the form to auto-submit when the tag is selected with a radio button.', 'csa-slp-es')
            );
        return;
    }

    /**
     * Add new custom labels.
     *
     */
    function slp_add_search_form_label_settings() {
        if (!$this->setPlugin()) { return ''; }
        
        echo $this->plugin->helper->create_SubheadingLabel(__('Enhanced Search','csa-slp-es'));

        echo $this->plugin->AdminUI->MapSettings->CreateInputDiv(
            'sl_name_label',
            __('Name', 'csa-slp-es'),
            __('Search form name label.','csa-slp-es'),
            '',
            'Name'
            );

        echo $this->plugin->AdminUI->MapSettings->CreateInputDiv(
                '_search_by_city_pd_label',
                __('City First Entry', 'csa-slp-es'),
                __('The first entry on the search by city pulldown. (a Pro Pack Option)','csa-slp-es'),
                SLPLUS_PREFIX,
                __('--Search By City--','csa-slp-es')
                );
        echo $this->plugin->AdminUI->MapSettings->CreateInputDiv(
                '_search_by_state_pd_label',
                __('State First Entry', 'csa-slp-es'),
                __('The first entry on the search by state pulldown. (a Pro Pack Option)','csa-slp-es'),
                SLPLUS_PREFIX,
                __('--Search By State--','csa-slp-es')
                );
        echo $this->plugin->AdminUI->MapSettings->CreateInputDiv(
                '_search_by_country_pd_label',
                __('Country First Entry', 'csa-slp-es'),
                __('The first entry on the search by country pulldown (a Pro Pack Option).','csa-slp-es'),
                SLPLUS_PREFIX,
                __('--Search By Country--','csa-slp-es')
                );        }

    /**
     * Method: slp_save_map_settings
     */
    function slp_save_map_settings() {
        if (!$this->setPlugin()) { return ''; }

        // Checkboxes
        //
        $BoxesToHit = array(
            'enhanced_search_auto_submit',
            'enhanced_search_hide_search_form',
            'es_allow_addy_in_url',
            'enhanced_search_show_tag_radio'
            );
        foreach ($BoxesToHit as $JustAnotherBox) {
            $this->plugin->helper->SaveCheckBoxToDB($JustAnotherBox);
        }

        // Checkboxes with special names
        //
        $BoxesToHit = array(
            SLPLUS_PREFIX.'_show_search_by_name'
            );
        foreach ($BoxesToHit as $JustAnotherBox) {
            $this->plugin->helper->SaveCheckBoxToDB($JustAnotherBox,'','');
        }

        // Text boxes
        //
        $BoxesToHit = array(
            'sl_name_label'                         ,
            SLPLUS_PREFIX.'_search_by_city_pd_label'       ,
            SLPLUS_PREFIX.'_search_by_state_pd_label'      ,
            SLPLUS_PREFIX.'_search_by_country_pd_label'    ,
            );
        foreach ($BoxesToHit as $JustAnotherBox) {
            $this->plugin->helper->SavePostToOptionsTable($JustAnotherBox);
        }

        // Serialized
        //
        $this->plugin->helper->SavePostToOptionsTable(SLPLUS_PREFIX.'_slpes');
    }

    /**
     * Method: wp_enqueue_scripts
     */
    function wp_enqueue_scripts() {
        wp_enqueue_script(
                'slp_enhanced_search_test_script',
                $this->url.'/my.js',
                'csl_script'
        );
    }
}

add_action( 'init', array( 'SLPEnhancedSearch', 'init' ) );
