<?php

/*

Plugin Name: Pinterest Lightbox

Plugin URI: http://www.travishoglund.com

Description: Add Pinterest in a Lightbox to the NextGEN Gallery plugin.

Version: 1.0

Author: Travis Hoglund

Author URI: http://www.travishoglund.com

*/



if (!defined('WP_CONTENT_URL'))

      define('WP_CONTENT_URL', get_option('siteurl').'/wp-content');

if (!defined('WP_CONTENT_DIR'))

      define('WP_CONTENT_DIR', ABSPATH.'wp-content');

if (!defined('WP_PLUGIN_URL'))

      define('WP_PLUGIN_URL', WP_CONTENT_URL.'/plugins');

if (!defined('WP_PLUGIN_DIR'))

      define('WP_PLUGIN_DIR', WP_CONTENT_DIR.'/plugins');





function lightPin() {

?>

<script type="text/javascript">

	jQuery(document).ready(function($){	

	    var select = $('a[href$=".bmp"],a[href$=".gif"],a[href$=".jpg"],a[href$=".jpeg"], a[href$=".png"],a[href$=".BMP"],a[href$=".GIF"],a[href$=".JPG"],a[href$=".JPEG"],a[href$=".PNG"]');

		select.lightPin();

	});

</script>

<?php    

}



if (!is_admin()) :

	wp_enqueue_script('jquery');

	wp_enqueue_script('jquery.lightPin', WP_PLUGIN_URL."/pinterest-lightbox/javascript/jquery.pinterest-lightbox.js", array('jquery'), '2.03');

	wp_enqueue_style('jquery.lightPin', WP_PLUGIN_URL."/pinterest-lightbox/stylesheets/jquery.pinterest-lightbox.css", false, '2.03');

	add_action('wp_head', 'lightPin');

endif;



?>