=== Store Locator Plus : Tagalong ===
Plugin Name:  Store Locator Plus : Tagalong
Contributors: charlestonsw
Donate link: http://www.charlestonsw.com/product/store-locator-plus-tagalong/
Tags: search form, google maps
Requires at least: 3.3
Tested up to: 3.5.1
Stable tag: 0.9

A premium add-on pack for Store Locator Plus that adds location categories as an advanced form of tagging.

== Description ==

Categorize locations, changing map markers and other information on a category basis.
This plugin requires both the free Store Locator Plus base plugin as well as the Store Pages premium add-on.

= Features =


= Related Links =

* [Store Locator Plus](http://www.charlestonsw.com/product/store-locator-plus/)
* [Other CSA Plugins](http://profiles.wordpress.org/charlestonsw/)

== Installation ==

= Requirements =

* Store Locator Plus: 3.11+
* Wordpress: 3.3.2+
* PHP: 5.1+

= Install After SLP =

1. Go fetch and install Store Locator Plus version 3.7.5 or higher.
2. Purchase this plugin from CSA to get the latest .zip file.
3. Go to plugins/add new.
4. Select upload.
5. Upload the slp-tagalong.zip file.

== Frequently Asked Questions ==

= What are the terms of the license? =

The license is GPL.  You get the code, feel free to modify it as you
wish.  We prefer that our customers pay us because they like what we do and
want to support our efforts to bring useful software to market.  Learn more
on our [CSL License Terms](http://www.charlestonsw.com/products/general-eula/).

== Changelog ==

We update about once per month or more frequently as needed.

Visit the [CSA Website for details](http://www.charlestonsw.com/).


= v0.9 =

* Change: Requires SLP 3.11
* Enhancement: better communication with SLP.
* Enhancement: Make the "hide empty categories" functionality more obvious.
* Enhancement: Editing a location shows the location categories with children indented below parents.
* Enhancement: Categories drop down on the front end search form shows children indented below parents.
* Fix: Stop duplicating addresses on an edit of a location.
* Fix: Remember the category we selected when editing a location.
* Fix: Remember the lat/long when editing a location.

= 0.8 =

* Fixed bulk update error "title, content, excerpt empty".

= 0.7 =

* Enhancement: Make add-on SLP 3.9 compatible.
* Fix: Map marker now shows the assigned Tagalong marker (first found) for the location.
* Fix: Use new SLP admin UI create_Navbar() method.
* Fix: tests for SLP is active now works on WPMU sites.
* Fix: tests that Store Pages is active on core features, otherwise some features break
* Fix: warnings with single location update
* Fix: bad CSS formats for admin

= 0.6 =

* Fix: Parameter passing issues on manage locations page.

= 0.5 =

* Fix icon render call.
* Save the marker/icon data when CREATING a new category.

= 0.4 =

* SLP 3.8.10 compatibility update.

= 0.3 =

* Minor tweak to CSA updater.

= 0.2 =

* New Option: use default map markers
* New Search Data Returned: amarker.tagalongmarker, the map marker for the category assigned to the location
* New Search Data Returned: amarker.tagalongmedicon, the medium icon for the category assigned to the location
* Use the new search data with the Enhanced Results custom results string.
* TAGALONG NOW REQUIRES THE STORE PAGES PREMIUM ADD-ON.
** You do not have to use the store pages, but this is where the per-location categories are stored.
* Fix manage locations editor so it remembers what category you had selected.
* Updating the location category data now creates (or updates) a Store Page.
** Default setting for new pages is draft, for existing Store Pages it takes on the prior publication status.
* Fix style on category selector for add locations page.
* Update the category selector layout on manage locations.
* Hook into the Store Locator Plus custom updater.
** Notifies users via the standard WordPress updates system when a new version is available.

= 0.1 =

* Initial Release
* Show/hide the search form.
* Replace tags pulldown with radio button list.
