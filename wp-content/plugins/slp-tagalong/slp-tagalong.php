<?php
/**
 * Plugin Name: Store Locator Plus : Tagalong
 * Plugin URI: http://www.charlestonsw.com/product/store-locator-plus-tagalong/
 * Description: A premium add-on pack for Store Locator Plus that adds advanced location tagging features.
 * Version: 0.9
 * Author: Charleston Software Associates
 * Author URI: http://charlestonsw.com/
 * Requires at least: 3.3
 * Test up to : 3.5.1
 *
 * Text Domain: csa-slp-tagalong
 * Domain Path: /languages/
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// No SLP? Get out...
//
include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); 
if ( !function_exists('is_plugin_active') ||  !is_plugin_active( 'store-locator-le/store-locator-le.php')) {
    return;
}

/**
 * The Tagalong Add-On Pack for Store Locator Plus.
 *
 * @package StoreLocatorPlus\Tagalong
 * @author Lance Cleveland <lance@charlestonsw.com>
 * @copyright 2012-2013 Charleston Software Associates, LLC
 */
class SLPTagalong {

    //-------------------------------------
    // Properties
    //-------------------------------------

    /**
     * Are we in the admin panel?
     *
     * @var boolean $adminMode
     */
    private $adminMode              = false;

    /**
     * The directory we live in.
     *
     * @var string $dir
     */
    private $dir;

    /**
     * Data to help us with Taglong stuff.
     *
     * @var mixed[] $metadata
     */
    private $metadata;

    /**
     * The options that the user has set for Tagalong elements.
     *
     * @var mixed[] $options
     */
    public  $options                = array(
        'default_icons'         => '0',
        'hide_empty'            => '0',
        'label_category'        => 'Category: ',
        'show_option_all'       => 'Any',
        'show_cats_on_search'   => ''
        );

    /**
     * The base class for the SLP plugin
     *
     * @var SLPlus $plugin
     **/
    public  $plugin                 = null;

    /**
     * The Tagalong slug.
     *
     * @var string $slug
     */
    private $slug                   = null;

    /**
     * True of the Store Pages add-on pack is active.
     *
     * @var boolean $StorePagesActive
     */
    public $StorePagesActive       = false;

    /**
     * The minimum version of SLP that is required to use this version of Tagalong.
     *
     * Set this whenever we depend on a specific version of SLP.
     *
     * @var string $min_slp_version
     */
    private $min_slp_version        = '3.11';

    /**
     * The url to this plugin admin features.
     *
     * @var string $url
     */
    private $url;

    //------------------------------------------------------
    // METHODS
    //------------------------------------------------------

    /**
     * Invoke the Tagalong plugin.
     *
     * This ensures a singleton of this plugin.
     *
     * @static
     */
    public static function init() {
        static $instance = false;
        if ( !$instance ) {
            load_plugin_textdomain( 'csa-slp-tagalong', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
            $instance = new SLPTagalong;
        }
        return $instance;
    }

    /**
     * Constructor.
     *
     * Hook to slp_init_complete and slp_admin_menu_starting only.
     * This ensures we don't do anything until the SLP base plugin is running.
     * Since plugins can load in any order (well, alphabetical) we need to ensure
     * we go AFTER SLP.
     *
     * Properties, etc. should all go in the other methods for these hooks.
     *
     * slp_init_complete will run for ANY page, user or admin panel side.
     * slp_admin_menu_starting will run ONLY for admin pages
     *
     * SLP Hook: slp_init_complete
     * SLP Hook: slp_admin_menu_starting
     */
    function SLPTagalong() {
        add_action('slp_init_complete'          ,array($this,'slp_init')                            );
        add_action('slp_admin_menu_starting'    ,array($this,'admin_menu')                          );
    }

    /**
     * After SLP initializes, do this.
     *
     * Runs on any page type where SLP is active (admin panel or UI).
     *
     * SLP Action: slp_init_complete
     *
     * @return null
     */
    function slp_init() {
        if (!$this->setPlugin()) { return; }

        // Check main plugin version is good enough.
        //
        if (version_compare(SLPLUS_VERSION, $this->min_slp_version, '<')) {
            if (is_admin()) {
                $this->plugin->notifications->add_notice(
                        4,
                        sprintf(
                            __('You have %s version %s. You need version %s or greater for this version of %s.','csa-slp-es'),
                            __('Store Locator Plus','csa-slp-tagalong'),
                            $this->plugin->version,
                            $this->min_slp_version,
                            __('Tagalong'   ,'csa-slp-tagalong')
                            )
                        );
            }
            return;
        }

        // Set Properties
        //
        $this->plugin_path  = dirname( __FILE__ );
        $this->plugin_url   = plugins_url('',__FILE__);
        $this->url          = plugins_url('',__FILE__);
        $this->dir          = plugin_dir_path(__FILE__);
        $this->slug         = plugin_basename(__FILE__);
        $this->StorePagesActive = ( function_exists('is_plugin_active') &&  is_plugin_active( 'slp-pages/slp-pages.php'));
        
        // Tell SLP we are here
        //
        $this->plugin->register_addon($this->slug);

        // General Init
        //
        $this->initOptions();

        // Admin / Nav Menus (start of admin stack)
        //
        add_filter('slp_action_boxes'           ,array($this,'manage_locations_actionbar'   )       );

        // Manage Locations : categorize locations in bulk
        //
        add_action('slp_manage_locations_action',array($this,'categorize_locations'         )       );

        // AJAX and other ubiquitous stuff
        // Save category data for stores taxonomy type
        //
        add_action('create_stores'                  ,array($this,'create_or_edited_stores'      ),10 ,2 );
        add_filter('slp_location_filters_for_AJAX'  ,array($this,'filter_JSONP_SearchByCategory')       );

        // Search/Map Results
        add_filter('slp_results_marker_data'    ,array($this, 'filter_SetMapMarkers'        )       );
    }

    /**
     * Initialize the options properties from the WordPress database.
     */
    function initOptions() {
        $dbOptions = get_option(SLPLUS_PREFIX.'-TAGALONG-options');
        if (is_array($dbOptions)) {
            $this->options = array_merge($this->options,$dbOptions);

            // Search Form
            // Since this defaults to off we only need to test this
            // if the database had Tagalong options in it.
            //
            if ($this->options['show_cats_on_search']==='on'){
                add_filter('slp_search_form_divs',
                        array($this,'filter_AddCategorySelectorToSearchForm'),
                        35
                        );
            }

            // Do allow hide empty if Store Pages is not active.
            //
            if (!$this->StorePagesActive) {
                $this->options['hide_empty'] = '0';
            }
        }
    }

    //====================================================
    // WordPress Admin Actions
    //====================================================

    /**
     * Get our hooks & filters tied in to SLP.
     *
     * WordPress update checker and all admin-only things go in here.
     *
     * SLP Action: slp_admin_menu_starting
     *
     * @return null
     */
    function admin_init(){

        // Register the admin stylesheet
        //
        wp_register_style(
            'slp_tagalong_style',
            $this->url . '/admin.css'
            );

        // The Categories Interface (aka Taxonomy)
        // where we attach the icon/marker to a category
        //
        add_action('stores_add_form_fields'         ,array($this,'stores_add_form_fields'                           )           );
        add_action('stores_edit_form'               ,array($this,'stores_edit_form'                                 )           );
        add_action('edited_stores'                  ,array($this,'create_or_edited_stores'                          ),10    ,2  );

        // Manage Locations Interface
        //
        add_filter('slp_manage_location_columns'    ,array($this,'filter_AddCategoriesHeaderToManageLocations'      )           );
        add_filter('slp_column_data'                ,array($this,'filter_RenderCategoriesInManageLocationsTable'    ),90    ,3  );
        add_filter('slp_edit_location_data'         ,array($this,'set_LocationProperties'                           )           );
        add_filter('slp_edit_location_right_column' ,array($this,'filter_AddCategoriesToEditForm'                   ),20        );
        add_filter('slp_update_location_data'       ,array($this,'data_UpdateLocationCategoryandPageID'             ),90    ,2  );

        // WordPress Update Checker - if this plugin is active
        //
        if (is_plugin_active($this->slug)) {
            $this->metadata = get_plugin_data(__FILE__, false, false);
            if (!$this->setPlugin()) { return; }                
            $this->Updates = new SLPlus_Updates(
                    $this->metadata['Version'],
                    $this->plugin->updater_url,
                    $this->slug
                    );
        }
    }

    /**
     * Run when on admin page.
     *
     * Should only load CSS and JS and setup the admin_init hook.
     *
     * This ensures SLP is prepped for admin but waits until WOrddPress
     * is further along before firing off related admin functions.
     *
     * SLP Action: slp_init_complete sets up...
     *     WP Action: admin_menu
     */
    function admin_menu(){
        $this->adminMode = true;
        $slugPrefix = 'store-locator-plus_page_';

        // Admin Styles
        //
        add_action(
                'admin_print_styles-' . 'store-locator-plus_page_slp_tagalong',
                array($this,'enqueue_tagalong_admin_stylesheet')
                );
        add_action(
                'admin_print_styles-' . $slugPrefix . 'slp_manage_locations',
                array($this,'enqueue_tagalong_admin_stylesheet')
                );
        add_action(
               'admin_print_styles-' . $slugPrefix . 'slp_add_locations',
                array($this,'enqueue_tagalong_admin_stylesheet')
                );

        if (isset($_REQUEST['taxonomy']) && ($_REQUEST['taxonomy']==='stores')) {
             add_action(
                     'admin_print_styles-' . 'edit-tags.php',
                     array($this,'enqueue_tagalong_admin_stylesheet')
                     );
        }

        // Admin Actions
        //
        add_action('admin_init'             ,array($this,'admin_init'       )       );
        add_filter('slp_menu_items'         ,array($this,'add_menu_items'               ),90    );
    }


    /**
     * Add our tag link to the main SLP navbar.
     *
     * SLP Action: slp_init_complete sets up...
     *     WP Action: admin_menu sets up...
     *         SLP Filter: slp_menu_items
     *
     *
     * @param array $menuItems
     * @return string
     */
    function add_menu_items($menuItems) {
        if (!$this->setPlugin()) { return $menuItems; }
        return array_merge(
                    $menuItems,
                    array(
                        array(
                        'label' => __('Tagalong','csa-slp-tagalong'),
                        'slug'              => 'slp_tagalong',
                        'class'             => $this,
                        'function'          => 'renderPage_TagList'
                        )
                    )
                );
    }

    /**
     * Enqueue the tagalong style sheet when needed.
     */
    function enqueue_tagalong_admin_stylesheet() {
        wp_enqueue_style('slp_tagalong_style');
        wp_enqueue_style($this->plugin->AdminUI->styleHandle);
    }

    //====================================================
    // Helpers
    //====================================================

    /**
     * Set the plugin property to point to the primary plugin object.
     *
     * Returns false if we can't get to the main plugin object.
     *
     * @global wpCSL_plugin__slplus $slplus_plugin
     * @return boolean true if plugin property is valid
     */
    function setPlugin() {
        if (!isset($this->plugin) || ($this->plugin == null)) {
            global $slplus_plugin;
            $this->plugin = $slplus_plugin;
        }
        return (isset($this->plugin) && ($this->plugin != null));
    }

    //====================================================
    // Helpers - UI
    //====================================================

    /**
     * Add our custom category selection div to the search form.
     *
     * SLP Filter: slp_search_form_divs
     *
     * @param string $html the HTML for all the divs up to this point
     * @return string the HTML for this div appended to the other HTML
     */
    function filter_AddCategorySelectorToSearchForm($html) {
$this->plugin->debugMP('msg',($this->options['hide_empty'] == 'on')?'Hide On':'Hide Off');

        return $html .
            '<div id="tagalong_category_selector" class="search_item">' .
                '<label for="cat">'.
                    $this->options['label_category']    .
                '</label>'.
                wp_dropdown_categories(
                        array(
                            'echo'              => 0,
                            'hierarchical'      => 1,
                            'depth'             => 99,
                            'hide_empty'        => (($this->options['hide_empty'] == 'on')?1:0),
                            'show_option_all'   => $this->options['show_option_all'],
                            'taxonomy'          => 'stores'
                        )
                        ).
            '</div>'
            ;
    }

    /**
     * Help deserialize data to array.
     *
     * Useful for sl_option_value  field processing.
     *
     * @param type $value
     * @return type
     */
    function deserialize_to_array($value) {
        $arrayData = maybe_unserialize($value);
        if (!is_array($arrayData)) {
            if ($arrayData == '') {
                $arrayData = array();
            } else {
                $arrayData = array('value' => $arrayData);
            }
        }
        return $arrayData;
    }

    /**
     * Set custom map marker for this location.
     *
     * Add the icon marker and filters out any results that don't match the selected category.
     *
     * SLP Filter: slp_results_marker_data
     *
     * @param type $mapData
     */
    function filter_SetMapMarkers($mapData) {
            $optArray = $this->deserialize_to_array(html_entity_decode($mapData['option_value']));

            $assignedCats = (isset($optArray['store_categories']['stores']) ? $optArray['store_categories']['stores'] : array());

            // If this location has a category assigned...
            //
            if (isset($assignedCats[0])) {

                // Set the map marker
                //
                $tagalongData = get_option(SLPLUS_PREFIX.'-TAGALONG-category_'.$assignedCats[0]);
                $marker = 
                    (
                        ($this->options['default_icons'] != 'on') &&
                        (isset($tagalongData['map-marker']) && ($tagalongData['map-marker']!=''))
                    ) ?
                    $tagalongData['map-marker'] :
                    '';

                $moreinfo = print_r($assignedCats,true) . print_r($_POST,true);
                $filterOut = 
                        isset($_POST['formflds']) && 
                        isset($_POST['formflds']['cat']) &&
                        ($_POST['formflds']['cat'] > 0) &&
                        ($assignedCats[0] != $_POST['formflds']['cat'])
                        ;

            } else {
                $marker = '';
                $moreinfo = '';
                $filterOut = isset($_POST['formflds']) && isset($_POST['formflds']['cat']) && ($_POST['formflds']['cat'] > 0);
                $tagalongData = array(
                    'map-marker' => '',
                    'medium-icon' => ''
                    );
            }


            // If the category is not filtered out, merge our new data
            // otherwise return an empty array
            //
            $mapData = (
                ($filterOut===true)  ?
                    array()     :
                    array_merge($mapData,
                                array(
                                    'icon'              => $tagalongData['map-marker'],
                                    'iconImage'         => $tagalongData['map-marker'],
                                    'info'              => $moreinfo,
                                    'tagalongmarker'    => $tagalongData['map-marker'],
                                    'tagalongmedicon'   => $tagalongData['medium-icon'],
                                )
                    )
                );

            return $mapData;
    }

    //====================================================
    // Helpers - Admin UI
    //====================================================

    /**
     * Perform the manage locations action for bulk categorization.
     */
    function categorize_locations() {
        if (isset($_REQUEST['sl_id']) &&
            isset($_REQUEST['tax_input']['stores'])
            ) {
            $locationArray =
                is_array($_REQUEST['sl_id'])    ?
                    $_REQUEST['sl_id']          :
                    array($_REQUEST['sl_id'])
                ;
            foreach ($locationArray as $location) {
                $this->plugin->currentLocation->set_PropertiesViaDB($location);
                $this->data_SaveLocationCategories($location, $_REQUEST['tax_input']['stores']);
            }
        }
    }

     /**
      * Add Tagalong action buttons to the action bar
      *
      * @param array $actionBoxes - the existing action boxes, 'A'.. each named array element is an array of HTML strings
      * @return string
      */
     function manage_locations_actionbar($actionBoxes) {
            if (!$this->setPlugin()) { return $actionBoxes; }

            $actionBoxes['B.tagalong'][] = 
                    '<script type="text/javascript">' .
                        "jQuery(document).ready(function(){" .
                            "jQuery('#slp_tagalong_fields input:checkbox').change(function(){" .
                                'theCats="";' .
                                'jQuery("#slp_tagalong_fields li").' .
                                    'find("input[type=checkbox]:checked" ).' .
                                        'each(function(idx) {' .
                                            'theCats += jQuery(this).parent().text() + ", ";' .
                                         '})' .
                                ';' .
                                'jQuery("#slp_tagalong_current_category").text(theCats);' .
                            "});" .
                        "});" .
                     '</script>'
                    ;

            $actionBoxes['B.tagalong'][] =
                    '<p class="centerbutton">' .
                        "<a class='like-a-button' href='#' "            .
                                "onclick=\"doAction('categorize','"     .
                                    __('Categorize?','slplus-tagalong')   .
                                    "')\" name='categorize_selected'>"  .
                                    __('Categorize', 'slplus-tagalong')   .
                         '</a>'                                         .
                    '</p>'
            ;
            $actionBoxes['B.tagalong'][] =
                '<div id="slp_tagalong_category_picker">' .
                    '<span id="slp_tagalong_current_category">'.__('None','slp-tagalong').'</span>' .
                    '<span id="slp_tagalong_cat_expander" class="like-a-button" ' .
                        'onClick="jQuery(\'#slp_tagalong_selector\').toggle();">+</span>' .
                    '<div id="slp_tagalong_selector" style="display:none;">' .
                        '<div id="slp_tagalong_selector_closer" ' .
                            'onClick="jQuery(\'#slp_tagalong_selector\').toggle();">x</div>' .
                        '<div id="slp_tagalong_fields" class="slp_editform_section">'.
                            '<ul>'.
                                $this->get_CheckList(0) .
                            '</ul>'.
                        '</div>'.
                    '</div>' .
                '</div>'
                ;
            return $actionBoxes;
     }

    /**
     * Return the div string to render an image.
     *
     * @param string $img - fully qualified image url
     * @return string - the div text string with the image in it
     */
    function ShowImage($img = null) {
        if ($img === null) { return; }
        if ($img === '')   { return; }
        return '<div class="slp_tagalog_category_image">' .
                   '<img src="'.$img.'"/>' .
              '</div>'
              ;
    }

    /**
     * Show the image loader interface.
     */
    function ShowImageLoader($postid = null) {
        if ($postid === null) { return; }
        return '<a class="thickbox add_media" '.
                    'onclick="return false;" ' .
                    'title="'.__('Add Media').'" ' .
                    'href="' .admin_url() .'media-upload.php?post_id='.$postid.'&TB_iframe=1&width=540&height=560" '.
                    '>'.
                    __('Upload/Insert') . ' '.
                    '<img width="15" height="15" src="'.admin_url().'images/media-button.png">'.
                '</a>'
            ;

    }

    //----------------------------------
    // Data I/O Methods
    //----------------------------------

    /**
     * Write the Tagalong category data to the database.
     *
     * Only used in bulk locations update.
     *
     * @param int $locationID
     * @param array $categories array of ints representing the tagalong categories
     */
    function data_SaveLocationCategories($locationID,$categories) {

        // Get the SQL string for updating the database.
        //
        $field_value_str = substr($this->data_UpdateLocationCategoryandPageID('',$locationID),2);

        // Update the database record for this location.
        //
        if ($field_value_str != '') {
            $this->plugin->db->query("UPDATE".$this->plugin->database['table']."SET $field_value_str WHERE sl_id=$locationID");
        }
    }

    /**
     * Attach our category data to the update string.
     *
     * Put it in the sl_option_value field as a seralized string.
     *
     * SLP Filter: slp_update_location_data
     *
     * @param string $sqlSetStr the original SQL set syntax
     * @return string the modified SQL set syntax
     */
    function data_UpdateLocationCategoryandPageID($sqlSetStr,$locationID) {

        // Set our filter to get the page data updated
        //
        add_filter('slp_location_page_attributes',array($this,'filter_SetTaxonomyForLocation'));

        // Create a related page if it does not exist.
        //
        $this->CreateRelatedPage($locationID);

        // Setup our option data for this location
        //
        if (isset($_POST['option_value-'.$locationID])) {
            $this->plugin->debugMP('msg','Tagalong.UpdateLocationCategoryandPageID get data from POST');
            $originalOptionArray = $this->deserialize_to_array($_POST['option_value-'.$locationID]);

        // Get the options from the currentLocation as loaded from memory.
        //
        } else {
            $this->plugin->debugMP('msg','Tagalong.UpdateLocationCategoryandPageID get data from currentLocation');
            $originalOptionArray = $this->plugin->currentLocation->attributes;
        }

        // Build an options array that has...
        // All original options +
        // The new store categories from POST['tax_input'] +
        // A linked post ID for the current SLP Page for the location
        //
        $newOptionArray =
            array_merge($originalOptionArray,
                        array(
                            'store_categories'  => isset($_POST['tax_input'])?$_POST['tax_input']:array(),
                        )
            );

        // Replace any prior sl_option_value field
        //
        $sqlSetStr = preg_replace("/,?\s?sl_option_value='(.*?)'/",'',$sqlSetStr);
        $sqlSetStr .= ", sl_option_value='".maybe_serialize($newOptionArray)."'" ;

        // Don't set the linked post ID.
        // CreateRelatedPage does this through the SLPlus_Location interface via
        // the crupdate_Page() method which happens to also calls MakePersistent()
        // if a new page was constructed.
        //
        $sqlSetStr = preg_replace("/,?\s?sl_linked_postid='(.*?)',?/",'',$sqlSetStr);

       $this->plugin->debugMP('msg','Tagalong.UpdateLocationCategoryandPageID SQL:<br/>'.$sqlSetStr);
       return $sqlSetStr;
    }

    //----------------------------------
    // Filters
    //----------------------------------

    /**
     * Add the category editor/selector on the manage/edit location form.
     *
     * SLP Filter: slp_edit_location_right_column
     *
     * @param string $theForm the original HTML form for the manage locations edit (right side)
     * @return string the modified HTML form
     */
    function filter_AddCategoriesToEditForm($theForm) {
        $this->plugin->currentLocation->debugProperties();
        $theForm .= '<div id="slp_tagalong_fields" class="slp_editform_section"><strong>Tagalong</strong><ul>'.
                $this->get_CheckList($this->plugin->currentLocation->linked_postid) .
                '</ul></div>';
        return $theForm;
    }
    /**
     * Add the categories column header to the manage locations table.
     *
     * SLP Filter: slp_manage_location_columns
     *
     * @param mixed[] $currentCols column name + column label for existing items
     * @return mixed[] column name + column labels, extended with our categories data
     */
    function filter_AddCategoriesHeaderToManageLocations($currentCols) {
        return array_merge($currentCols,
                array(
                    'sl_option_value'       => __('Categories'        ,'csa-slp-tagalong')
                )
            );

    }

    /**
     * Add the categry condition to the MySQL statement used to fetch locations with JSONP.
     *
     * @param type $currentFilters
     * @return type
     */
    function filter_JSONP_SearchByCategory($currentFilters) {

        // Category selectors are in the formdata variable, make sure we have one.
        //
        if (!isset($_POST['formdata']) || ($_POST['formdata'] == '')){
            return $currentFilters;
        }

        // Set our JSON Post vars
        //
        $JSONPost = wp_parse_args($_POST['formdata'],array());

        // Don't have cat in the vars?  Don't add a new selection filter.
        //
        if (!isset($JSONPost['cat']) || ($JSONPost['cat'] <= 0)) {
            return $currentFilters;
        }

        // The serialization here mimics that of the stored category as serialized data
        // The "22" near the end is the JSONPost['cat'] value that came in from the JSONP
        // post.
        //
        // a:1:{s:16:"store_categories";a:1:{s:6:"stores";a:1:{i:0;s:2:"22";}}}
        //
        $catAsSerialRegex = 
            'a:1:{' .
                's:16:"store_categories";'.
                'a:1:{'.
                    's:6:"stores";a:[0-9]+:{.*i:[0-9]+;s:[0-9]+:"'.$JSONPost['cat'].'";.*}'.
                '}'.
            '}'
            ;

        return array_merge(
                $currentFilters,
                array(" AND ( sl_option_value REGEXP '". $catAsSerialRegex ."') ")
                );
    }

    /**
     * Render the categories column in the manage locations table.
     *
     * SLP Filter: slp_column_data
     *
     * @param string $theData  - the option_value field data from the database
     * @param string $theField - the name of the field from the database (should be sl_option_value)
     * @param string $theLabel - the column label for this column (should be 'Categories')
     * @return type
     */
    function filter_RenderCategoriesInManageLocationsTable($theData,$theField,$theLabel) {
        if (
            ($theField === 'sl_option_value') &&
            ($theLabel === __('Categories'        ,'csa-slp-tagalong'))
           ) {

            // Category selected
            //
            if ($theData != '') {
                $OptionArray = $this->deserialize_to_array($theData);
                if (isset($OptionArray['store_categories']['stores'])){
                    $theData = '';
                    foreach ($OptionArray['store_categories']['stores'] as $optVal) {
                        $theCat = get_term($optVal,'stores');
                        $tagalongData = get_option(SLPLUS_PREFIX.'-TAGALONG-category_'.$theCat->term_id);
                        $theData .= '<div class="tagalong_entry">';
                        if (isset($tagalongData['map-marker']) && ($tagalongData['map-marker']!='')) {
                            $theData .=
                                '<img src="'.$tagalongData['map-marker'].'" '.
                                    'id="slp_map_marker_'.$theCat->slug.'" '.
                                    'class="slp_map_marker" '.
                                    'alt="'.$theCat->slug.'" '.
                                    'title="'.$theCat->slug.'"/>'
                                ;
                        }
                        $theData .= $theCat->name.'</div>';
                    }
                } else {
                    $theData = '';
                }
            }
        }
        return $theData;
    }

    /**
     * Set the Tagalong categories for the new store page.
     *
     * SLP Filter: slp_location_page_attributes
     *
     * @param mixed[] $pageAttributes - the wp_insert_post page attributes
     * @return mixed[] - pageAttributes with tax_input set
     */
    function filter_SetTaxonomyForLocation($pageAttributes) {
        return array_merge(
                $pageAttributes,
                array(
                        'tax_input' => (isset($_POST['tax_input'])?$_POST['tax_input']:array('stores'=>array()))
                    )
                );
    }

    /**
     * Creates a Store Page for this location as needed.
     * 
     * @param int $locationID
     * @return string
     */
    function CreateRelatedPage($locationID) {
        if (!$this->setPlugin()) { return ''; }

        // Then go and create or update the related page
        //
        $this->plugin->currentLocation->debugProperties();
        $this->plugin->currentLocation->crupdate_Page();

        // Could not create page
        //
        if ($this->plugin->currentLocation->linked_postid <= 0) {
            $this->plugin->notifications->add_notice(
                    'warning',
                    __('Could not store tagalong data for this location.','csa-slp-tagalong')
                    );
            $this->plugin->notifications->display();
        }
    }

    /**
     * Generate the categories check list.
     * 
     * @param int $postID the ID for the store page related to the location.
     * @return string HTML of the checklist.
     */
    function get_CheckList($postID) {
        ob_start();
        wp_terms_checklist(
                    $postID,
                    array(
                        'checked_ontop' => false,
                        'taxonomy' => 'stores',
                    )
                );
        return ob_get_clean();
     }

    /**
     * Grab a copy of the incoming location data.
     *
     * @param mixed[] $locationData - the location data in a named array
     * @return mixed[] modified location data
     */
    function set_LocationProperties($locationData) {
        $this->plugin->debugMP('pr','Tagalong.set_LocationProperties location data',$locationData);
        $this->plugin->currentLocation->set_PropertiesViaArray($locationData);
        return $locationData;
    }

    /**
     * Render the extra tagalong category fields for the add form.
     */
    function stores_add_form_fields() {
        $this->render_ExtraCategoryFields();
    }

    /**
     * Render the extra tagalong category fields for the edit form.
     */
    function stores_edit_form($tag) {
        print '<div id="tagalong_editform" class="form-wrap">';
        $this->render_ExtraCategoryFields($tag->term_id,get_option(SLPLUS_PREFIX.'-TAGALONG-category_'.$tag->term_id));
        print '</div>';
    }


    /**
     * Render the extended Tagalong fields on the WordPress add/edit category forms for store_page categories.
     *
     * @param int $term_id the category id.
     * @param mixed[] $TagalongData tagalong data for this category.
     */
    function render_ExtraCategoryFields($term_id = null ,$TagalongData=null) {
        print '<h3>Tagalong' . __(' Extended Data','csa-slp-tagalong'). '</h3>';

        // Medium Icon
        //
        print
              '<div class="form-field">' .
                   '<label for="medium-icon">Medium Icon</label>' .
                   '<input type="text" id="medium-icon" name="medium-icon" value="'.$TagalongData['medium-icon'].'">'.
                   '<p>'.__('The URL from whence the medium icon will be served.','csa-slp-tagalong').'</p>' .
                   '<p><img id="medium-icon-image" align="top" src=""></p>'.
                   $this->ShowImage($TagalongData['medium-icon']).
              '</div>'
              ;
        print $this->plugin->AdminUI->CreateIconSelector('medium-icon','medium-icon-image');

        // Map Marker
        //
        print
              '<div class="form-field">' .
                   '<label for="map-marker">Map Marker</label>' .
                   '<input type="text" id="map-marker" name="map-marker" value="'.$TagalongData['map-marker'].'">'.
                   '<p>'.__('The URL for the map marker.','csa-slp-tagalong').'</p>' .
                   '<p><img id="map-marker-image" align="top" src=""></p>'.
                   $this->ShowImage($TagalongData['map-marker']).
              '</div>'
              ;
        print $this->plugin->AdminUI->CreateIconSelector('map-marker','map-marker-image');
    }

    /**
     * Called after a store category is inserted or updated in the database.
     *
     * Creates an entry in the wp_options table with an option name
     * based on the category ID and a tagalong prefix like this:
     *
     * csl-slplus-TAGALONG-category_14
     *
     * The data is serialized.  WordPress update_option() and get_option
     * will take care of serializing and deserializing our data.
     *
     * @param int $term_id - the newly inserted category ID
     */
    function create_or_edited_stores($term_id,$ttid) {
        $TagalongData = array(
           'medium-icon' => $_POST['medium-icon'],
           'map-marker'  => $_POST['map-marker']
        );
        update_option(SLPLUS_PREFIX.'-TAGALONG-category_'.$term_id, $TagalongData);
    }



    //====================================================
    // Helpers - Admin UI (tagalong settings page)
    //====================================================


    /**
     * Render the taglist admin interface.
     */
    function renderPage_TagList() {
        if (!$this->setPlugin()) { return ''; }

        $this->Settings = new wpCSL_settings__slplus(
            array(
                    'no_license'        => true,
                    'prefix'            => $this->plugin->prefix,
                    'css_prefix'        => $this->plugin->prefix,
                    'url'               => $this->plugin->url,
                    'name'              => $this->plugin->name . ' - Tagalong',
                    'plugin_url'        => $this->plugin->plugin_url,
                    'render_csl_blocks' => true,
                    'form_action'       => admin_url().'admin.php?page=slp_tagalong'
                )
         );


        // If we are updating settings...
        //
        if (isset($_REQUEST['action']) && ($_REQUEST['action']==='update')) {
            $this->updateTagalongSettings();
        }


        require_once($this->dir.'templates/adminPanel.php');
        $this->AdminUI = new SLPTagalong_AdminPanel($this);
        $this->AdminUI->renderPage();
    }

    /**
     * Update the Tagalong settings.
     */
    function updateTagalongSettings() {
        if (!isset($_REQUEST['page']) || ($_REQUEST['page']!='slp_tagalong')) { return; }
        if (!isset($_REQUEST['_wpnonce'])) { return; }

        $BoxesToHit = array(
            'default_icons',
            'hide_empty',
            'label_category',
            'show_cats_on_search',
            );
        foreach ($BoxesToHit as $BoxName) {
            if (!isset($_REQUEST[SLPLUS_PREFIX.'-TAGALONG-'.$BoxName])) {
                $_REQUEST[SLPLUS_PREFIX.'-TAGALONG-'.$BoxName] = '';
            }
        }

        array_walk($_REQUEST,array($this,'isTagalongOption'));
        update_option(SLPLUS_PREFIX.'-TAGALONG-options', $this->options);

        $this->plugin->notifications->add_notice(
                9,
                __('Tagalong settings saved.','csa-slp-tagalong')
                );
    }

    /**
     * Set the tagalong options from the incoming REQUEST
     *
     * @param mixed $val - the value of a form var
     * @param string $key - the key for that form var
     */
    function isTagalongOption($val,$key) {
        $simpleKey = preg_replace('/^'.SLPLUS_PREFIX.'-TAGALONG-/','',$key);
        if ($simpleKey !== $key){
            $this->options[$simpleKey] = $val;
        }
     }

    /**
     * Render the list of store categories.
     *
     * The direct edit URL: http://wpdev.cybersprocket.com/wp/wp-admin/edit-tags.php?action=edit&taxonomy=stores&tag_ID=4&post_type=store_page
     *
     *  @return HTML
     */
    function renderStoreCategoryList() {
        return
            '<ul id="tagalong_category_list">' .
            wp_list_categories(
                    array(
                        'echo'          => 0,
                        'hide_empty'    => 0,
                        'show_count'    => 0,
                        'taxonomy'      => 'stores',
                        'title_li'      => '',
                    )
                ) .
            '</ul>'
            ;
    }
}


add_action( 'init', array( 'SLPTagalong', 'init' ) );
