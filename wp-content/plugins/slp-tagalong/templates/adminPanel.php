<?php
if (! class_exists('SLPTagalong_AdminPanel')) {

    /**
     * Manage admin panel interface elements for Tagalong.
     *
     * @package StoreLocatorPlus\Tagalong\AdminPanel
     * @author Lance Cleveland <lance@charlestonsw.com>
     * @copyright 2012-2013 Charleston Software Associates, LLC
     */
    class SLPTagalong_AdminPanel {

        /**
         * The Tagalong add-on pack.
         * 
         * @var SLPTagalong $parent 
         */
        public $parent = null;

        /**
         * Instantiate the admin panel object.
         * 
         * @param SLPTagalong $parent
         */
        function __construct($parent) {
            $this->parent = $parent;
        }

        /**
         * Render the admin panel.
         */
        function renderPage() {
            // Display any notices
            $this->parent->plugin->notifications->display();

            //-------------------------
            // Navbar Section
            //-------------------------
            $this->parent->Settings->add_section(
                array(
                    'name'          => 'Navigation',
                    'div_id'        => 'slplus_navbar',
                    'description'   => $this->parent->plugin->AdminUI->create_Navbar(),
                    'is_topmenu'    => true,
                    'auto'          => false,
                    'headerbar'     => false
                )
            );

            //-------------------------
            // Main Section
            //-------------------------
            $slpSectionDescription = 'Tagalong basic settings';

            $this->parent->Settings->add_section(
                array(
                        'name'          => __('General Info','slplus-tagalong'),
                        'description'   => $slpSectionDescription,
                        'auto'          => true
                    )
             );
            $this->parent->Settings->add_checkbox(
                    __('General Info','slplus-tagalong'),
                    __('Use Default Icons','slplus-tagalong'),
                    'TAGALONG-default_icons',
                    __('Do not use custom tagalong destination markers on map, use default Map Settings icons.','slplus-tagalong'),
                    $this->parent->options['default_icons']
                    );

            $this->parent->Settings->add_checkbox(
                    __('General Info','slplus-tagalong'),
                    __('Show Categories On Search','slplus-tagalong'),
                    'TAGALONG-show_cats_on_search',
                    __('Check this to show the category selector is shown on the search form.','slplus-tagalong'),
                    $this->parent->options['show_cats_on_search']
                    );
            $this->parent->Settings->add_checkbox(
                    __('General Info','slplus-tagalong'),
                    __('Hide Unpublished Page Categories','slplus-tagalong'),
                    'TAGALONG-hide_empty',
                    __('Only works if Store Pages is installed.  Hide the empty categories from the category selector. Store Pages in draft mode are considered "empty".','slplus-tagalong'),
                    ($this->parent->options['hide_empty'] == 'on'),
                    !$this->parent->StorePagesActive
                    );
            $this->parent->Settings->add_input(
                    __('General Info','slplus-tagalong'),
                    __('Any Category Label','slplus-tagalong'),
                    'TAGALONG-show_option_all',
                    __('If set, prepends this text to select "any category" as an option to the selector. Set to blank to not provide the any selection.','slplus-tagalong'),
                    $this->parent->options['show_option_all']
                    );
            $this->parent->Settings->add_input(
                    __('General Info','slplus-tagalong'),
                    __('Category Select Label','slplus-tagalong'),
                    'TAGALONG-label_category',
                    __('The label for the category selector.','slplus-tagalong'),
                    $this->parent->options['label_category']
                    );

            //-------------------------
            // List Store Categories
            //-------------------------
            $slpSectionDescription = '';

            // Category Manager Link
            //
            $slpSectionDescription .=
                    '<h2>'.
                    sprintf(
                            __('<a href="%s">Category Manager</a>','slplus-tagalong'),
                            admin_url() . 'edit-tags.php?taxonomy=stores'
                            ) .
                    '</h2>'.
                    '<p class="lvl1">'.
                    __('The category manager provides a full management interface for Store Categories.', 'slplus-tagalong') .
                    '</p>'
                    ;


            // List of Categories
            //
            $slpSectionDescription .=
                    '<h2>' .
                    __('Category List','slplus-tagalong') .
                    '</h2>' .
                    $this->parent->renderStoreCategoryList()
                    ;


            // Footer Note
            //
            $slpSectionDescription .= '<p class="footernote">';
            if ($this->parent->StorePagesActive) {
                $slpSectionDescription .=
                        __('Clicking on a link will bring you to the most recently touched Store Page.', 'slplus-tagalong') .
                        ' '.
                        __('Depending on your theme you may have older/newer posts button on the bottom of each page to navigate the matching pages.','slplus-tagalong')
                        ;
            } else {
                $slpSectionDescription .= '<br/> ' .
                    sprintf(__('The links will show a list of stores in that category if <a href="%s" target="CSA">Store Pages</a>, a separate add-on pack, is installed.','slplus-tagalong'),
                            'http://www.charlestonsw.com/product-category/slplus/')
                    ;
            }
            $slpSectionDescription .= '</p>';





            $this->parent->Settings->add_section(
                array(
                        'auto'          => true,
                        'name'          => __('Store Categories','slplus-tagalong'),
                        'description'   => $slpSectionDescription,
                        'div_id'        => 'slp_tagalong_categories'
                    )
             );


            //------------------------------------------
            // RENDER
            //------------------------------------------
            $this->parent->Settings->render_settings_page();
        }
    }
}


?>
