/**** Description of RitasIce.com/data folder contents  ****/
/*

This folder serves as the main location to store the most recent copy of JSON data exported from Rita's internal database servers.
These files that are critical to site functionality and are generated on a regular basis.

Files critical to homepage of site:
- Product Flavor/Size Variations json = http://www.ritasfranchises.com/Vendors_Gateway/data/star_product_size_flavor.txt

- Product Nutrition json = http://ritasfranchises.com/Vendors_Gateway/data/star_nutrition.txt

Files critical to franchise location search engine:
- locations json = http://www.ritasfranchises.com/vendors_gateway/data/star_store_detail.txt

*/

